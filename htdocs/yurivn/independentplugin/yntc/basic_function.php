<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/26/14
 * Time: 8:29 PM
 */
function getConfig($configName) {
    require_once('config.php');
    $configData = new configData();
    return $configData->get($configName);
}

function loadHelper($helperName) {
    require_once("Helper/{$helperName}Helper.php");
    $helperName = $helperName.'Helper';
    $helper = new $helperName;
    return $helper;
}

function getModel($modelName, $constructArgs = array())
{
    require_once("Model/AncestorModel.php");
    require_once("Model/DatabaseModel.php");
    require_once("Model/{$modelName}Model.php");
    $modelName = $modelName.'Model';
    $model = new $modelName($constructArgs);
    return $model;
}

function callController($controllerName)
{
    $controllerList = scandir('Controller');
    $controllerExisted = false;

    foreach($controllerList as $controllerFile) {
        $controllerItem = str_replace('Controller.php', '', $controllerFile);
        if(strtolower($controllerItem) == $controllerName) {
            $controllerName = $controllerItem;
            $controllerExisted = true;
            break;
        }
    }
    if(!$controllerExisted) {
        die('Controller does not existed');
    }

    require_once("Controller/AncestorController.php");
    require_once("Controller/{$controllerName}Controller.php");
    $controllerName = $controllerName.'Controller';
    $controller = new $controllerName;
    return $controller;
}

function enhancedEcho(&$var, $default = '')
{
    echo isset($var) ? $var : $default;
}

function getPost($name = null)
{
    if(isset($_POST)) {
        if(is_null($name)) {
            return $_POST;
        }

        if(isset($_POST[$name])) {
            return $_POST[$name];
        } else {
            return '';
        }
    }
}

function getParam($name = null)
{
    if(isset($_GET)) {
        if(is_null($name)) {
            return $_GET;
        }

        if(isset($_GET[$name])) {
            return $_GET[$name];
        } else {
            return '';
        }
    }
}

function isPost() {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        return true;
    } else {
        return false;
    }
}

function shuffleObject($object) {
    $array = (array)$object;
    shuffle($array);
    return (object)$array;
}

function getClientIp() {
    $ipaddress = false;
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');

    return $ipaddress;
}


function romanicNumber($integer, $upcase = true)
{
    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
    $return = '';
    while($integer > 0)
    {
        foreach($table as $rom=>$arb)
        {
            if($integer >= $arb)
            {
                $integer -= $arb;
                $return .= $rom;
                break;
            }
        }
    }

    return $return;
}

function print_pre($data)
{
    echo '<pre>';
    print_r($data);
    die();
}