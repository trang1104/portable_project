<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/14/15
 * Time: 9:57 AM
 */
class configData
{
    protected $_config = array();

    public function __construct()
    {
        $this->_config['databaseName'] = 'yurivn_independentplugin';
        $this->_config['admin'] = '651';
        $this->_config['eventForumId'] = '191';
        $this->_config['eventArchiveId'] = '192';
    }

    public function get($configName)
    {
        return $this->_config[$configName];
    }
}
