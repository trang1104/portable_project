/**
 * Created by Ms Trang on 1/4/15.
 */
function validateForm() {
    var msg = {
        'noblank': 'Vui lòng không bỏ trống trường này',
        'isnumber': 'Chỉ được nhập số',
        'isurl': 'Định dạng link không đúng',
        'isurls': 'Một vài link có định dạng không đúng'
    };
    var error = '';

    $('.gin-form').find('input, textarea, select').each(function(){
        var inputValue = $(this).val();
        if (inputValue == null) {
            inputValue = $(this).html();
        }

        var condition = $(this).attr('condition');

        if(condition != null) {
            condition = condition.split(' ');
            for (var index = 0; index < condition.length; index++) {
                switch(condition[index]) {
                    case 'noblank':
                        if (inputValue == null || inputValue == "") {
                            error += "\n - " + $("label[for='"+$(this).attr('id')+"']").text() + ': ' + msg.noblank;
                        }
                        break;

                    case 'isnumber':
                        if (!$.isNumeric(inputValue)) {
                            error += "\n - " + $("label[for='"+$(this).attr('id')+"']").text() + ': ' + msg.number;
                        }
                        break;

                    case 'isurl':
                        if(!/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(inputValue) && inputValue != null && inputValue != ''){
                            error += "\n - " + $("label[for='"+$(this).attr('id')+"']").text() + ': ' + msg.isurl;
                        }
                        break;

                    case 'isurls':
                        var arrayValue = inputValue.split("\n");
                        for (var offset = 0; offset < condition.length; offset++) {
                            if(!/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(arrayValue[offset]) && arrayValue[offset] != null && arrayValue[offset] != ''){
                                error += "\n - " + $("label[for='"+$(this).attr('id')+"']").text() + ': ' + msg.isurls;
                                break;
                            }
                        }
                        break;
                }
            }
        }


    });

    if(error != '') {
        error = 'Có lỗi xảy ra, hãy kiểm tra lại các thông tin sau:' + error;
        alert(error);
        return false;
    }
//    var x = document.forms["myForm"]["fname"].value;
//    if (x==null || x=="") {
//        alert("First name must be filled out");
//        return false;
//    }
}