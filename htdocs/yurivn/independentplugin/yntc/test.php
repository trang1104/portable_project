<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/10/15
 * Time: 11:05 AM
 */
$knockoutGenerated = 112;
$roundNumber = 1;
$i = 0;
while(pow(2, $i) <= $knockoutGenerated) {
    $i++;
}

$j = $i - 1;
while($knockoutGenerated != pow(2, $j)) {
    $knockoutGenerated = $knockoutGenerated - pow(2, $j);
    $roundNumber++;
    $j--;
}
echo $roundNumber;