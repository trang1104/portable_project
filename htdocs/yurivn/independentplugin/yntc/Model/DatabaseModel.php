<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/26/14
 * Time: 8:32 PM
 */

class DatabaseModel
{
    protected $_mysqli;
    protected $_table;

    public function __construct($constructArgs)
    {
        $this->_mysqli = new mysqli('localhost', 'root', '', $constructArgs['database']);
        if ($this->_mysqli->connect_error) {
            die("Connection failed: " . $this->_mysqli->connect_error);
        }
        $this->_table = $constructArgs['table'];
    }

    public function insert_id()
    {
        return $this->_mysqli->insert_id;
    }

    public function fetchOnce($sql)
    {
        $result = array();
        $resource = $this->_mysqli->query($sql);
        if($resource) {
            $result = $resource->fetch_assoc();
        }
        return $result;
    }

    public function fetchAll($query)
    {
        $result = $this->_mysqli->query($query);
        $data = array();

        if(!empty($result)) {
            while($row = $result->fetch_assoc()){
                $data[] = $row;
            }
        }

        return $data;
    }

    public function insert($data)
    {
        $table_field = array();
        $table_value = array();
        foreach($data as $field => $value){
            $table_field[] = "`$field`";
            $table_value[] = "'$value'";
        }
        $table_field = implode(",", $table_field);
        $table_value = implode(",", $table_value);
        $query= "INSERT INTO `$this->_table`($table_field) VALUES($table_value)";
        $this->_mysqli->query($query);
    }

    public function update($data, $condition)
    {
        $table_newinfo = array();
        foreach($data as $field => $value){
            $table_newinfo[]="`$field` = '$value'";
        }
        $table_newinfo = implode(",", $table_newinfo);
        $query= "UPDATE `$this->_table` SET $table_newinfo WHERE $condition";
        $this->_mysqli->query($query);
    }

    public function delete($condition = '')
    {
        if($condition) {
            $query = "DELETE FROM `$this->_table` WHERE $condition";
        } else {
            $query = "DELETE FROM `$this->_table`";
        }
        $this->_mysqli->query($query);
    }

    public function getColumns(){
        $sql = "DESCRIBE {$this->_table}";
        $resource = $this->_mysqli->query($sql);
        while ($x = $resource->fetch_assoc()){
            $columns[] = $x['Field'];
        }
        return $columns;
    }
}