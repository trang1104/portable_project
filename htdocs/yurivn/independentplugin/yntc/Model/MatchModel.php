<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/26/14
 * Time: 8:35 PM
 */
class MatchModel extends AncestorModel
{
    protected $_tableName = 'yntc_match';
    protected $_idColumn = 'id';

    public function finish()
    {
        $couple1Votes = getModel('Vote')->getMatchVotes($this->getId(), $this->getCouple1());
        $couple2Votes = getModel('Vote')->getMatchVotes($this->getId(), $this->getCouple2());

        if($couple1Votes > $couple2Votes) {
            $winnerId = $this->getCouple1();
        } elseif($couple1Votes < $couple2Votes) {
            $winnerId = $this->getCouple2();
        } else {
            $winnerId = -1;
        }
        $this->setWinner($winnerId)->setActive(0)->save();
    }

    public function calculateTotalRoundRobin()
    {
        $totalGroup = getModel('Group')->getCollection()->count();
        $totalCouple = getModel('Couple')->getCollection()->count();
        $perGroup = $totalCouple / $totalGroup;
        $totalRoundRobin = (($perGroup * ($perGroup - 1)) / 2) * $totalGroup;
        return $totalRoundRobin;
    }

    public function isVoted()
    {
        $userId = loadHelper('Forum')->getUserInfo('userid');
        $ip = getClientIp();
        $isVoted = getModel('Vote')->setFilter(array('match' => $this->getId(), 'user' => $userId))
                                   ->addFilter(array('match' => $this->getId(), 'ip' => $ip))
                                   ->getCollection()
                                   ->count();
        return $isVoted;
    }

    public function getLoser()
    {
        $winner = $this->getWinner();
        if($winner == $this->getCouple1()) {
            return $this->getCouple2();
        } elseif($winner == $this->getCouple2()) {
            return $this->getCouple1();
        } else {
            return false;
        }
    }
}