<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/26/14
 * Time: 8:35 PM
 */
class VoteModel extends AncestorModel
{
    protected $_tableName = 'yntc_vote';
    protected $_idColumn = 'id';

    public function getMatchVotes($matchId, $coupleId)
    {
        $condition = array(
            'match' => $matchId,
            'couple' => $coupleId
        );
        $votes = getModel('Vote')->setFilter($condition)->getCollection()->count();
        return $votes;
    }
}