<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/7/15
 * Time: 9:50 PM
 */
class GroupModel extends AncestorModel
{
    protected $_tableName = 'yntc_group';
    protected $_idColumn = 'id';

    public function setResult()
    {
        $couples = getModel('Couple')->setFilter('group', $this->getId())
                                     ->setOrder('score', 'DESC')
                                     ->getCollection();
        $totalCouples = $couples->count();

        $coupleScanned = 1;
        foreach($couples as $key => $couple) {
            $equalScoreCouple = array();
            for($i = $key + 1; $i < $totalCouples; $i++) {
                if($couple->getScore() == $couples->{$i}->getScore()) {
                    $equalScoreCouple[] = $couples->{$i}->getId();
                }
            }

            switch(count($equalScoreCouple)) {
                case 0:
                    if($coupleScanned == 1) {
                        $this->setWinner($couple->getId());
                        break;
                    } else {
                        $this->setRunnerUp($couple->getId());
                        break 2;
                    }

                case 1:
                    if($coupleScanned == 1) {
                        $winner = getModel('Match')->setFilter(array('couple1' => $couple->getId(), 'couple2' => $equalScoreCouple[0]))
                                                   ->addFilter(array('couple1' => $equalScoreCouple[0], 'couple2' => $couple->getId()))
                                                   ->getFirstItem()
                                                   ->getWinner();
                        $runnerUp = ($couple->getId() == $winner ? $equalScoreCouple[0] : $couple->getId());
                        $this->setWinner($winner);
                        $this->setRunnerUp($runnerUp);
                    } else {
                        $runnerUp = getModel('Match')->setFilter(array('couple1' => $couple->getId(), 'couple2' => $equalScoreCouple[0]))
                                                     ->addFilter(array('couple1' => $equalScoreCouple[0], 'couple2' => $couple->getId()))
                                                     ->getFirstItem()
                                                     ->getWinner();
                        $this->setRunnerUp($runnerUp);
                    }
                    break 2;
                default:
                    echo 'Something went wrong with group '. $this->getName();
                    die();
            }
            $coupleScanned++;

        }
        $this->save();
    }


}