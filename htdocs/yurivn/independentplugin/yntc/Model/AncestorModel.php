<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/28/14
 * Time: 6:52 PM
 */
class AncestorModel
{
    protected $_db;
    protected $_storedData = array();
    protected $_filters = array();
    protected $_orders = array();

    public function __construct()
    {
        if(isset($this->_tableName)) {
            $this->_db = getModel('Database', array('database' => getConfig('databaseName'), 'table' => $this->_tableName));
        }
    }

    public function clear()
    {
        foreach ($this as $key => $value) {
            unset($this->$key);
        }
        return $this;
    }

    public function reset()
    {
        foreach ($this as $key => $value) {
            $this->$key = null;
        }
        return $this;
    }

    public function setFilter($field, $value = null)
    {
        if($value === null) {
            $filter = array($field);
        } else {
            $filter = array(array($field => $value));
        }
        $this->_filters = $filter;
        return $this;
    }

    public function addFilter($field, $value = null)
    {
        if($value === null) {
            $filter = array($field);
        } else {
            $filter = array(array($field => $value));
        }
        $this->_filters = array_merge($this->_filters, $filter);
        return $this;
    }

    public function setOrder($field, $orderType = 'ASC')
    {
        $this->_orders = array($orderType => $field);
        return $this;
    }

    public function addOrder($field, $orderType = 'ASC')
    {
        $this->_orders = array_merge($this->_orders, array($orderType => $field));
        return $this;
    }

    public function save()
    {
        $columns = $this->_db->getColumns();

        foreach($this->_storedData as $name => $value) {
            foreach($columns as $column) {
                if(preg_replace("/[^a-z0-9]+/", "", $column) == $name) {
                    $data[$column] = $this->_storedData[$name];
                    unset($this->_storedData[$name]);
                }
            }
        }

        $getId = 'get'.(preg_replace("/[^a-z0-9]+/", "", $this->_idColumn));
        if(!$this->$getId()) {
            try{
                $this->_db->insert($data);
                return true;
            } catch(Exception $e) {
                return false;
            }
        } else {
            try{
                $this->_db->update($data, "{$this->_idColumn} = '{$this->$getId()}'");
                return true;
            } catch(Exception $e) {
                return false;
            }

        }
    }

    public function load($value, $columnToSelect='')
    {
        if(empty($columnToSelect)) {
            $columnToSelect = $this->_idColumn;
        }
        $data = $this->_db->fetchOnce("SELECT * FROM `{$this->_tableName}` WHERE `$columnToSelect` = '$value'");

        if(!empty($data)) {
            foreach($data as $column => $value) {
                $property = preg_replace("/[^a-z0-9]+/", "", $column);
                $set = "set$property";
                $this->$set($value);
            }
            return $this;
        } else {
            return false;
        }

    }

    public function delete($id)
    {
        $this->_db->delete("`{$this->_idColumn}` = '$id'");
    }

    public function deleteAll()
    {
        $this->_db->delete();
    }

    public function getCollection()
    {
        $where = '';
        $order = '';
        if(!empty($this->_filters)) {
            $where = 'WHERE ';
            foreach($this->_filters as $filters) {
                $whereNode = '';
                foreach($filters as $field => $value) {
                    if($whereNode != '') {
                        $whereNode.= " AND `" . $field . "` LIKE '" . $value. "'";
                    } else {
                        $whereNode .= "`" . $field . "` LIKE '" . $value. "'";
                    }
                }
                if(count($filters) > 1) {
                    $whereNode = "($whereNode)";
                }

                if($where != 'WHERE ') {
                    $where.= " OR $whereNode";
                } else {
                    $where .= "$whereNode";
                }
            }
        }

        if(!empty($this->_orders)) {
            $order = 'ORDER BY ';
            foreach($this->_orders as $orderType => $field) {
                if($order != 'ORDER BY ') {
                    $order.= ", `$field` $orderType";
                } else {
                    $order .= "`$field` $orderType";
                }
            }
        }

        $query = "SELECT * FROM `{$this->_tableName}` $where $order";
        $dataList = $this->_db->fetchAll($query);

        $collection = getModel('Ancestor')->clear();
        $i = 0;
        if($dataList) {
            foreach($dataList as $data) {
                $item = getModel(str_replace('Model', '', get_called_class()));
                foreach($data as $propertyName => $propertyValue) {
                    $set = "set$propertyName";
                    $item->$set($propertyValue);
                }
                $collection->$i = $item;
                $i++;
            }
        }

        return $collection;
    }

    public function getFirstItem()
    {
        return $this->getCollection()->{0};
    }

    public function getData()
    {
        $dataList = array();
        if(get_called_class() == 'AncestorModel') {
            foreach($this as $item) {
                $data = array();
                foreach($item->_storedData as $propertyName => $propertyValue) {
                    $data[$propertyName] = $propertyValue;
                }

                if(!empty($data)) {
                    $dataList[] = $data;
                }
            }
        } else {
            foreach($this->_storedData as $propertyName => $propertyValue) {
                $dataList[$propertyName] = $propertyValue;
            }
        }

        return $dataList;
    }

    public function count()
    {
        $numberOfProperties = 0;
        foreach ($this as $key => $value) {
            $numberOfProperties++;
        }
        return $numberOfProperties;
    }

    public function insert_id()
    {
        return $this->_db->insert_id();
    }

    public function __call($method,$args)
    {
        if(substr($method, 0, 3) == 'get' && isset($this->{strtolower(substr($method, 3))})) {
            return $this->{strtolower(substr($method, 3))};
        }

        if(substr($method, 0, 3) == 'set') {
            $this->_storedData[strtolower(substr($method, 3))] = $this->{strtolower(substr($method, 3))} = $args[0];
            return $this;
        }

        if(substr($method, 0, 5) == 'unset') {
            $this->_storedData[strtolower(substr($method, 5))] = $this->{strtolower(substr($method, 5))} = $args[0];
            return $this;
        }
    }
}