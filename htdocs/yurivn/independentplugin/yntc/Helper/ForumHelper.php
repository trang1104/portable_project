<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/10/15
 * Time: 8:17 PM
 */
class ForumHelper
{
    protected $_vbulletin;

    public function __construct()
    {
        $a = getcwd();
        chdir('../../');
        require_once('./global.php');
        global $vbulletin;
        $this->_vbulletin = $vbulletin;
        chdir($a);
    }

    public function getUserInfo($field = '')
    {
        if($field) {
            return $this->_vbulletin->userinfo[$field];
        } else {
            return $this->_vbulletin->userinfo;
        }

    }

    public function saveThread($threadData)
    {
        $threadMan =& datamanager_init('Thread_FirstPost', $this->_vbulletin, ERRTYPE_ARRAY, 'threadpost');
        $forumInfo = fetch_foruminfo($threadData['forumId']);

        $threadInfo = fetch_threadinfo($threadData['threadId']);
        if($threadInfo) {
            $threadMan->set_existing($threadInfo);
        }

        $threadMan->set_info('forum', $forumInfo);
        $threadMan->set_info('thread', $threadInfo);
        $threadMan->setr('forumid', $threadData['forumId']);
        $threadMan->setr('userid', $threadData['userId']);
        $threadMan->setr('title', $threadData['title']);
        $threadMan->setr('pagetext', $threadData['pageText']);

        $threadMan->setr('showsignature', $signature);
        $threadMan->set('allowsmilie', $threadData['allowSmilie']);
        $threadMan->set('visible', $threadData['visible']);
        $threadMan->set_info('parseurl', $threadData['parseUrl']);
        $threadMan->set('prefixid', $threadData['prefixId']);

        $threadMan->save();
        //rebuildForum($threadData['forumId']);
    }
}