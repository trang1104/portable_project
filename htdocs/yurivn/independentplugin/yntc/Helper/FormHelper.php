<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/5/15
 * Time: 12:18 AM
 */
class FormHelper
{
    protected $errorMsg = '';
    protected $error = array();
    protected $msg = array(
        'noblank' => 'Vui lòng không bỏ trống trường này',
        'isnumber' => 'Chỉ được nhập số',
        'isurl' => 'Định dạng link không đúng',
        'isurls' => 'Một vài link có định dạng không đúng',
        'ispower2' => 'Giá trị nhập vào phải là lũy thừa của 2'
    );

    public function validate($rule)
    {
        foreach($rule as $field => $ruleInfo) {
            $fieldLabel = $ruleInfo[0];
            $conditions = explode(' ', $ruleInfo[1]);
            if(in_array('trim', $conditions)) {
                $inputValue = trim(getPost($field));
            } else {
                $inputValue = getPost($field);
            }

            foreach($conditions as $condition) {

                if(strpos($condition, 'ud_') === 0) {
                    $function = substr($condition,3);
                    if(!callController(getParam('controller'))->$function($this)){
                        $this->errorMsg .= "\n - $fieldLabel: {$this->msg[$function]}";
                        $this->error[$field] = $this->msg[$function];
                    }
                } else {
                    switch($condition) {
                        case 'noblank':
                            if ($inputValue == null || $inputValue == "") {
                                $this->errorMsg .= "\n - $fieldLabel: {$this->msg['noblank']}";
                                $this->error[$field] = $this->msg['noblank'];
                            }
                            break;
                        case 'isnumber':
                            if (!is_numeric($inputValue)) {
                                $this->errorMsg .= "\n - $fieldLabel: {$this->msg['isnumber']}";
                                $this->error[$field] = $this->msg['isnumber'];
                            }
                            break;

                        case 'isurl':
                            if(filter_var($inputValue, FILTER_VALIDATE_URL) === false) {
                                $this->errorMsg .= "\n - $fieldLabel: {$this->msg['isurl']}";
                                $this->error[$field] = $this->msg['isurl'];
                            }
                            break;

                        case 'isurls':
                            $inputItems = explode("\n", $inputValue);
                            foreach($inputItems as $inputItem) {
                                if(!filter_var(trim($inputItem), FILTER_VALIDATE_URL)) {
                                    $this->errorMsg .= "\n - $fieldLabel: {$this->msg['isurls']}";
                                    $this->error[$field] = $this->msg['isurls'];
                                    break;
                                }
                            }
                            break;

                        case 'ispower2':
                            if(($inputValue & ($inputValue - 1)) != 0) {
                                $this->errorMsg .= "\n - $fieldLabel: {$this->msg['ispower2']}";
                                $this->error[$field] = $this->msg['ispower2'];
                            }
                    }
                }

            }
        }

        if($this->errorMsg) {
            $this->errorMsg = 'Có lỗi xảy ra, hãy kiểm tra lại các thông tin sau:' . $this->errorMsg;
            return false;
        } else {
            return true;
        }

    }

    public function addMsg($msg)
    {
        $this->msg += $msg;
    }

    public function getError($field)
    {
        if(isset($this->error[$field])) {
            return $this->error[$field];
        }

    }

    public function getErrorMsg()
    {
        return $this->errorMsg;
    }
}
