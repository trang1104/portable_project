<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/26/14
 * Time: 8:28 PM
 */
require_once('basic_function.php');

$controllerName = getParam('controller');
$actionName = getParam('action');

if($controllerName && $actionName) {
    $controller = callController($controllerName);
    $action = $actionName.'Action';
    $controller->$action();
}