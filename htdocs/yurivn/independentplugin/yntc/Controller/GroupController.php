<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/7/15
 * Time: 9:52 PM
 */
class GroupController extends AncestorController
{
    public function fillAction()
    {
        $data['pageTitle'] = 'YNTC - Chia bảng';
        $data['form'] = $form = loadHelper('Form');

        if(isPost()) {
            $data['inputData'] = $inputData = getPost();
            $rule = array(
                'numberofgroup' => array('Số lượng bảng', 'trim noblank isnumber ispower2 ud_checkCouple')
            );

            if ($form->validate($rule)) {
                $groupModel = getModel('Group');
                $groupModel->deleteAll();
                for($i = 1; $i <= $inputData['numberofgroup']; $i++) {
                    $groupModel->setName($i)->save();
                }

                $coupleCollection = getModel('Couple')->getCollection();
                $numberOfCouple = $coupleCollection->count();
                $perGroup = $numberOfCouple/$inputData['numberofgroup'];

                $shuffledCoupleCollection = shuffleObject($coupleCollection);
                $groupData = $groupModel->getCollection()->getData();
                $currentGroup = 0;
                $i = 1;
                foreach($shuffledCoupleCollection as $couple) {
                    $couple->setGroup($groupData[$currentGroup]['id'])->save();
                    if($i == $perGroup) {
                        $currentGroup++;
                        $i = 0;
                    }
                    $i++;
                }
                $data['msg'] = 'Chia bảng thành công!';
                $this->renderView('simplemsg', $data);
            }
        }

        $this->renderView('form', $data);
    }

    public function checkCouple($form)
    {
        $form->addMsg(array('checkCouple' => 'Số lượng cặp đôi không phù hợp với số bảng này'));
        $numberOfCouple = getModel('Couple')->getCollection()->count();
        $perGroup = $numberOfCouple/getPost('numberofgroup');
        //print_pre($perGroup % 2);
        if($perGroup % 2 != 0 || $perGroup <= 0 || !is_int($perGroup)) {
            return false;
        } else {
            return true;
        }
    }
}