<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/7/15
 * Time: 9:39 PM
 */
class MatchController extends AncestorController
{
    public function generateAction()
    {
        $matchModel = getModel('Match');
        $groupModel = getModel('Group');

        //finish and set winner for all previous matches
        $coupleModel = getModel('Couple');
        $activeMatches = $matchModel->setFilter('active', 1)->getCollection();
        if($activeMatches->count()) {
            foreach($activeMatches as $activeMatch) {
                $activeMatch->finish();

                $isKnockoutRound = $coupleModel->setFilter('active', 0)->getCollection()->count();

                if(!$isKnockoutRound) {
                    $winnerId = $activeMatch->getWinner();
                    if($winnerId != -1) {
                        $winner = $coupleModel->load($winnerId);
                        $oldScore = $winner->getScore();
                        $winner->setScore($oldScore + 3)->save();
                    } elseif($winnerId == -1) {
                        $couple1 = $coupleModel->load($activeMatch->getCouple1());
                        $couple1Score = $couple1->getScore();
                        $couple1->setScore($couple1Score + 1)->save();

                        $couple2 = $coupleModel->load($activeMatch->getCouple2());
                        $couple2Score = $couple2->getScore();
                        $couple2->setScore($couple2Score + 1)->save();
                    }
                } else {
                    $loserId = $activeMatch->getLoser();
                    $coupleModel->load($loserId)->setActive(0)->save();

                    $groupWithLoser = $groupModel->setFilter('winner', $loserId)->addFilter('runnerup', $loserId)->getFirstItem();
                    if($groupWithLoser->getWinner() == $loserId) {
                        $groupWithLoser->setWinner(0);
                    } elseif($groupWithLoser->getRunnerUp() == $loserId) {
                        $groupWithLoser->setRunnerUp(0);
                    }
                    $groupWithLoser->save();
                }
            }
        }

        //Generate matches
        $totalRoundRobin = $matchModel->calculateTotalRoundRobin();
        $generatedMatches = getModel('Match')->getCollection()->count();

        if($generatedMatches < $totalRoundRobin) {
            $this->generateRoundRobinMatches();
            $generatedMatchesAfter = getModel('Match')->getCollection()->count();
            if($generatedMatchesAfter == $totalRoundRobin) {
                $data['pageTitle'] = 'Tạo trận đấu';
                $data['msg'] = 'Đã tạo hết toàn bộ trận đấu cho vòng bảng';
                $this->renderView('simplemsg', $data);
            }
        } elseif($generatedMatches == $totalRoundRobin) {
            $groupCollection = getModel('Group')->getCollection();
            foreach($groupCollection as $group) {
                $group->setResult();
                $couplesInGroup = $coupleModel->setFilter('group', $group->getId())->getCollection();
                foreach($couplesInGroup as $coupleInGroup) {
                    $coupleId = $coupleInGroup->getId();
                    if($coupleId != $group->getWinner() && $coupleId != $group->getRunnerUp()){
                        $coupleInGroup->setActive(0)->save();
                    }
                }
            }

            $this->shuffleRunnerUp();

            $this->generateKnockoutMatches();
        } elseif($generatedMatches > $totalRoundRobin) {
            $this->generateKnockoutMatches();
        }
    }

    public function displayAction()
    {
        $data['userId'] = loadHelper('Forum')->getUserInfo('userid');
        if(!$data['userId']) {
            $data['pageTitle'] = 'Trận đấu';
            $data['msg'] = 'Bạn phải đăng nhập mới có thể bình chọn';
            $this->renderView('simplemsg', $data);
        }

        $matchId = getParam('id');
        $data['match'] = getModel('Match')->load($matchId);
        if(!$data['match']) {
            $data['pageTitle'] = 'Trận đấu';
            $data['msg'] = 'Không có thông tin về trận đấu này';
            $this->renderView('simplemsg', $data);
        }
        $data['couple1'] = getModel('Couple')->load($data['match']->getCouple1());
        $data['couple2'] = getModel('Couple')->load($data['match']->getCouple2());
        $data['voteModel'] = getModel('Vote');

        $data['pageTitle'] = 'Trận đấu';
        $this->renderView('match', $data);
    }

    protected function shuffleRunnerUp()
    {
        $groupCollection = getModel('Group')->getCollection();
        $groupCollectionSize = $groupCollection->count();
        $bottomKey = $groupCollectionSize - 1;
        $scannedGroup = 0;
        foreach($groupCollection as $key => $group) {
            if($scannedGroup == $groupCollectionSize/2)
            {
                break;
            }
            $topGroupRunnerUp = $group->getRunnerUp();
            $bottomGroupRunnerUp = $groupCollection->{$bottomKey - $scannedGroup}->getRunnerUp();

            $group->setRunnerUp($bottomGroupRunnerUp)->save();
            $groupCollection->{$bottomKey - $scannedGroup}->setRunnerUp($topGroupRunnerUp)->save();
            $scannedGroup++;
        }
    }

    protected function generateRoundRobinMatches()
    {
        $groupCollection = getModel('Group')->getCollection();
        $matchModel = getModel('Match');
        $forum = loadHelper('Forum');

        $generatedMatches = $matchModel->getCollection()->count();
        $perStep = getModel('Couple')->getCollection()->count() / 2;
        $round = romanicNumber($generatedMatches / $perStep + 1);

        echo $round;
        foreach($groupCollection as $group) {
            $coupleCollection = getModel('Couple')->setFilter('group', $group->getId())->getCollection();
            $numberOfCouple = $coupleCollection->count();

            foreach($coupleCollection as $key => $couple) {
                for($i = $key+1; $i <= $numberOfCouple; $i++) {
                    if(!isset($coupleCollection->{$i})) {
                        continue;
                    }

                    $couple1 = $coupleCollection->{$key};
                    $couple2 = $coupleCollection->{$i};
                    $couple1Id = $couple1->getId();
                    $couple2Id = $couple2->getId();
                    $existedMatch = $matchModel->setFilter(array('couple1' => $couple1Id, 'couple2' => $couple2Id))
                                               ->addFilter(array('couple1' => $couple2Id, 'couple2' => $couple1Id))
                                               ->getCollection();
                    if(!$existedMatch->count()) {
                        $matchModel->setCouple1($couple1Id)
                                   ->setCouple2($couple2Id)
                                   ->setActive(1)
                                   ->save();

                        $postContent = "[Center][SIZE=4][B][COLOR=Red]Vòng bảng $round - Bảng " . $group->getName() . "[/COLOR][/B][/SIZE]\n
                                        [INDENT][LEFT][COLOR=Red][B]Lưu ý:[/B][/COLOR]\n
                                        [B]- Bạn cần phải đăng nhập mới có thể bình chọn.\n
                                        - Clone không được phép bình chọn.[/B][/LEFT][/INDENT]\n
                                        [SPOILER=\" \"][iframe=937|700]independentplugin/yntc/index.php?controller=match&action=display&id=" . $matchModel->insert_id() . "[/iframe][/SPOILER][/Center]\n
                        ";
                        $threadData = array(
                            'forumId' => getConfig('eventForumId'),
                            'userId' => getConfig('admin'),
                            'title' => "Vòng bảng $round - Bảng " . $group->getName() . ": [" . $couple1->getChar1() . " x " . $couple1->getChar2() . "] VS [" . $couple2->getChar1() . " x " . $couple2->getChar2() . "]",
                            'pageText' => $postContent,
                            'showSignature' => 1,
                            'allowSmilie' => 1,
                            'visible' => 1,
                            'parseUrl' => 1
                        );
                        $forum->saveThread($threadData);

                        unset($coupleCollection->{$key});
                        unset($coupleCollection->{$i});
                        break;
                    }
                }
            }
        }
    }

    protected function generateKnockoutMatches()
    {
        $groupCollection = getModel('Group')->getCollection();
        $matchModel = getModel('Match');

        $remainCouples = new stdClass();
        $i = 0;
        foreach($groupCollection as $group) {
            if($winner = $group->getWinner()) {
                $remainCouples->{$i} = $winner;
                $i++;
            }
            if($runnerUp = $group->getRunnerUp()) {
                $remainCouples->{$i} = $runnerUp;
                $i++;
            }

        }

        $knockoutGenerated = $matchModel->getCollection()->count() - $matchModel->calculateTotalRoundRobin();
        $numberOfRemainCouples = count((array)$remainCouples);
        if($numberOfRemainCouples == 1) {
            $eventWinner = getModel('Couple')->load($remainCouples->{0});
            $data['pageTitle'] = 'Kết quả Event';
            $data['msg'] = "Event kết thúc, cặp đôi chiến thắng là " . $eventWinner->getChar1() . " x " . $eventWinner->getChar2() . "!";
            $this->renderView('simplemsg', $data);
        }

        $forum = loadHelper('Forum');

        switch($numberOfRemainCouples) {
            case 2:
                $roundName = 'Chung kết';
                break;
            case 4:
                $roundName = "Bán kết";
                break;
            case 8:
                $roundName = "Tứ kết";
                break;
            default:
                if($knockoutGenerated == 0) {
                    $roundNumber = 1;
                } else {
                    $roundNumber = 2;

                    $i = 0;
                    while(pow(2, $i) <= $knockoutGenerated) {
                        $i++;
                    }
                    $j = $i - 1;
                    while($knockoutGenerated != pow(2, $j)) {
                        $knockoutGenerated = $knockoutGenerated - pow(2, $j);
                        $roundNumber++;
                        $j--;
                    }
                }
                $roundName = "Vòng loại $roundNumber";
                break;
        }

        $currentMatch = 1;
        foreach($remainCouples as $key => $couple) {
            $couple1 = getModel('Couple')->load($couple);
            $couple2 = getModel('Couple')->load($remainCouples->{$key + 1});
            $matchModel->setCouple1($couple)
                       ->setCouple2($remainCouples->{$key + 1})
                       ->setWinner(0)
                       ->setActive(1)
                       ->save();

            if($numberOfRemainCouples > 8) {
                $roundNameFull = "$roundName - Trận $currentMatch";
            } elseif($numberOfRemainCouples > 2) {
                $roundNameFull = "$roundName $currentMatch";
            } else {
                $roundNameFull = $roundName;
            }
            $postContent = "[Center][SIZE=4][B][COLOR=Red]{$roundNameFull}[/COLOR][/B][/SIZE]\n
                            [INDENT][LEFT][COLOR=Red][B]Lưu ý:[/B][/COLOR]\n
                            [B]- Bạn cần phải đăng nhập mới có thể bình chọn.\n
                            - Clone không được phép bình chọn.[/B][/LEFT][/INDENT]\n
                            [SPOILER=\" \"][iframe=937|700]independentplugin/yntc/index.php?controller=match&action=display&id=" . $matchModel->insert_id() . "[/iframe][/SPOILER][/Center]\n
                            ";
            $threadData = array(
                'forumId' => getConfig('eventForumId'),
                'userId' => getConfig('admin'),
                'title' => "$roundNameFull: [" . $couple1->getChar1() . " x " . $couple1->getChar2() . "] VS [" . $couple2->getChar1() . " x " . $couple2->getChar2() . "]",
                'pageText' => $postContent,
                'showSignature' => 1,
                'allowSmilie' => 1,
                'visible' => 1,
                'parseUrl' => 1
            );
            $forum->saveThread($threadData);
            unset($remainCouples->{$key});
            unset($remainCouples->{$key + 1});
            $currentMatch++;
        }
    }
}