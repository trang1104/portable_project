<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/7/15
 * Time: 9:52 PM
 */
class VoteController extends AncestorController
{
    public function voteAction()
    {
        if(isPost()) {
            $result = array(
                'error' => 0,
                'msg' => ''
            );

            $matchId = getPost('match');
            $coupleId = getPost('couple');
            $userId = loadHelper('Forum')->getUserInfo('userid');
            $ip = getClientIp();

            $voteModel = getModel('Vote');
            $existedVote = $voteModel->setFilter(array('match' => $matchId, 'user' => $userId))
                ->addFilter(array('match' => $matchId, 'ip' => $ip))
                ->getCollection()
                ->count();

            if($existedVote > 0 || !$userId) {
                $result['error'] = 1;
                $result['msg'] = 'Bạn chỉ có thể bình chọn một lần cho mỗi trận đấu';
            } else {
                $voteModel->setMatch($matchId)
                    ->setCouple($coupleId)
                    ->setUser($userId)
                    ->setIp($ip)
                    ->save();
            }

            echo json_encode($result);
        }
    }
}