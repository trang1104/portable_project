<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class CoupleController extends AncestorController
{
    public function addAction()
    {
        $data['pageTitle'] = 'YNTC - Đề cử cặp đôi';
        $form = loadHelper('Form');
        $data['form'] = $form;

        if(isPost()) {
            $data['inputData'] = getPost();
            $rule = array(
                    'char1' => array('Nhân vật 1', 'trim noblank ud_validateCouple'),
                    'char2' => array('Nhân vật 2', 'trim noblank ud_validateCouple'),
                    'image' => array('Ảnh minh họa', 'trim noblank isurls'),
                    'from' => array('Đến từ', 'trim noblank ud_validateCouple'),
                    'url' => array('Link tham khảo', 'trim noblank isurl')
            );

            if ($form->validate($rule)) {
                $coupleModel = getModel('Couple');
                $coupleModel->setChar1(trim(getPost('char1')))
                    ->setChar2(trim(getPost('char2')))
                    ->setImage(trim(getPost('image')))
                    ->setFrom(trim(getPost('from')))
                    ->setUrl(trim(getPost('url')));
                if($coupleModel->save()) {
                    $data['msg'] = 'Cám ơn, bạn đã đề cử cặp đôi ' . getPost('char1') . ' x ' . getPost('char2') . ' thành công! Nếu muốn, bạn có thể tiếp tục <a href="index.php?controller=couple&action=add"> Đề cử cặp đôi khác</a>';
                    $this->renderView('simplemsg', $data);
                }
            }
        }

        $this->renderView('form', $data);

    }

    public function listAction()
    {
        $data['pageTitle'] = 'YNTC - Bảng xếp hạng';
        $data['couples'] = getModel('Couple')->setFilter('active', 1)->setOrder('group')->addOrder('score', 'DESC')->getCollection();
        $this->renderView('list', $data);
    }

    public function validateCouple($form)
    {
        $form->addMsg(array('validateCouple' => 'Cặp này đã được đề cử'));

        $case1 = array(
            'char1' => trim(getPost('char1')),
            'char2' => trim(getPost('char2')),
            'from' => trim(getPost('from'))
        );

        $case2 = array(
            'char1' => trim(getPost('char2')),
            'char2' => trim(getPost('char1')),
            'from' => trim(getPost('from'))
        );
        $existedCouple = getModel('Couple')->setFilter($case1)
                                           ->addFilter($case2)
                                           ->getCollection()
                                           ->count();

        if($existedCouple > 0) {
            return false;
        } else {
            return true;
        }

    }
}