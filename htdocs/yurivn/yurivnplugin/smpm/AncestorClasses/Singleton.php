<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/24/15
 * Time: 7:40 PM
 */
class Singleton
{
    private static $instances = array();
    protected function __construct() {}
    protected function __clone() {}
    public function __wakeup()
    {
        throw new Exception("Cannot unserialize singleton");
    }

    public static function getInstance($constructArgs='')
    {
        $cls = get_called_class(); // late-static-bound class name
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static($constructArgs);
        }
        return self::$instances[$cls];
    }
}