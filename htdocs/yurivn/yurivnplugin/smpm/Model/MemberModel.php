<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/26/15
 * Time: 1:46 PM
 */
class MemberModel extends AncestorModel
{
    protected $_tableName = 'smpm_member';
    protected $_serializeValues = array('position');

    public function getName()
    {
        if($this->getUserId()) {
            return callModel('ForumUser')->load($this->getUserId())->getUserName();
        } else {
            return false;
        }

    }

    public function delete()
    {
        $projectMembers = callModel('ProjectMember')->setFilter('memberid', $this->getId())->getCollection();
        foreach($projectMembers as $projectMember) {
            $projectMember->delete();
        }
        parent::delete();
    }
}