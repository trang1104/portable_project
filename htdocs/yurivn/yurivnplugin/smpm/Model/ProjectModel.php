<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/26/15
 * Time: 1:46 PM
 */
class ProjectModel extends AncestorModel
{
    protected $_tableName = 'smpm_project';
    protected $_serializeValues = array('position');

    public function getLeaderName()
    {
        if($this->getLeader()) {
            $leaderUserId = getModel('Member')->load($this->getLeader())->getUserId();
            return getModel('ForumUser')->load($leaderUserId)->getUserName();
        } else {
            return false;
        }
    }

    public function getLeaderUserId()
    {
        if($this->getLeader()) {
            return getModel('Member')->load($this->getLeader())->getUserId();
        } else {
            return false;
        }
    }

    public function delete()
    {
        $projectMembers = getModel('ProjectMember')->setFilter('projectid', $this->getId())->getCollection();
        foreach($projectMembers as $projectMember) {
            $projectMember->delete();
        }
        parent::delete();
    }
}