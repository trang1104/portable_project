<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/26/15
 * Time: 1:46 PM
 */
class ProjectMemberModel extends AncestorModel
{
    protected $_tableName = 'smpm_projectmember';
    protected $_serializeValues = array('position');

    public function getMemberName()
    {
        if($this->getMemberId()) {
            return getModel('Member')->load($this->getMemberId())->getName();
        } else {
            return false;
        }
    }

    public function getProjectName()
    {
        if($this->getProjectId()) {
            return getModel('Project')->load($this->getProjectId())->getName();
        } else {
            return false;
        }
    }

    public function getApplications($leader = 0)
    {
        $projectModel = getModel('Project');
        if($leader) {
            $projectModel->setFilter('leader', $leader);
        }
        $projects = $projectModel->getCollection();
        if($projects->count()) {
            foreach($projects as $project) {
                $filter = array(
                    'projectid' => $project->getId(),
                    'applying' => 1
                );
                $this->addFilter($filter);
            }
        } else {
            $filter = array(
                'projectid' => 0
            );
            $this->setFilter($filter);
        }
        return $this->getCollection();

    }
}