<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class AncestorController extends Singleton
{
    protected $_aditionalDataView = array();

    protected function renderView($viewName, $dataView=array())
    {
        $dataView += $this->_aditionalDataView;
        if(is_array($dataView)) {
            foreach($dataView as $key => $value) {
                ${$key} = $value;
            }
        }
        $forum = callHelper('Forum');
        $userId = $forum->getUserInfo('userid');

        $allPendingApplications = getModel('ProjectMember')->getApplications()->count();
        $pendingApplications = getModel('ProjectMember')->getApplications(getModel('Member')->load($userId, 'userid')->getId())->count();

        ob_start();
        require_once("View/".str_replace('Controller', '',get_class($this)."/$viewName.phtml"));
        $content = ob_get_contents();
        ob_get_clean();

        require_once("View/wrapper.phtml");

        die();
    }

    protected function getAction()
    {
        return getParam('action');
    }

    public function setAdditionalDataView($additionalDataView)
    {
        $this->_aditionalDataView = $additionalDataView;
    }

    public function addAdditionalDataView($additionalDataView)
    {
        if(empty($this->_aditionalDataView)) {
            $this->_aditionalDataView = $additionalDataView;
        } else {
            $this->_aditionalDataView += $additionalDataView;
        }

    }
}