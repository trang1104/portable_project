<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class ProjectController extends AncestorController
{
    protected $_model;

    public function __construct()
    {
        $this->_model = getModel('Project');
        parent::__construct();
    }

    public function listAction()
    {
        $projects = $this->_model->setOrder(array('desc'=> array('processtype', 'status')))->getCollection();
        $dataView['pageTitle'] = 'Danh sách Project';
        $dataView['projects'] = $projects;
        $this->renderView('list', $dataView);
    }

    public function addAction()
    {
        $dataView['pageTitle'] = 'Thêm Project';
        $dataView['formButton'] = 'Thêm';
        if(isPost()) {
            $inputData = $this->convertData(getPost());
            $project = $this->_model->load($inputData['code'], 'code');
            if($project->getId()) {
                die('Project đã tồn tại, vui lòng kiểm tra lại Project Code');
            }
            if(!$inputData['leader']) {
                die("{$inputData['leadername']} không phải là thành viên của nhóm dịch");
            }
            $project
                ->setName($inputData['name'])
                ->setType($inputData['type'])
                ->setLeader($inputData['leader'])
                ->setCode($inputData['code'])
                ->setStatus($inputData['status'])
                ->setPosition($inputData['position'])
                ->setProcess($inputData['process'])
                ->setProcessType($inputData['processtype'])
                ->save();

            die('Thêm Project thành công!');
        }

        $this->renderView('form', $dataView);
    }

    public function editAction()
    {
        $dataView['pageTitle'] = 'Sửa Project';
        $dataView['formButton'] = 'Sửa';
        $forum = callHelper('Forum');
        $form = callHelper('Form');
        $project = $this->_model->load(getParam('id'));
        if(!$project->getId()) {
            die('Project không tồn tại!');
        }
        if($forum->getUserInfo('userid') != $project->getLeaderUserId()
            && $forum->getUserInfo('userid') != $forum->getConfig('SpecialUsers/superadministrators', 2)) {
            print_no_permission();
        }

        $formData = $project->getData();
        $formData['leadername'] = $project->getLeaderName();
        $form->setFormData($formData);

        if(isPost()) {
            $inputData = $this->convertData(getPost());
            if($inputData['code'] != $project->getCode()) {
                if(getModel('Project')->load($inputData['code'], 'code')->getId()) {
                    die('Project code bị trùng, vui lòng kiểm tra lại.');
                }
            }

            if(!$inputData['leader']) {
                die("{$inputData['leadername']} không phải là thành viên của nhóm dịch");
            }

            $project
                ->setName($inputData['name'])
                ->setType($inputData['type'])
                ->setLeader($inputData['leader'])
                ->setCode($inputData['code'])
                ->setStatus($inputData['status'])
                ->setPosition($inputData['position'])
                ->setProcess($inputData['process'])
                ->setProcessType($inputData['processtype'])
                ->save();

            die('Sửa Project thành công!');
        }
        $dataView['project'] = $project;
        $this->renderView('form', $dataView);
    }

    public function deleteAction() {
        $forum = callHelper('Forum');
        $userId = $forum->getUserInfo('userid');
        $project = getModel('Project')->load(getParam('id'));
        if(!$userId == $project->getLeaderUserId() && !$userId == $forum->getConfig('SpecialUsers/superadministrators', 2)) {
            print_no_permission();
        }
        if(!$project->getId()) {
            die('Không tồn tại project này');
        }

        $project->delete();

        redirect('yurivnplugin/smpm/index.php?controller=project&action=list');
    }

    protected function convertData($rawData)
    {
        $convertedData = $rawData;
        $convertedData['code'] = strtoupper($rawData['code']);

        if($rawData['leadername']) {
            $leaderUserId = getModel('ForumUser')->load($rawData['leadername'], 'username')->getUserId();
            if($leaderUserId){
                $convertedData['leader'] = getModel('Member')->load($leaderUserId, 'userid')->getId();
            }
        } else {
            $convertedData['leader'] = getModel('Member')->load(callHelper('Forum')->getUserInfo('userid'), 'userid')->getId();
        }

        if(!$rawData['position']) {
            $convertedData['position'] = array();
        }

        return $convertedData;
    }
}