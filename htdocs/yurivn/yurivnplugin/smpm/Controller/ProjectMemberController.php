<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class ProjectMemberController extends AncestorController
{
    protected $_model;

    public function __construct()
    {
        $this->_model = getModel('ProjectMember');
        parent::__construct();
    }

    public function applicationAction()
    {
        $forum = callHelper('Forum');
        $leader = getModel('Member')->load($forum->getUserInfo('userid'), 'userid')->getId();

        $applications = $this->_model->getApplications($leader);

        $dataView['pageTitle'] = 'Danh sách đăng ký tham gia project';
        $dataView['applications'] = $applications;
        $dataView['leader'] = $leader;
        $this->renderView('list', $dataView);
    }

    public function allApplicationAction()
    {
        $forum = callHelper('Forum');
        $userId = $forum->getUserInfo('userid');
        if(!$userId == $forum->getConfig('SpecialUsers/superadministrators', 2)) {
            print_no_permission();
        }

        $applications = $this->_model->getApplications();

        $dataView['pageTitle'] = 'Danh sách đăng ký tham gia project';
        $dataView['applications'] = $applications;
        $this->renderView('list', $dataView);
    }

    public function addAction()
    {
        $forum = callHelper('Forum');
        $userId = $forum->getUserInfo('userid');
        $project = getModel('Project')->load(getParam('projectid'));
        if(!$userId == $project->getLeaderUserId() && !$userId == $forum->getConfig('SpecialUsers/superadministrators', 2) && getParam('apply')!=1) {
            print_no_permission();
        }
        if(!$project->getId()) {
            die('Project không tồn tại');
        }

        if(getParam('apply')==1) {
            $dataView['pageTitle'] = 'Đăng ký tham gia Project '.$project->getName();
            $dataView['formButton'] = 'Đăng ký';
        } else {
            $dataView['pageTitle'] = 'Thêm Thành viên cho Project '.$project->getName();
            $dataView['formButton'] = 'Thêm';
        }
        $dataView['project'] = $project;

        if(isPost()) {
            $inputData = $this->convertData(getPost());
            if(getParam('apply')==1) {
                $inputData['memberid'] = getModel('Member')->load($forum->getUserInfo('userid'), 'userid')->getId();
            }

            $filter = array(
                'projectid' => getParam('projectid'),
                'memberid' => $inputData['memberid']
            );
            if(getModel('ProjectMember')->setFilter($filter)->getCollection()->count()) {
                if(getParam('apply')==1) {
                    die('Bạn đã đăng ký tham gia project này, vui lòng kiểm tra lại.
                    Nếu đăng ký của bạn chưa được duyệt, vui lòng liên hệ project leader tại: '.getModel('Member')->load($project->getLeader())->getContact());
                } else {
                    die('Thành viên này đã tham gia Project. Nếu bạn muốn chỉnh sửa vị trí, vui lòng xóa thành viên này đi và thêm lại');
                }
            }

            if(!$inputData['memberid']) {
                die('Thành viên này không tồn tại!');
            }


            $this->_model
                ->setProjectId(getParam('projectid'))
                ->setMemberId($inputData['memberid'])
                ->setPosition($inputData['position']);
            if(getParam('apply')==1) {
                $this->_model->setApplying(1);
            }
            $this->_model->save();

            if(getParam('apply')==1) {
                $registeredPositions = implode(", ", $inputData['position']);
                $forum = callHelper('Forum');
                $subject = 'Đăng ký tham gia Project: '.$project->getName();
                $message = '[B]'.callHelper('Forum')->getUserInfo('username').'[/B] đã đăng ký tham gia Project [B]'.$project->getName().'[/B] ở vị trí: '.
                    "[B]{$registeredPositions}[/B].\n".
                    'Bấm vào [URL="http://yurivn.net/yurivnplugin/smpm/index.php?controller=projectmember&action=application'.
                    '"]Đây[/URL] để xét duyệt bản đăng ký của [B]'.callHelper('Forum')->getUserInfo('username')."[/B].\n".
                    '[B][COLOR="Red"]Lưu ý: Đây là tin nhắn tự động từ hệ thống, vui lòng không reply tin nhắn này.[/COLOR][/B]';

                $forum->sendPM($forum->getConfig('SpecialUsers/superadministrators', 2), array($project->getLeaderUserId()), $subject, $message);

                die('Bạn đã đăng ký thành công! Bản đăng ký của bạn sẽ được '.$project->getLeaderName().
                    ' xem xét và duyệt trong thời gian sớm nhất có thể. Nếu không có phản hồi, hãy liên lạc với '.$project->getLeaderName().
                    ' tại đây: '.getModel('Member')->load($project->getLeader())->getContact());
            } else {
                redirect('yurivnplugin/smpm/index.php?controller=project&action=edit&id='.$project->getId());
            }
        }
        $this->renderView('form', $dataView);
    }

    public function deleteAction() {
        $forum = callHelper('Forum');
        $userId = $forum->getUserInfo('userid');
        $projectMember = getModel('ProjectMember')->load(getParam('id'));
        $project = getModel('Project')->load($projectMember->getProjectId());
        if(!$userId == $project->getLeaderUserId() && !$userId == $forum->getConfig('SpecialUsers/superadministrators', 2)) {
            print_no_permission();
        }
        if(!$projectMember->getId()) {
            die('Không tồn tại thành viên này trong project');
        }

        $projectMember->delete();
        if(getParam('apply') == 1) {
            $subject = 'Đăng ký tham gia Project: '.$project->getName();
            $message = 'Bản đăng ký tham gia Project [B]'.$project->getName().'[/B] của bạn đã bị từ chối. '.
                'Vui lòng liên hệ với '.$forum->getUserInfo('username')." để biết thêm chi tiết. \n".
                '[B][COLOR="Red"]Lưu ý: Đây là tin nhắn tự động từ hệ thống, vui lòng không reply tin nhắn này.[/COLOR][/B]';
            $recipient = getModel('Member')->load($projectMember->getMemberId())->getUserId();
            $forum->sendPM($forum->getConfig('SpecialUsers/superadministrators', 2), array($recipient), $subject, $message);

            redirect('yurivnplugin/smpm/index.php?controller=projectmember&action=application');
        }
        redirect('yurivnplugin/smpm/index.php?controller=project&action=edit&id='.$project->getId());
    }

    public function approveAction() {
        $forum = callHelper('Forum');
        $userId = $forum->getUserInfo('userid');
        $projectMember = $this->_model->load(getParam('id'));
        $project = getModel('Project')->load($projectMember->getProjectId());
        if(!$userId == $project->getLeaderUserId() && !$userId == $forum->getConfig('SpecialUsers/superadministrators', 2)) {
            print_no_permission();
        }

        $projectMember->setApplying(0)->save();
        $openPositions = $project->getPosition();
        foreach($projectMember->getPosition() as $registeredPosition) {
            if(array_search($registeredPosition, $openPositions) !== false) {
                unset($openPositions[array_search($registeredPosition, $openPositions)]);
            }
        }
        $project->setPosition($openPositions);
        if(empty($openPositions)) {
            $project->setStatus(1);
        }
        $project->save();

        $forum = callHelper('Forum');
        $subject = 'Đăng ký tham gia Project: '.$project->getName();
        $message = 'Bản đăng ký tham gia Project [B]'.$project->getName().'[/B] của bạn đã được chấp nhận! '.
            "Project leader sẽ liên hệ với bạn trong thời gian sớm nhất có thể.\n".
            '[B][COLOR="Red"]Lưu ý: Đây là tin nhắn tự động từ hệ thống, vui lòng không reply tin nhắn này.[/COLOR][/B]';
        $recipient = getModel('Member')->load($projectMember->getMemberId())->getUserId();
        $forum->sendPM($forum->getConfig('SpecialUsers/superadministrators', 2), array($recipient), $subject, $message);

        redirect('yurivnplugin/smpm/index.php?controller=projectmember&action=application');
    }

    protected function convertData($rawData)
    {
        $convertedData = $rawData;
        $userId = getModel('ForumUser')->load(trim($rawData['membername']), 'username')->getUserId();
        $convertedData['memberid'] = getModel('Member')->load($userId,'userid')->getId();
        return $convertedData;
    }
}