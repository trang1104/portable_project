<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class MemberController extends AncestorController
{
    protected $_model;

    public function __construct()
    {
        $this->_model = getModel('Member');
        parent::__construct();
    }

    public function addAction()
    {
        $dataView['pageTitle'] = 'Thêm thành viên';
        $dataView['formButton'] = 'Thêm';
        $forum = callHelper('Forum');
        if($forum->getUserInfo('userid') != $forum->getConfig('SpecialUsers/superadministrators', 2)) {
            print_no_permission();
        }

        if(isPost()) {
            $inputData = $this->convertData(getPost());
            if(!$inputData['userid']) {
                die('Không tồn tại thành viên này!');
            }
            $this->_model
                ->setUserId($inputData['userid'])
                ->setEmail($inputData['email'])
                ->setContact($inputData['contact'])
                ->setPosition($inputData['position'])
                ->setRank($inputData['rank'])
                ->save();
            die('Thêm thành viên thành công!');
        }
        $this->renderView('form', $dataView);
    }

    public function editAction()
    {
        $dataView['pageTitle'] = 'Sửa thành viên';
        $dataView['formButton'] = 'Sửa';
        $member = $this->_model->load(getParam('id'));
        $form = callHelper('Form');
        $forum = callHelper('Forum');

        if($forum->getUserInfo('userid') != $forum->getConfig('SpecialUsers/superadministrators', 2)) {
            print_no_permission();
        }

        $formData = $member->getData();
        $formData['name'] = $member->getName();
        $form->setFormData($formData);
        if(isPost()) {
            $inputData = $this->convertData(getPost());
            if(!$inputData['userid']) {
                die('Không tồn tại thành viên này!');
            }
            $member
                ->setUserId($inputData['userid'])
                ->setEmail($inputData['email'])
                ->setContact($inputData['contact'])
                ->setPosition($inputData['position'])
                ->setRank($inputData['rank'])
                ->save();
            die('Sửa thành viên thành công!');
        }
        $this->renderView('form', $dataView);
    }

    public function listAction()
    {
        $members = $this->_model->getCollection();
        $dataView['pageTitle'] = 'Danh sách thành viên';
        $dataView['members'] = $members;
        $this->renderView('list', $dataView);
    }

    public function deleteAction() {
        $forum = callHelper('Forum');
        $userId = $forum->getUserInfo('userid');
        $member = getModel('Member')->load(getParam('id'));
        if(!$userId == $forum->getConfig('SpecialUsers/superadministrators', 2)) {
            print_no_permission();
        }
        if(!$member->getId()) {
            die('Không tồn tại thành viên này');
        }

        $member->delete();

        redirect('yurivnplugin/smpm/index.php?controller=member&action=list');
    }

    protected function convertData($rawData)
    {
        $convertedData = $rawData;
        $convertedData['userid'] = getModel('ForumUser')->load(trim($rawData['name']), 'username')->getUserId();
        return $convertedData;
    }
}