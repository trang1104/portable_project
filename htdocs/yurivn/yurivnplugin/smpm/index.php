<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/26/14
 * Time: 8:28 PM
 */
require_once('AncestorClasses/Singleton.php');
require_once('library/basic.php');
//require_once('library/plugin.php');

$currentDir = getcwd();
chdir('../../');
require_once('./global.php');
require_once(DIR . '/includes/functions_user.php');
require_once(DIR . '/includes/functions_threadmanage.php');
require_once(DIR . '/includes/adminfunctions.php');
require_once(DIR . '/includes/functions_databuild.php');
require_once(DIR . '/includes/functions_misc.php');
chdir($currentDir);

$controllerName = getParam('controller');
$actionName = getParam('action');

if(!$controllerName) {
    $controllerName = 'project';
    $actionName = 'list';
}

if($controllerName != 'project' || $actionName != 'list') {
    $forum = callHelper('Forum');
    if(!getModel('Member')->load($forum->getUserInfo('userid'), 'userid')->getId()) {
        print_no_permission();
    }
}

if($controllerName && $actionName) {
    $controller = callController($controllerName);
    $action = $actionName.'Action';
    $controller->$action();
}