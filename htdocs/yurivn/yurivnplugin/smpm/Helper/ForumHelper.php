<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/10/15
 * Time: 8:17 PM
 */
class ForumHelper extends Singleton
{
    protected $_vbulletin;
    protected $_vbphrase;

    public function __construct()
    {
        global $vbulletin, $vbphrase;
        $this->_vbulletin = $vbulletin;
        $this->_vbphrase = $vbphrase;
    }

    public function getUserInfo($field = '')
    {
        if($field) {
            return $this->_vbulletin->userinfo[$field];
        } else {
            return $this->_vbulletin->userinfo;
        }

    }

    public function getConfig($configName, $type = 1)
    {
        if($type == 2) {
            $configNames = explode('/', $configName);
            $configValue = $this->_vbulletin->config;
            foreach($configNames as $configName) {
                $configValue = $configValue[$configName];
            }
            return $configValue;
        }
        return $this->_vbulletin->options[$configName];
    }

    public function getPhrase($phraseName)
    {
        return $this->_vbphrase[$phraseName];
    }

    public function getConvertedPhrase($phraseName)
    {
        $convertedPhrase = str_replace(' ', '_', $this->_vbphrase[$phraseName]);
        return $convertedPhrase;
    }

    public function deConvertPhrase($convertedPhrase)
    {
        $deConvertedPhrase = str_replace('_', ' ', $convertedPhrase);
        return $deConvertedPhrase;
    }

    public function getModerators($forumId, $includeSuperMod = 0)
    {
        $db = getHelper('Database', 'moderator');

        if($includeSuperMod) {
            $results = $db->fetchAll("SELECT `userid` FROM `moderator` WHERE `forumid` = $forumId OR `forumid` = -1");
        } else {
            $results = $db->fetchAll("SELECT `userid` FROM `moderator` WHERE `forumid` = $forumId");
        }

        $moderators = array();
        foreach($results as $result) {
            $moderators[] = $result['userid'];
        }
        return $moderators;
    }

    public function sendPM($sender, $recipients, $subject, $message)
    {
        $no = 0;
        $yes = 1;
        $pmdm =& datamanager_init('PM', $this->_vbulletin, ERRTYPE_ARRAY);

        $pmdm->set_info('savecopy',      0);
        $pmdm->set_info('receipt',       0);
        $pmdm->set_info('cantrackpm',    1);
        $pmdm->set_info('forward',       0);
        $pmdm->set_info('bccrecipients', 0);
        if ($this->_vbulletin->userinfo['permissions']['adminpermissions'] & $this->_vbulletin->bf_ugp_adminpermissions['cancontrolpanel'])
        {
            $pmdm->overridequota = true;
        }

        $userModel = getModel('ForumUser');
        $senderName = $userModel->load($sender)->getUserName();
        $recipientNames = array();
        foreach($recipients as $recipient) {
            $recipientNames[] = $userModel->load($recipient)->getUserName();
        }
        $recipientNames = implode(';', $recipientNames);

        $pmdm->set('fromuserid', $sender);
        $pmdm->set('fromusername', $senderName);
        $pmdm->setr('title', $subject);
        $pmdm->set_recipients($recipientNames, $yes, 'cc');
        $pmdm->setr('message', $message);
        $pmdm->setr('iconid', $no);
        $pmdm->set('dateline', TIMENOW);
        $pmdm->setr('showsignature', $no);
        $pmdm->set('allowsmilie', 1);

        $pmdm->save();
    }

    public function saveThread($threadData)
    {
        $threadMan =& datamanager_init('Thread_FirstPost', $this->_vbulletin, ERRTYPE_ARRAY, 'threadpost');
        $forumInfo = fetch_foruminfo($threadData['forumId']);

        $threadInfo = fetch_threadinfo($threadData['threadId']);
        if($threadInfo) {
            $threadMan->set_existing($threadInfo);
        }

        $threadMan->set_info('forum', $forumInfo);
        $threadMan->set_info('thread', $threadInfo);
        $threadMan->setr('forumid', $threadData['forumId']);
        $threadMan->setr('userid', $threadData['userId']);
        $threadMan->setr('title', $threadData['title']);
        $threadMan->setr('pagetext', $threadData['pageText']);

        $threadMan->setr('showsignature', $signature);
        $threadMan->set('allowsmilie', $threadData['allowSmilie']);
        $threadMan->set('visible', $threadData['visible']);
        $threadMan->set_info('parseurl', $threadData['parseUrl']);
        $threadMan->set('prefixid', $threadData['prefixId']);

        $threadMan->save();
        rebuildForum($threadData['forumId']);
    }

    function rebuildForum($parentid){
        $db = $this->_vbulletin->db;
        $forums = $db->query_read("
		SELECT forumid
		FROM " . TABLE_PREFIX . "forum
		WHERE parentid = $parentid OR forumid = $parentid
		ORDER BY forumid"
        );


        while ($forum = $db->fetch_array($forums))
        {
            build_forum_counters($forum['forumid'], true);
            vbflush();
        }

        // and finally rebuild the forumcache
        unset($forumarraycache, $this->_vbulletin->forumcache);
        build_forum_permissions();
    }
}