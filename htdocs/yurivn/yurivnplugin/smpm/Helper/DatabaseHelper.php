<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/26/14
 * Time: 8:32 PM
 */

class DatabaseHelper extends Singleton
{
    protected $_db;
    protected $_table;

    public function __construct($tableName = '')
    {
        $currentUrl = getcwd();
        chdir('../../');
        require_once('./global.php');
        global $vbulletin;
        chdir($currentUrl);

        $this->_db = $vbulletin->db;
        if(!empty($tableName)) {
            $this->_table = $tableName;
        }

    }

    public function insert_id()
    {
        return $this->_db->insert_id();
    }

    public function fetchOnce($sql)
    {
        $result = array();
        $resource = $this->_db->query_read($sql);
        if($resource) {
            $result = $resource->fetch_assoc();
        }
        return $result;
    }

    public function fetchAll($query)
    {
        $result = $this->_db->query_read($query);
        $data = array();
        if(!empty($result)) {
            while($row = $result->fetch_assoc()){
                $data[] = $row;
            }
        }

        return $data;
    }

    public function insert($data)
    {
        $table_field = array();
        $table_value = array();
        foreach($data as $field => $value){
            $table_field[] = "`$field`";
            $table_value[] = "'".$this->_db->escape_string($value)."'";
        }
        $table_field = implode(",", $table_field);
        $table_value = implode(",", $table_value);
        $query= "INSERT INTO `$this->_table`($table_field) VALUES($table_value)";
        $this->_db->query_write($query);
    }

    public function update($data, $condition)
    {
        if(!empty($data)) {
            $table_newinfo = array();
            foreach($data as $field => $value){
                $table_newinfo[]="`$field` = '".$this->_db->escape_string($value)."'";
            }
            $table_newinfo = implode(",", $table_newinfo);
            $query= "UPDATE `$this->_table` SET $table_newinfo WHERE $condition";
            $this->_db->query_write($query);
        }
    }

    public function delete($condition = '')
    {
        if($condition) {
            $query = "DELETE FROM `$this->_table` WHERE $condition";
        } else {
            $query = "DELETE FROM `$this->_table`";
        }
        $this->_db->query_write($query);
    }

    public function numRows($query)
    {
        $queryResult = $this->_db->query_read($query);
        return $this->_db->num_rows($queryResult);
    }

    public function getTotalItem()
    {
        $query = "SELECT COUNT(*) as `1` FROM `$this->_table`";
        $result = $this->fetchOnce($query);
        return $result[1];

    }

    public function getColumns(){
        $sql = "DESCRIBE {$this->_table}";
        $resource = $this->_db->query($sql);
        while ($x = $resource->fetch_assoc()){
            $columns[] = $x['Field'];
        }
        return $columns;
    }
}