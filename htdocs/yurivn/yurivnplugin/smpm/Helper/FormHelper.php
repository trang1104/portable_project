<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/5/15
 * Time: 12:18 AM
 */
class FormHelper extends Singleton
{
    public $_rule = array();
    protected $_errorMsg = '';
    protected $_error = array();
    protected $_msg = array();

    public function __construct()
    {
        $forum = callHelper('Forum');
        $this->_msg = array(
            'noblank' => $forum->getPhrase('yf_msg_noblank'),
            'isnumber' => $forum->getPhrase('yf_msg_isnumber'),
            'ispositive' => $forum->getPhrase('yf_msg_ispositive'),
            'isinteger' => $forum->getPhrase('yf_msg_isinteger'),
            'isurl' => $forum->getPhrase('yf_msg_isurl'),
            'isurls' => 'Một vài link có định dạng không đúng',
            'ispower2' => 'Giá trị nhập vào phải là lũy thừa của 2',
            'other' => $forum->getPhrase('yf_msg_other'),
            'array_other' => $forum->getPhrase('yf_msg_other'),
        );
    }

    public function validate()
    {
        $forum = callHelper('Forum');
        $this->setFormData();

        foreach($this->_rule as $field => $ruleInfo) {
            $fieldLabel = $ruleInfo[0];
            $conditions = explode(' ', $ruleInfo[1]);
            if(in_array('trim', $conditions)) {
                $inputValue = trim(getPost($field));
            } else {
                $inputValue = getPost($field);
            }

            foreach($conditions as $condition) {

                if(strpos($condition, 'ud_') === 0) {
                    $function = substr($condition,3);
                    if(!callController(getParam('controller'))->$function($inputValue)){
                        $this->_errorMsg .= "\n - $fieldLabel: {$this->_msg[$function]}";
                        $this->_error[$field] = $this->_msg[$function];
                    }
                } elseif(strpos($condition, '[') !== false) {
                    preg_match('/\[(.*?)\]/', $condition, $strInBracket);
                    $params = explode(',', $strInBracket[1]);
                    $condition = trim(preg_replace('/\s*\[[^)]*\]/', '', $condition));

                    switch($condition) {
                        case 'other':
                            $inputValues = explode(';', $inputValue);
                            if(count($inputValues) > 1) {
                                foreach($inputValues as $inputValue) {
                                    foreach($params as $param) {
                                        list($paramName, $paramLabel) = explode('|', $param);
                                        $paramLabel = $forum->deConvertPhrase($paramLabel);
                                        if($inputValue === getPost($paramName)) {
                                            $this->_errorMsg .= "\n - $fieldLabel: ". construct_phrase($this->_msg['other'], $paramLabel);
                                            $this->_error[$field] = construct_phrase($this->_msg['other'], $paramLabel);
                                            break 3;
                                        }
                                    }
                                }
                            }
                            break;
                    }
                } else {
                    switch($condition) {
                        case 'noblank':
                            if ($inputValue == null || $inputValue == "") {
                                $this->_errorMsg .= "\n - $fieldLabel: {$this->_msg['noblank']}";
                                $this->_error[$field] = $this->_msg['noblank'];
                            }
                            break;
                        case 'isnumber':
                            if (!is_numeric($inputValue)) {
                                $this->_errorMsg .= "\n - $fieldLabel: {$this->_msg['isnumber']}";
                                $this->_error[$field] = $this->_msg['isnumber'];
                            }
                            break;

                        case 'isinteger':
                            if (!is_numeric($inputValue) || floor($inputValue) != $inputValue) {
                                $this->_errorMsg .= "\n - $fieldLabel: {$this->_msg['isinteger']}";
                                $this->_error[$field] = $this->_msg['isinteger'];
                            }
                            break;

                        case 'ispositive':
                            if (!is_numeric($inputValue) || $inputValue <= 0) {
                                $this->_errorMsg .= "\n - $fieldLabel: {$this->_msg['ispositive']}";
                                $this->_error[$field] = $this->_msg['ispositive'];
                            }
                            break;

                        case 'isurl':
                            if(filter_var($inputValue, FILTER_VALIDATE_URL) === false && $inputValue != '') {
                                $this->_errorMsg .= "\n - $fieldLabel: {$this->_msg['isurl']}";
                                $this->_error[$field] = $this->_msg['isurl'];
                            }
                            break;

                        case 'isurls':
                            $inputItems = explode("\n", $inputValue);
                            foreach($inputItems as $inputItem) {
                                if(!filter_var(trim($inputItem), FILTER_VALIDATE_URL)) {
                                    $this->_errorMsg .= "\n - $fieldLabel: {$this->_msg['isurls']}";
                                    $this->_error[$field] = $this->_msg['isurls'];
                                    break;
                                }
                            }
                            break;

                        case 'ispower2':
                            if(($inputValue & ($inputValue - 1)) != 0) {
                                $this->_errorMsg .= "\n - $fieldLabel: {$this->_msg['ispower2']}";
                                $this->_error[$field] = $this->_msg['ispower2'];
                            }
                    }
                }

            }
        }

        if($this->_errorMsg) {
            $this->_errorMsg = nl2br('Có lỗi xảy ra, hãy kiểm tra lại các thông tin sau:' . $this->_errorMsg);
            return false;
        } else {
            return true;
        }

    }

    public function setFormData($datas = array())
    {
        $currentController = callController(getParam('controller'));
        if(empty($datas)) {
            $datas = getPost();
        } else

        $additionalDataView = array();
        foreach($datas as $key => $data) {
            $additionalDataView['formValue'][$key] = $data;
            if(is_string($data)) {
                $additionalDataView['formSelect'][$key][$data] = 'Selected';
                $additionalDataView['formCheck'][$key][$data] = 'Checked';
            } elseif(is_array($data)) {
                foreach($data as $itemData) {
                    $additionalDataView['formCheck'][$key][$itemData] = 'Checked';
                }
            }

        }

        $currentController->setAdditionalDataView($additionalDataView);
    }

    public function addMsg($msg)
    {
        $this->_msg += $msg;
    }

    public function getError($field)
    {
        if(isset($this->_error[$field])) {
            return $this->_error[$field];
        }

    }

    public function getErrorMsg()
    {
        return $this->_errorMsg;
    }

    public function setRule($rule)
    {
        $this->_rule = $rule;
    }

    public function addRule($newRule)
    {
        $this->_rule += $newRule;
    }

    public function unsetRule($rule)
    {
        if(is_array($rule)) {
            foreach ($rule as $singleRule) {
                unset($this->_rule[$singleRule]);
            }
        } else {
            unset($this->_rule[$rule]);
        }

    }

    public function getRule()
    {
        return $this->_rule;
    }
}
