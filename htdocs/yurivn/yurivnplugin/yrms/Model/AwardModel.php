<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/26/15
 * Time: 1:46 PM
 */
class AwardModel extends AncestorModel
{
    protected $_tableName = 'yrms_award';
    protected $_serializeValues = array('content');
    protected $_originalContent;
    protected $_earnedAward = array();

    public function setContent($content)
    {
        if($this->getContent()) {
            $this->_originalContent = $this->getContent();
        }
        return parent::setContent($content);
    }

    public function load($value, $columnToSelect = '')
    {
        parent::load($value, $columnToSelect);
        $this->_originalContent = $this->getContent();
        return $this;
    }

    public function save()
    {
        parent::save();
        $this->_earnedAward = array();
        if($this->_originalContent != $this->getContent()) {
            if(!empty($this->_originalContent)) {
                $this->apply('minus', $this->_originalContent);
            }
            $this->apply('plus', $this->getContent());
        }
    }

    public function delete()
    {
        $this->apply('minus', $this->getContent());
        parent::delete();
    }

    public function softDelete()
    {
        $this->apply('minus', $this->getContent());
        $this->setActive(0)->save();
    }

    public function restore()
    {
        $this->apply('plus', $this->getContent());
        $this->setActive(1)->save();
    }

    protected function apply($rule, $awardContent)
    {
        if(!empty($awardContent)) {
            switch($rule){
                case 'plus':
                    foreach($awardContent as $userId => $amount) {
                        $user = callModel('ForumUser')->load($userId);
                        $newYun = $user->getYun() + $amount;
                        $user->setYun($newYun)->save();
                        $this->_earnedAward[$userId] = $this->_earnedAward[$userId] + $amount;
                    }
                    break;
                case 'minus':
                    foreach($awardContent as $userId => $amount) {
                        $user = callModel('ForumUser')->load($userId);
                        $newYun = $user->getYun() - $amount;
                        $user->setYun($newYun)->save();
                        $this->_earnedAward[$userId] = $this->_earnedAward[$userId] - $amount;
                    }
                    break;
            }
        }

    }

    public function convertData($rawData)
    {
        $convertedData = array();
        foreach($rawData as $userIds => $amount) {
            $userIds = explode(';', $userIds);
            foreach($userIds as $userId) {
                $convertedData = array_mesh($convertedData, array($userId => ceil($amount/count($userIds))));
            }
        }

        $userModel = callModel('ForumUser');
        foreach($convertedData as $userId => $amount) {
            if(!$userModel->load($userId)->getUserId()) {
                unset($convertedData[$userId]);
            }
        }
        
        return $convertedData;
    }

    public function getEarnedAward()
    {
        $this->_earnedAward = array_filter($this->_earnedAward);
        return $this->_earnedAward;
    }
}