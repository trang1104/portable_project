<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/22/15
 * Time: 3:55 PM
 */
class VietsubmangaMangaModel extends AncestorModel
{
    protected $_tableName =  'yrms_vietsubmanga_manga';
    protected $_serializeValues = array('hostlist', 'fansubname', 'fansubsite', 'fansubmember');
    protected $_specialProperties = array(
        'thread' => 'return getModel("ForumThread")->loadThread($this->getThreadId());',
        'award' => 'return getModel("Award")->load($this->getThread()->getFirstPostId(), "postid");'
    );

    public function getCollectionByOwner($owner)
    {
        $collection = $this->getCollection();
        foreach($collection as $key => $item) {
            if(!$item->isOwner($owner)) {
                unset($collection->$key);
            }
        }
        return $collection;
    }

    public function save()
    {
        $mangaJustAdded = ($this->getId()) ? 0 : 1;
        parent::save();
        $forum = callHelper('Forum');
        $postUserId = ($this->getThread()->getPostUserId()) ? $this->getThread()->getPostUserId() : $forum->getUserInfo('userid');
        $threadInfo = $this->buildPostContent();

        $this->getThread()
            ->setForumId($forum->getConfig('yrms_vietsubmanga_id_truyendich'))
            ->setPostUserId($postUserId)
            ->setPrefixId($threadInfo['prefixid'])
            ->setTitle($threadInfo['title'])
            ->setPageText($threadInfo['pagetext'])
            ->setAllowSmilie($threadInfo['allowsmilie'])
            ->setParseUrl($threadInfo['parseurl'])
            ->setVisible($threadInfo['visible'])
            ->setTaglist($threadInfo['taglist'])
            ->saveThread();

        callModel('ForumPost')->load($this->getThread()->getFirstPostId())->setYrmsPost(1)->save();

        if($mangaJustAdded) {
            $this->setThreadId($this->getThread()->getThreadId())->setPostId($this->getThread()->getFirstPostId());
            parent::save();

            $awardContent = array(
                $postUserId => $forum->getConfig('yrms_vietsubmanga_yun_newproject')
            );
            $this->getAward()
                ->setPostId($this->getThread()->getFirstPostId())
                ->setContent($awardContent)
                ->setResourceId($this->getId())
                ->setResourceType('vietsubmanga')
                ->save();

            $this->setEarnedAward($forum->getConfig('yrms_vietsubmanga_yun_newproject'));
        }

        return true;
    }

    public function delete()
    {
        $chapters = getModel('VietsubmangaChapter')->setFilter('mangaid', $this->getId())->getCollection();
        foreach($chapters as $chapter) {
            $chapter->delete();
        }
        $this->getThread()->delete();
        $this->getAward()->delete();
        parent::delete();
        return true;
    }

    public function softDelete()
    {

        $chapters = getModel('VietsubmangaChapter')->setFilter('mangaid', $this->getId())->getCollection();
        foreach($chapters as $chapter) {
            $chapter->softDelete();
        }
        $this->getThread()->softDelete();
        $this->getAward()->softDelete();
        $this->setActive(0);

        parent::save();
        return true;
    }

    public function restore()
    {
        $chapters = getModel('VietsubmangaChapter')->setFilter('mangaid', $this->getId())->getCollection();
        foreach($chapters as $chapter) {
            $chapter->restore();
        }
        $this->getThread()->restore();
        $this->getAward()->restore();
        $this->setActive(1);
        parent::save();
        return true;
    }

    protected function buildPostContent()
    {
        $fetchedData = $this->fetchData();

        //region Post title
        $forum = callHelper('Forum');
        $postInfo['prefixid'] = $fetchedData['type'];

        $postInfo['title'] = '';
        if($this->getFansubName()) {
            $postInfo['title'] .= "[".$fetchedData['fansubname']."] ";
        }
        if($this->getType() == 3 && $this->getOriginalComposition()) {
            $postInfo['title'] .= "[".$this->getOriginalComposition()."] ";
        }
        $postInfo['title'] .= $this->getTitle();
        $postInfo['allowsmilie'] = 1;
        $postInfo['visible'] = 1;
        $postInfo['parseurl'] = 1;

        //endregion

        //region Taglist
        $tagList = array();
        setIfValExist($tagList, $this->getFansubName());
        setIfValExist($tagList, $this->getOriginalComposition());
        setIfValExist($tagList, $fetchedData['type']);
        setIfValExist($tagList, $this->getTitle());
        setIfValExist($tagList, $fetchedData['othertitle']);
        if($this->getAuthor()) {
            $tagList[] = $fetchedData['author'];
        }
        setIfValExist($tagList, $fetchedData['genre']);

        $postInfo['taglist'] = implode(',', $tagList);
        //endregion

        //region Manga_info
        $mangaInfo = construct_phrase($forum->getPhrase('yrms_postformat_highlightvalue'), $forum->getPhrase('yrms_mangatitle'), $fetchedData['title'])."\n";
        if ($this->getOtherTitle()) {
            $mangaInfo .= construct_phrase($forum->getPhrase('yrms_postformat_highlightvalue'), $forum->getPhrase('yrms_othertitle'), $fetchedData['othertitle'])."\n";
        }
        $mangaInfo .= construct_phrase($forum->getPhrase('yrms_postformat_highlightvalue'), $forum->getPhrase('yrms_author'), $fetchedData['author'])."\n"
            .construct_phrase($forum->getPhrase('yrms_postformat_highlightvalue'), $forum->getPhrase('yrms_genre'), $fetchedData['genre'])."\n"
            .construct_phrase($forum->getPhrase('yrms_postformat_highlightvalue'), $forum->getPhrase('yrms_totalchapter'), $fetchedData['finishedchapter'].'/'.$fetchedData['totalchapter'])."\n"
            .construct_phrase($forum->getPhrase('yrms_postformat_highlightvalue'), $forum->getPhrase('yrms_projectstatus'), $fetchedData['status'])."\n";

        $fansubInfo = '';

        foreach($this->getFansubMember() as $position => $members) {
            foreach($members as $key => $member) {
                $members[$key] = callModel('ForumUser')->load($member)->getUserName();
            }
            $position = ($position == 'editor') ? 'mangaeditor' : $position;
            $fansubInfo .= construct_phrase($forum->getPhrase('yrms_postformat_highlightvalue'), $forum->getPhrase('yrms_'.$position), implode(', ', $members))."\n";
        }

        $fansubInfo .= construct_phrase($forum->getPhrase('yrms_postformat_highlightvalue'), $forum->getPhrase('yrms_fansub_website'), $fetchedData['fansubsite']);
        //endregion

        $linkformat = $this->buildLinkFormat();
        if(!$linkformat){
            $linkformat = $forum->getPhrase('yrms_tobeupdated');
        }

        //build post
        $postInfo['pagetext']= construct_phrase($forum->getPhrase('yrms_postformat_vietsubmanga'),
            $forum->getConfig('yrms_main_imagewidth'),
            $fetchedData['image'],
            $mangaInfo,
            $fetchedData['summary'],
            $fetchedData['fansubname'],
            $fansubInfo,
            $fetchedData['fansubnote'],
            $linkformat);

        return $postInfo;
    }

    protected function buildLinkFormat()
    {
        $forum = callHelper('Forum');
        $hostList = $this->getHostList();
        if(empty($hostList)) {
            return '';
        }

        $header = '';
        $isSplitted = (array_sum($hostList) > count($hostList)) ? 1 : 0;
        foreach($hostList as $name => $quantity){
            if($name=='stream') {
                $header = ($isSplitted) ? '[TH=rowspan=2|]'.$forum->getPhrase('yrms_readlink').'[/TH]'. $header : '[TH]'.$forum->getPhrase('yrms_readlink').'[/TH]' . $header;
            } else {
                if ($isSplitted) {
                    $header .= ($quantity > 1) ? "[TH=colspan=$quantity|]".$name.'[/TH]' : '[TH=rowspan=2|]'.$name.'[/TH]';;
                } else {
                    $header .='[TH]'.$name.'[/TH]';
                }
            }
        }
        if(!isset($hostList['stream'])) {
            $header = ($isSplitted) ? '[TH=rowspan=2|][/TH]'. $header : '[TH][/TH]' . $header;
        }
        $header = '[TR]'.$header.'[/TR]';

        $header2 = '';
        if($isSplitted) {
            foreach($hostList as $quantity){
                if($quantity > 1) {
                    for($i=1; $i <= $quantity; $i++) {
                        $header2 .= '[TH]'.$forum->getPhrase('yrms_part').$i.'[/TH]';
                    }
                }
            }
        }
        $header2 = '[TR]'.$header2.'[/TR]';

        $body = '';
        $filter = array(
            'mangaid' => $this->getId(),
            'active' => 1
        );
        $chapters = getModel('VietsubmangaChapter')->setFilter($filter)->setOrder(array('type', 'code'))->getCollection();
        foreach($chapters as $chapter){
            $row = '';
            $streamThreadId = $chapter->getStreamThreadId();
            $streamThreadInfo = fetch_threadinfo($streamThreadId);
            $chapterDownloadLink = $chapter->getAllDownloadLink();
            $chapterStreamLink = '';
            foreach($chapter->getAllStreamLink() as $streamLink) {
                if($streamLink) {
                    $chapterStreamLink = $streamLink;
                    break;
                }
            }
            $row.= ($chapterStreamLink)
                ? '[TD][URL="'.fetch_full_seo_url('thread', $streamThreadInfo).'"]'.$forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode().'[/URL][/TD]'
                : '[TD]'.$forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode().'[/TD]';
            if($chapter->getAdult()) {
                $downloadPostId = $chapter->getPostId();
                $downloadPostInfo = fetch_postinfo($downloadPostId);
                $colsp = ($hostList['stream']) ? array_sum($hostList) - 1 : array_sum($hostList);
                $row.= '[TD=colspan='.$colsp.'|][URL="'.fetch_full_seo_url('post', $downloadPostInfo).'"]'.$forum->getPhrase('yrms_download').'[/URL][/TD]';
            } else {
                foreach($hostList as $host => $quantity){
                    if($host == 'stream') {
                        continue;
                    }
                    $totalLink = $remainingLinks = count($chapterDownloadLink[$host]);
                    $totalUsedColsp = 0;
                    $linkOrder = 1;
                    if($chapterDownloadLink[$host]) {
                        foreach($chapterDownloadLink[$host] as $link) {
                            if($quantity > $totalLink) {
                                $colsp = ($quantity - $totalUsedColsp) / $remainingLinks;
                                if($colsp == $quantity) {
                                    $row .= '[TD=colspan='.$colsp.'|][URL="'.$link.'"]'.$forum->getPhrase('yrms_download').'[/URL][/TD]';
                                } else {
                                    $row .= ($colsp > 1)
                                        ? '[TD=colspan='.ceil($colsp).'|][URL="'.$link.'"]'.$forum->getPhrase('yrms_part').$linkOrder.'[/URL][/TD]'
                                        : '[TD][URL="'.$link.'"]'.$forum->getPhrase('yrms_part').$linkOrder.'[/URL][/TD]';
                                }
                                $usedColsp = ceil($colsp);
                            } elseif($totalLink > 1) {
                                $row .= '[TD][URL="'.$link.'"]'.$forum->getPhrase('yrms_part').$linkOrder.'[/URL][/TD]';
                            } else {
                                $row .= '[TD][URL="'.$link.'"]'.$forum->getPhrase('yrms_download').'[/URL][/TD]';
                            }
                            $remainingLinks--;
                            $linkOrder++;
                            $totalUsedColsp += $usedColsp;
                        }
                    } else {
                        $row.= '[TD=colspan='.$quantity.'|][/TD]';
                    }
                }
            }

            $body.= '[TR]'.$row.'[/TR]';
        }

        $linkFormat = '[TABLE=|text-align: center;]'.$header.$header2.$body.'[/TABLE]';
        return $linkFormat;
    }

    public function rebuildInfo()
    {
        $filter = array(
            'mangaid' => $this->getId(),
            'active' => 1
        );
        $chapters = getModel('VietsubmangaChapter')->setFilter($filter)->getCollection();
        $hostList = array();
        $fansubNames = array();
        $fansubSites = array();

        foreach($chapters as $chapter) {
            if($chapter->getDownloadLink()) {
                foreach($chapter->getDownloadLink() as $hostName => $hostLinks) {
                    if($hostList[$hostName] < count($hostLinks)){
                        $hostList[$hostName] = count($hostLinks);
                    }
                }
            }
            if($chapter->getStreamLink()) {
                $hostList['stream'] = 1;
            }

            if($chapter->getFansubName() && !in_array($chapter->getFansubName(), $fansubNames)){
                $fansubNames[] = $chapter->getFansubName();
            }

            if($chapter->getFansubSite() && !in_array($chapter->getFansubSite(), $fansubSites)){
                $fansubSites[] = $chapter->getFansubSite();
            }
        }

        $this->setHostList($hostList)
            ->setFansubName($fansubNames)
            ->setFansubSite($fansubSites)
            ->save();
    }

    public function rebuildInforumList()
    {
        $forum = callHelper('Forum');
        $alphas = range('A', 'Z');
        $fullAlphas = array_merge(array('#'), $alphas);
        $divisions = array();
        $mangaTypes = array(1,2,3);

        foreach($mangaTypes as $mangaType) {
            $filter = array(
                'type' => $mangaType,
                'active' => 1
            );
            $mangas = (array) $this->reset()->setFilter($filter)->getCollection();
            usort($mangas, 'callback_mangaSort');
            $blocks = array();
            foreach($fullAlphas as $currentCharacter) {
                $items = array();
                foreach($mangas as $key => $manga) {
                    $condition = ($currentCharacter == '#')
                        ? (!in_array(unicode_str_filter(strtoupper(mb_substr($manga->getOriginalComposition().$manga->getTitle(), 0, 1, 'utf-8'))), $alphas))
                        : (unicode_str_filter(strtoupper(mb_substr($manga->getOriginalComposition().$manga->getTitle(), 0, 1, 'utf-8'))) == $currentCharacter);
                    if($condition) {
                        $threadId = $manga->getThreadId();
                        $threadInfo = fetch_threadinfo($threadId);
                        $fansubName = ($manga->getFansubName()) ? '['.implode(' + ', $manga->getFansubName()).']' : '';
                        $originalComposition = ($manga->getOriginalComposition()) ? '['.$manga->getOriginalComposition().']' : '';
                        $mangaTitle = $fansubName.$originalComposition.$manga->getTitle();
                        $items[] = construct_phrase($forum->getPhrase('yrms_postformat_vietsubmangalist_item'),
                            fetch_full_seo_url('thread', $threadInfo),
                            $manga->getImage(),
                            $manga->getSummary(),
                            $mangaTitle
                        );
                        unset($mangas[$key]);
                    } else {
                        break;
                    }
                }

                if(!empty($items)) {
                    $blocks[] = construct_phrase($forum->getPhrase('yrms_postformat_vietsubmangalist_block'),
                        $currentCharacter,
                        implode("\n",$items)
                    );
                }
            }

            $divisions[] = ($mangaType == 1)
                ? construct_phrase($forum->getPhrase('yrms_postformat_vietsubmangalist_divisionfirst'),$forum->getPhrase('yrms_mangatype'.$mangaType),implode("\n\n",$blocks))
                : construct_phrase($forum->getPhrase('yrms_postformat_vietsubmangalist_division'),$forum->getPhrase('yrms_mangatype'.$mangaType),implode("\n\n",$blocks));
        }

        $list = construct_phrase($forum->getPhrase('yrms_postformat_vietsubmangalist_wrapper'), implode("\n\n",$divisions));

        $listPostId = getModel("ForumThread")->loadThread($forum->getConfig('yrms_vietsubmanga_id_list'))->getFirstPostId();
        $listPost = getModel("ForumPost")->loadPost($listPostId);
        if($listPost->getPostId()) {
            $listPost->setPageText($list)->savePost();
        }
    }

    public function fetchData()
    {
        $forum = callHelper('Forum');
        $chapterModel = getModel('VietsubmangaChapter');

        $fetchedData = array();
        foreach ($this as $key => $value) {
            if(strpos($key, '_') !== 0) {
                $getKey = "get$key";
                $fetchedData[$key] = $this->$getKey();
            }
        }

        $fetchedData['fansubmember'] = $this->getFansubMember();
        if($this->getTotalChapter() == 0) {
            $fetchedData['totalchapter'] = '??';
        }
        if($this->getOtherTitle()) {
            $fetchedData['othertitle'] = str_replace(';', ',', $this->getOtherTitle());
        }
        if($this->getId()) {
            $fetchedData['finishedchapter'] = $chapterModel->setFilter('mangaid', $this->getId())->getCollection()->count();
        } else {
            $fetchedData['finishedchapter'] = 0;
        }

        if(!empty($fetchedData['fansubname'])) {
            $fetchedData['fansubname'] = implode(' + ', $this->getFansubName());
        } else {
            $fetchedData['fansubname'] = $forum->getPhrase('yrms_unknown');
        }

        if(!empty($fetchedData['fansubsite'])) {
            $fetchedData['fansubsite'] = implode(' + ', $this->getFansubSite());
        } else {
            $fetchedData['fansubsite'] = $forum->getPhrase('yrms_unknown');
        }

        $fetchedData['type'] = $forum->getConfig('yrms_vietsubmanga_prefixid_type'.$this->getType());
        $fetchedData['status'] = $forum->getPhrase('yrms_projectstatus'.$this->getStatus());
//        setIfEmpty($fetchedData['fansubname'], $forum->getPhrase('yrms_unknown'));
//        setIfEmpty($fetchedData['fansubsite'], $forum->getPhrase('yrms_unknown'));

        return $fetchedData;
    }

    public function isOwner($userId = '')
    {
        $forum = callHelper('Forum');
        if(!$userId) {
            $userId = $forum->getUserInfo('userid');
        }
        $mangaPoster = callModel('ForumThread')->load($this->getThreadId())->getPostUserId();
        if($userId == $mangaPoster) {
            return true;
        }
        if(in_array($userId, $this->getOwners())) {
            return true;
        }

        return false;
    }

    public function getOwners()
    {
        $owners = array();
        foreach($this->getFansubMember() as $position => $fansubMembers) {
            foreach($fansubMembers as $fansubMember) {
                if(!in_array($fansubMember, $owners)) {
                    $owners[] = $fansubMember;
                }
            }
        }
        return $owners;
    }
}