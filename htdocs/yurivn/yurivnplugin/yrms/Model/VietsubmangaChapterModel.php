<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/22/15
 * Time: 3:55 PM
 */
class VietsubmangaChapterModel extends AncestorModel
{
    protected $_tableName =  'yrms_vietsubmanga_chapter';
    protected $_serializeValues = array('downloadlink', 'fansubmember');
    protected $_specialProperties = array(
        'manga' => 'return getModel("VietsubmangaManga")->load($this->getMangaId());',
        'downloadpost' => 'return getModel("ForumPost")->loadPost($this->getPostId());',
        'streamthread' => 'return getModel("ForumThread")->loadThread($this->getStreamThreadId());',
        'downloadaward' => 'return getModel("Award")->load($this->getPostId(), "postid");',
        'streamaward' => 'return getModel("Award")->load($this->getStreamThread()->getFirstPostId(), "postid");'
    );

    public function getFansubmember($position = '')
    {
        $fansubmember = parent::getFansubmember();
        if($position) {
            return $fansubmember[$position];
        } else {
            return $fansubmember;
        }
    }

    public function getAllDownloadLink($array = true)
    {
        $filter = array(
            'resourceid' => $this->getId(),
            'resourcetype' => str_replace('Model', '', get_class()),
            'type' => 1
        );
        $additionalLinks = getModel('AdditionalLink')->setFilter($filter)->getCollection();

        if($array) {
            $mergedDownloadLink = $this->getDownloadLink();
            foreach($additionalLinks as $additionalLink) {
                $mergedDownloadLink += $additionalLink->getLink();
            }
        } else {
            $mergedDownloadLink = array($this);
            foreach($additionalLinks as $additionalLink) {
                $mergedDownloadLink[]= $additionalLink;
            }

        }

        return $mergedDownloadLink;
    }

    public function getAllStreamLink($array = true)
    {
        $filter = array(
            'resourceid' => $this->getId(),
            'resourcetype' => str_replace('Model', '', get_class()),
            'type' => 2
        );
        $additionalLinks = getModel('AdditionalLink')->setFilter($filter)->getCollection();

        if($array) {
            $mergedStreamLink = array($this->getStreamLink());
            foreach($additionalLinks as $additionalLink) {
                $mergedStreamLink[] = $additionalLink->getLink();
            }
        } else {
            $mergedStreamLink = array($this);
            foreach($additionalLinks as $additionalLink) {
                $mergedStreamLink[]= $additionalLink;
            }
        }

        return $mergedStreamLink;
    }

    public function loadByMangaCode($mangaId, $code)
    {
        $filter = array(
            'code' => $code,
            'mangaid' => $mangaId

        );
        $this->setMangaId($mangaId);
        $chapterId = $this->setFilter($filter)->getFirstItem()->getId();
        return $this->load($chapterId);
    }

    public function save()
    {
        //region save new chapter and posts in database
        $forum = callHelper('Forum');
        parent::save();

        if($this->getDownloadLink()) {
            $downloadPostUserId = ($this->getDownloadPost()->getPostUserId()) ? $this->getDownloadPost()->getPostUserId() : $forum->getUserInfo('userid');
            $downloadPostInfo = $this->buildPostContent('download');
            $this->getDownloadPost()
                ->loadPost($this->getPostId())
                ->setThreadId($this->getManga()->getThreadId())
                ->setParent($this->getManga()->getPostId())
                ->setPostUserId($downloadPostUserId)
                ->setTitle($downloadPostInfo['title'])
                ->setPageText($downloadPostInfo['pagetext'])
                ->setAllowSmilie($downloadPostInfo['allowsmilie'])
                ->setParseUrl($downloadPostInfo['parseurl'])
                ->setVisible($downloadPostInfo['visible'])
                ->savePost();

            if(!$this->getDownloadPost()->getYrmsPost()) {
                $this->getDownloadPost()->setYrmsPost(1)->save();
            }
        }

        if($this->getStreamLink() || $this->getStreamThreadId()) {
            $streamPostUserId = ($this->getStreamThread()->getPostUserId()) ? $this->getStreamThread()->getPostUserId() : $forum->getUserInfo('userid');
            $streamPostInfo = $this->buildPostContent('stream');
            $this->getStreamThread()
                ->loadThread($this->getStreamThreadId())
                ->setForumId($forum->getConfig('yrms_vietsubmanga_id_doconline'))
                ->setPostUserId($streamPostUserId)
                ->setPrefixId($streamPostInfo['prefixid'])
                ->setTitle($streamPostInfo['title'])
                ->setPageText($streamPostInfo['pagetext'])
                ->setAllowSmilie($streamPostInfo['allowsmilie'])
                ->setParseUrl($streamPostInfo['parseurl'])
                ->setVisible($streamPostInfo['visible'])
                ->setTaglist($streamPostInfo['taglist'])
                ->saveThread();

            $streamPost = getModel('ForumPost')->load($this->getStreamThread()->getFirstPostId());
            if(!$streamPost->getYrmsPost()) {
                $streamPost->setYrmsPost(1)->save();
            }

        }

        $resaveFlag = 0;
        if($this->getPostId() != $this->getDownloadPost()->getPostId()) {
            $this->setPostId($this->getDownloadPost()->getPostId());
            $resaveFlag = 1;
        }
        if($this->getStreamThreadId() != $this->getStreamThread()->getThreadId()) {
            $this->setStreamThreadId($this->getStreamThread()->getThreadId());
            $resaveFlag = 1;
        }
        if($resaveFlag) {
            parent::save();
        }

        //endregion

        //region Reward
        $this->setEarnedAward($this->reward());
        //endregion

        //region Update Manga
        $hostList = $this->getManga()->getHostList();
        if(!emptyEnhanced($this->getAllDownloadLink())) {
            foreach($this->getAllDownloadLink() as $hostName => $hostLinks) {
                if($hostList[$hostName] < count($hostLinks)){
                    $hostList[$hostName] = count($hostLinks);
                }
            }
        }
        if(!emptyEnhanced($this->getAllStreamLink())) {
            $hostList['stream'] = 1;
        }

        $this->getManga()->setHostList($hostList)->save();
        //endregion

    }

    public function delete() {
        if($this->getDownloadAward()) $this->getDownloadAward()->delete();
        if($this->getDownloadPost()) $this->getDownloadPost()->delete();
        if($this->getStreamAward()) $this->getStreamAward()->delete();
        if($this->getStreamThread()) $this->getStreamThread()->delete();
        parent::delete();
        $this->getManga()->rebuildInfo();
        return true;
    }

    public function softDelete() {
        if($this->getDownloadAward()) $this->getDownloadAward()->softDelete();
        if($this->getDownloadPost()) $this->getDownloadPost()->softDelete();
        if($this->getStreamAward()) $this->getStreamAward()->softDelete();
        if($this->getStreamThread()) $this->getStreamThread()->softDelete();
        $this->setActive(0);
        parent::save();

        $this->getManga()->rebuildInfo();
        return true;
    }

    public function restore() {
        $this->getDownloadAward()->restore();
        $this->getDownloadPost()->restore();
        $this->getStreamAward()->restore();
        $this->getStreamThread()->restore();
        $this->setActive(1);
        parent::save();
        $this->getManga()->rebuildInfo();
        return true;
    }

    public function fetchData()
    {
        $forum = callHelper('Forum');

        $fetchedData = array();
        foreach ($this as $key => $value) {
            if(strpos($key, '_') !== 0) {
                $fetchedData[$key] = $value;
            }
        }

        $fetchedData['type'] = $forum->getPhrase('yrms_chaptertype'.$this->getType());

        $fetchedData['streamstatus'] = (!emptyEnhanced(array_filter($this->getAllStreamLink()))) ? $forum->getPhrase('yrms_availablestatus1') : $forum->getPhrase('yrms_availablestatus0');
        $fetchedData['mirrorstatus'] = (count($this->getAllDownloadLink()) > 1) ? $forum->getPhrase('yrms_availablestatus1') : $forum->getPhrase('yrms_availablestatus0');
        $fetchedData['linkstatus'] = (count($this->getAllDownloadLink()) > 0) ? $forum->getPhrase('yrms_linkstatus1') : $forum->getPhrase('yrms_linkstatus0');

        return $fetchedData;
    }

    public function buildPostContent($type)
    {
        $forum = callHelper('Forum');

        if($this->getManga()->getType() == 2 && $this->getType() == 1)
            $postInfo['title'] = $this->getManga()->getTitle();
        else
            $postInfo['title']=$this->getManga()->getTitle().' '. $forum->getPhrase('yrms_chaptertype' . $this->getType()). ' ' . $this->getCode();
        if($this->getTitle())
            $postInfo['title'].= ' - ' . $this->getTitle();

        //other setting
        $postInfo['allowsmilie'] = 1;
        $postInfo['visible'] = 1;
        $postInfo['parseurl'] = 1;

        if($type=='download'){
            if ($this->getAdult()) {
                $postInfo['pagetext'] = construct_phrase($forum->getPhrase('yrms_postformat_updatehide'), $postInfo['title'], $this->getFansubNote(), $this->buildLinkFormat());
            } else {
                $postInfo['pagetext'] = construct_phrase($forum->getPhrase('yrms_postformat_update'), $postInfo['title'], $this->getFansubNote());
            }
        }
        else if ($type == 'stream') {
            foreach($this->getAllStreamLink() as $streamLink) {
                if($streamLink) {
                    if ($this->getAdult()) {
                        $postInfo['pagetext'] = construct_phrase($forum->getPhrase('yrms_postformat_readonlinehide'), $postInfo['title'], $this->getFansubNote(), $streamLink);
                    } else{
                        $postInfo['pagetext'] = construct_phrase($forum->getPhrase('yrms_postformat_readonline'), $postInfo['title'], $this->getFansubNote(), $streamLink);
                    }
                    break;
                } else {
                    $postInfo['pagetext'] = construct_phrase($forum->getPhrase('yrms_postformat_readonline'), $postInfo['title'], $this->getFansubNote(), $forum->getPhrase('yrms_linkstatus0'));
                }
            }
        }

        return $postInfo;
    }

    public function buildLinkFormat()
    {
        $forum = callHelper('Forum');
        $totalHost = count($this->getDownloadLink(), COUNT_RECURSIVE);
        $header = '';
        $header2 = '';
        $body = '';
        $isSplitted = ($totalHost > count($this->getDownloadLink())) ? 1 : 0;
        foreach($this->getDownloadLink() as $host => $links){
            $totalLink = count($links);
            if ($isSplitted) {
                $header .= ($totalLink > 1) ? "[TH=colspan=$totalLink|]".$host.'[/TH]' : '[TH=rowspan=2|]'.$host.'[/TH]';;
            } else {
                $header .='[TH]'.$host.'[/TH]';
            }

            if($totalLink > 1) {
                for($i=1; $i <= $totalLink; $i++) {
                    $header2 .= '[TH]'."P$i".'[/TH]';
                }
            }

            $linkOrder = 1;
            foreach($links as $link) {
                if($totalLink > 1) {
                    $body.= '[TD][URL="'.$link.'"]'.$forum->getPhrase('yrms_part').$linkOrder.'[/URL][/TD]';
                } else {
                    $body.= '[TD][URL="'.$link.'"]'.$forum->getPhrase('yrms_download').'[/URL][/TD]';
                }
                $linkOrder++;
            }
        }
        $linkFormat = '[TABLE=|text-align: center;][TR]'.$header.'[/TR][TR]'.$header2.'[/TR][TR]'.$body.'[/TR][/TABLE]';
        return $linkFormat;
    }

    public function reward()
    {
        $forum = callHelper('Forum');
        $earnedAward = array();

        $fansubAwardContent = $this->calculateFansubAward();

        $mainUploadYun = explode(';', $forum->getConfig('yrms_vietsubmanga_yun_uploader'));
        $mirrorUploadYun = explode(';', $forum->getConfig('yrms_vietsubmanga_yun_mirror'));
        $budget = $mainUploadYun[$this->getLength()] + $mirrorUploadYun[$this->getLength()];
        $awardedTo = array();
        $loop = 1;

        foreach($this->getAllDownloadLink(false) as $downloadLinkObject) {
            $post = (get_class($downloadLinkObject) == get_class()) ? $downloadLinkObject->getDownloadPost() : $downloadLinkObject->getPost();
            $awardObject = (get_class($downloadLinkObject) == get_class()) ? $downloadLinkObject->getDownloadAward() : $downloadLinkObject->getAward();
            $postUserId = ($post->getUserId()) ? $post->getUserId() : $post->getPostUserId();
            $downloadLinks = (get_class($downloadLinkObject) == get_class()) ? $downloadLinkObject->getDownloadLink() : $downloadLinkObject->getLink();
            $downloadAwardContent = array();
            $amount = 0;

            foreach($downloadLinks as $downloadLink) {
                if($loop == 1) {
                    $amount += $mainUploadYun[$this->getLength()];
                } else {
                    $amount += $mirrorUploadYun[$this->getLength()];
                }
                $loop++;
                if($loop > 2) {
                    break;
                }
            }

            if($amount + $awardedTo[$postUserId] > $budget) {
                $amount = (($amount > $awardedTo[$postUserId])) ? $amount - $awardedTo[$postUserId] : 0;
            }

            $downloadAwardContent = array($postUserId => $amount);

            if(!isset($awardedTo[$postUserId])) {
                $awardedTo[$postUserId] = 0;
            }
            if($awardedTo[$postUserId] < $budget) {
                if(!$downloadLinkObject->getKeep()) {
                    if((get_class($downloadLinkObject) == get_class())) {
                        $awardObject->setContent(array_mesh($fansubAwardContent, $downloadAwardContent));
                    } else {
                        $awardObject->setContent($downloadAwardContent);
                    }
                    $awardObject
                        ->setPostId($downloadLinkObject->getPostId())
                        ->setResourceId($downloadLinkObject->getId())
                        ->setResourceType('vietsubmanga')
                        ->save();
                    $earnedAward = array_mesh($earnedAward, $awardObject->getEarnedAward());
                }

                $awardedTo[$postUserId] += (isset($fansubAwardContent[$postUserId]) && get_class($downloadLinkObject) == get_class())
                    ? $awardObject->getContent()[$postUserId] - $fansubAwardContent[$postUserId]
                    : $awardObject->getContent()[$postUserId];
            }
        }

        $streamAwardContent = array();
        $awardedTo = array();
        $budget = explode(';', $forum->getConfig('yrms_vietsubmanga_yun_stream'))[$this->getLength()];
        foreach($this->getAllStreamLink(false) as $streamLinkObject) {
            $canBreak = 0;
            $post = (get_class($streamLinkObject) == get_class())
                ? getModel('ForumPost')->loadPost($streamLinkObject->getStreamThread()->getFirstPostId())
                : $streamLinkObject->getPost();
            $postUserId = ($post->getUserId()) ? $post->getUserId() : $post->getPostUserId();

            $streamLink = (get_class($streamLinkObject) == get_class()) ? $streamLinkObject->getStreamLink() : $streamLinkObject->getLink();
            $awardObject = (get_class($streamLinkObject) == get_class()) ? $streamLinkObject->getStreamAward() : $streamLinkObject->getAward();
            if($streamLink) {
                $canBreak = 1;
                $amount = $budget;
                if($amount + $awardedTo[$postUserId] > $budget) {
                    $amount = (($amount > $awardedTo[$postUserId])) ? $amount - $awardedTo[$postUserId] : 0;
                }
                $streamAwardContent = (!$this->getPostId() && get_class($downloadLinkObject) == get_class())
                    ? array_mesh($fansubAwardContent, array($postUserId => $amount))
                    : array($postUserId => $amount);
            }

            if(!isset($awardedTo[$postUserId])) {
                $awardedTo[$postUserId] = 0;
            }
            if($awardedTo[$postUserId] < $budget) {
                if(!$streamLinkObject->getKeep()) {
                    $awardObject->setPostId($post->getPostId())
                        ->setContent($streamAwardContent)
                        ->setResourceId($streamLinkObject->getId())
                        ->setResourceType('vietsubmanga_chapter')
                        ->save();

                    $earnedAward = array_mesh($earnedAward, $awardObject->getEarnedAward());
                }
                $awardedTo[$postUserId] += (isset($fansubAwardContent[$postUserId]) && get_class($downloadLinkObject) == get_class())
                    ? $awardObject->getContent()[$postUserId] - $fansubAwardContent[$postUserId]
                    : $awardObject->getContent()[$postUserId];
            }

            if($canBreak) {
                break;
            }
        }


        return $earnedAward;
    }

    protected function isHomeFansub()
    {
        $forum = callHelper('Forum');
        foreach(explode(',', $forum->getConfig('yrms_main_homedomain')) as $homeDomain) {
            if(strpos($this->getFansubSite(), $homeDomain) !== FALSE) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getAwardGiven($userId, $onlyUpload = 1, $type = 'download')
    {
        $totalAmount = 0;
        switch($type) {
            case 'download':
                foreach($this->getAllDownloadLink(false) as $downloadLinkObject) {
                    $award = (get_class($downloadLinkObject) == get_class()) ? $downloadLinkObject->getDownloadAward() : $downloadLinkObject->getAward();
                    $amount = $award->getContent();
                    if(isset($amount[$userId])) {
                        $totalAmount += $amount[$userId];
                    }
                }

                if($onlyUpload && $this->getPostId()) {
                    $fansubAward = (int)$this->calculateFansubAward($userId);
                    $totalAmount = $totalAmount - $fansubAward;
                }

                return $totalAmount;

            case 'stream':
                foreach($this->getAllStreamLink(false) as $streamLinkObject) {
                    $award = (get_class($streamLinkObject) == get_class()) ? $streamLinkObject->getStreamAward() : $streamLinkObject->getAward();
                    $amount = $award->getContent();
                    if(isset($amount[$userId])) {
                        $totalAmount += $amount[$userId];
                    }
                }

                if($onlyUpload && !$this->getPostId()) {
                    $fansubAward = (int)$this->calculateFansubAward($userId);
                    $totalAmount = $totalAmount - $fansubAward;
                }

                return $totalAmount;
        }
    }

    public function calculateFansubAward($userId = 0)
    {
        $forum = callHelper('Forum');
        $fansubAwardContent = array();
        if($this->isHomeFansub()) {
            $translatorYun = explode(';', $forum->getConfig('yrms_vietsubmanga_yun_translator'));
            $proofreaderYun = explode(';', $forum->getConfig('yrms_vietsubmanga_yun_proofreader'));
            $editorYun = explode(';', $forum->getConfig('yrms_vietsubmanga_yun_editor'));
            $qualitycheckerYun = explode(';', $forum->getConfig('yrms_vietsubmanga_yun_qualitychecker'));

            if($this->getFansubmember('translator')){
                $fansubAwardContent = array_mesh($fansubAwardContent, array(implode(';', $this->getFansubmember('translator')) => $translatorYun[$this->getLength()]));
            }
            if($this->getFansubmember('proofreader')){
                $fansubAwardContent = array_mesh($fansubAwardContent, array(implode(';', $this->getFansubmember('proofreader')) => $proofreaderYun[$this->getLength()]));
            }
            if($this->getFansubmember('editor')){
                $fansubAwardContent = array_mesh($fansubAwardContent, array(implode(';', $this->getFansubmember('editor')) => $editorYun[$this->getLength()]));
            }
            if($this->getFansubmember('qualitychecker')){
                $fansubAwardContent = array_mesh($fansubAwardContent, array(implode(';', $this->getFansubmember('qualitychecker')) => $qualitycheckerYun[$this->getLength()]));
            }
            $fansubAwardContent = $this->getDownloadAward()->convertData($fansubAwardContent);
        }

        if($userId) {
            if(isset($fansubAwardContent[$userId])) {
                return $fansubAwardContent[$userId];
            }
            return 0;
        }
        return $fansubAwardContent;
    }
}