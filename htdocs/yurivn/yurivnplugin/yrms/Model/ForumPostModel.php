<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/25/15
 * Time: 9:36 AM
 */
class ForumPostModel extends AncestorModel
{
    protected $_tableName = 'post';
    protected $_idColumn = 'postid';
    protected $_vbulletin;

    public function __construct()
    {
        parent::__construct();
        global $vbulletin;
        $this->_vbulletin = $vbulletin;
    }

    public function savePost()
    {
        $postman =& datamanager_init('Post', $this->_vbulletin, ERRTYPE_ARRAY, 'threadpost');

        $threadId = $this->getThreadId();

        $postId = $this->getPostId();
        $postUserId = ($this->getPostUserId()) ? $this->getPostUserId() : $this->getUserId();
        $parentId = $this->getParentId();

        $threadInfo = fetch_threadinfo($threadId);
        $postInfo = fetch_postinfo($postId);
        if($postInfo) {
            $postman->set_existing($postInfo);
        }

        $postData = $this->getData();
        $postman->set_info('thread', $threadInfo);
        $postman->setr('threadid', $threadId);
        $postman->setr('parentid', $parentId);
        $postman->setr('userid', $postUserId);
        $postman->setr('pagetext', $postData['pagetext']);
        $postman->setr('title', $postData['title']);
        $postman->setr('showsignature', $showsignature);
        $postman->set('allowsmilie', $postData['allowsmilie']);
        $postman->set('visible', $postData['visible']);
        $postman->set_info('parseurl', $postData['parseurl']);

        if (!$postId) {
            $this->setPostId($postman->save());
        } else {
            $postman->save();
        }

        return true;
    }

    public function loadPost($value, $columnToSelect='')
    {
        if($columnToSelect) {
            parent::load($value, $columnToSelect);
            return $this;
        }

        $postId = $value;
        $postInfo = fetch_postinfo($postId);
        if(is_array($postInfo)) {
            foreach ($postInfo as $key => $info) {
                $set = "set$key";
                $this->$set($info);
            }
        }
        return $this;
    }

    public function delete()
    {
        delete_post($this->getPostId());
        return true;
    }

    public function softDelete()
    {
        delete_post($this->getPostId(), true, 0, false);
        return true;
    }

    public function restore()
    {
        undelete_post($this->getPostId(), true);
        return true;
    }
}