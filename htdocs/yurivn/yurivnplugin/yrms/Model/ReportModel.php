<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 7/3/15
 * Time: 8:07 PM
 */
class ReportModel extends AncestorModel
{
    protected $_tableName =  'yrms_report';
    protected $_serializeValues = array('deadhostname');
    protected $_specialProperties = array(
        'resource' => 'return getModel($this->getResourceType())->load($this->getResourceId());'
    );

    public function getResourceInfo($info='')
    {
        $forum = callHelper('Forum');
        switch($this->getResourceType()) {
            case 'VietsubmangaChapter':
                if($info) {
                    switch($info) {
                        case 'forumId':
                            return $forum->getConfig('yrms_vietsubmanga_id_truyendich');
                        case 'name':
                            return $this->getResource()->getManga()->getTitle().
                            ': '.$forum->getPhrase('yrms_chaptertype'.$this->getResource()->getType()).
                            ' '.$this->getResource()->getCode();
                        case 'headResource':
                            return $this->getResource()->getManga();
                    }
                } else {
                    return array(
                        'forumId' => $forum->getConfig('yrms_vietsubmanga_id_truyendich'),
                        'name' => $this->getResource()->getManga()->getTitle().
                            ': '.$forum->getPhrase('yrms_chaptertype'.$this->getResource()->getType()).
                            ' '.$this->getResource()->getCode(),
                        'headResource' => $this->getResource()->getManga()
                    );
                }
        }
    }

    public function getDuration($link, $type = 'download')
    {
        $present = time();
        if ($link->getResource()) {
            return secondToTime($present - $link->getPost()->getDateLine());
        } else {
            switch($type) {
                case 'download':
                    return secondToTime($present - $link->getDownloadPost()->getDateLine());
                case 'stream':
                    return secondToTime($present - $link->getStreamThread()->getDateLine());
            }
        }
    }
}