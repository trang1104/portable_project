<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 7/3/15
 * Time: 8:07 PM
 */
class AdditionalLinkModel extends AncestorModel
{
    protected $_tableName =  'yrms_additionallink';
    protected $_serializeValues = array('link');
    protected $_specialProperties = array(
        'post' => 'return getModel("ForumPost")->loadPost($this->getPostId());',
        'award' => 'return getModel("Award")->load($this->getPostId(), "postid");',
        'resource' => 'return getModel($this->getResourceType())->load($this->getResourceId());'
    );

    public function __construct()
    {
        $this->setPost();
        $this->setAward();
        parent::__construct();
    }

    public function convertLink($rawLink)
    {
        $resourceLinks = $this->getResource()->getAllDownloadLink();
        while (list($host, $link) = each($rawLink)) {
            if(array_key_exists($host, $resourceLinks)) {
                $rawLink[$host.'_mirror'] = $link;
                unset($rawLink[$host]);
                reset($rawLink);
            }
        }

        return $rawLink;
    }

    public function getResourceInfo($info='')
    {
        $forum = callHelper('Forum');
        switch($this->getResourceType()) {
            case 'VietsubmangaChapter':
                if($info) {
                    switch($info) {
                        case 'forumId':
                            return $forum->getConfig('yrms_vietsubmanga_id_truyendich');
                        case 'name':
                            return $forum->getPhrase('yrms_chaptertype'.$this->getResource()->getType()).' '.$this->getResource()->getCode();
                        case 'nameIncludeHead':
                            return $this->getResource()->getManga()->getTitle().
                            ': '.$forum->getPhrase('yrms_chaptertype'.$this->getResource()->getType()).
                            ' '.$this->getResource()->getCode();
                        case 'head':
                            return $this->getResource()->getManga();
                    }
                } else {
                    return array(
                        'forumId' => $forum->getConfig('yrms_vietsubmanga_id_truyendich'),
                        'name' => $forum->getPhrase('yrms_chaptertype'.$this->getResource()->getType()).' '.$this->getResource()->getCode(),
                        'nameIncludeHead' => $this->getResource()->getManga()->getTitle().
                            ': '.$forum->getPhrase('yrms_chaptertype'.$this->getResource()->getType()).
                            ' '.$this->getResource()->getCode(),
                        'head' => $this->getResource()->getManga()
                    );
                }
        }
    }

    public function save()
    {
        $forum = callHelper('Forum');
        $resourceInfo = $this->getResourceInfo();
        $thisIsNew = ($this->getId()) ? FALSE : TRUE;

        //region save additional link and its post to database
        parent::save();

        $postUserId = $forum->getUserInfo('userid');

        switch($this->getType()) {
            case 1:
                if($this->getResource()->getAdult()) {
                    $pagetext = construct_phrase($forum->getPhrase('yrms_postformat_update'), $forum->getPhrase('yrms_mirrorlink').' - '.$resourceInfo['name'], '');
                } else {
                    $pagetext = construct_phrase($forum->getPhrase('yrms_postformat_update'), $forum->getPhrase('yrms_mirrorlink').' - '.$resourceInfo['name'], '');
                }
                break;
            case 2:
                $pagetext = construct_phrase($forum->getPhrase('yrms_postformat_update'), $forum->getPhrase('yrms_readlink').' - '.$resourceInfo['name'], '');
                break;
        }

        $this->getPost()
            ->loadPost($this->getPostId())
            ->setThreadId($resourceInfo['head']->getThreadId())
            ->setParent($resourceInfo['head']->getPostId())
            ->setPostUserId($postUserId)
            ->setTitle($resourceInfo['nameIncludeHead'])
            ->setPageText($pagetext)
            ->setAllowSmilie(1)
            ->setParseUrl(1)
            ->setVisible(1)
            ->savePost();

        if(!$this->getPost()->getYrmsPost()) {
            $this->getPost()->setYrmsPost(1)->save();
        }

        if($thisIsNew) {
            $this->setPostId($this->getPost()->getPostId());
            parent::save();
        }

        //endregion
    }
}