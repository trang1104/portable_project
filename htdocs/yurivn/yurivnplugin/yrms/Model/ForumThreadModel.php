<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 1/25/15
 * Time: 9:36 AM
 */
class ForumThreadModel extends AncestorModel
{
    protected $_tableName = 'thread';
    protected $_idColumn = 'threadid';
    protected $_vbulletin;

    public function __construct()
    {
        parent::__construct();
        global $vbulletin;
        $this->_vbulletin = $vbulletin;
    }

    public function saveThread()
    {
        $threadman =& datamanager_init('Thread_FirstPost', $this->_vbulletin, ERRTYPE_ARRAY, 'threadpost');

        $forumId = $this->getForumId();
        $threadId = $this->getThreadId();
        $postUserId = $this->getPostUserId();
        $forumInfo = fetch_foruminfo($forumId);
        $threadInfo = fetch_threadinfo($threadId);
        if($threadInfo) {
            $threadman->set_existing($threadInfo);
        }

        $threadData = $this->getData();
        $threadman->set_info('forum', $forumInfo);
        $threadman->set_info('thread', $threadInfo);
        $threadman->setr('forumid', $forumId);
        $threadman->setr('userid', $postUserId);
        $threadman->setr('title', $threadData['title']);
        $threadman->setr('pagetext', $threadData['pagetext']);
        $threadman->setr('taglist', $threadData['taglist']);

        $showSignature = 1;
        $threadman->setr('showsignature', $showSignature);
        $threadman->set('allowsmilie', $threadData['allowsmilie']);
        $threadman->set('visible', $threadData['visible']);
        $threadman->set_info('parseurl', $threadData['parseurl']);
        $threadman->set('prefixid', $threadData['prefixid']);

        if (!$this->getThreadId()) {
            $this->setThreadId($threadman->save());
            $this->setFirstPostId($threadman->thread['firstpostid']);
        } else {
            $threadman->save();
        }
    }

    public function delete()
    {
        delete_thread($this->getThreadId());
        return true;
    }

    public function softDelete()
    {
        delete_thread($this->getThreadId(), true, 0, false);
        return true;
    }

    public function restore()
    {
        undelete_thread($this->getThreadId(), true);
        return true;
    }

    public function loadThread($threadId)
    {
        $threadInfo = fetch_threadinfo($threadId);
        if(!empty($threadInfo)) {
            foreach ($threadInfo as $key => $info) {
                $set = "set$key";
                $this->$set($info);
            }
        }
        return $this;
    }
}