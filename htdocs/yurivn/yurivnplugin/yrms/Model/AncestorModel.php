<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/28/14
 * Time: 6:52 PM
 */
class AncestorModel extends Singleton
{
    protected $_db;
    protected $_idColumn = 'id';
    protected $_storedData = array();
    protected $_filters = array();
    protected $_orders = array();
    protected $_limit = array();
    protected $_serializeValues = array();
    protected $_specialProperties = array();

    public function __construct()
    {
        if(isset($this->_tableName)) {
            $this->_db = getHelper('Database', $this->_tableName);
        }
        foreach($this->_serializeValues as $serializeValue) {
            $set = 'set'.$serializeValue;
            $this->$set(array());
        }
    }

    public function clear()
    {
        foreach ($this as $key => $value) {
            unset($this->$key);
        }
        return $this;
    }

    public function reset()
    {
        $this->_storedData = array();
        $this->_filters = array();
        $this->_orders = array();
        $this->_limit = array();
        foreach ($this as $key => $value) {
            if(strpos($key,'_') === false) {
                unset($this->$key);
            }

        }
        $this->__construct();
        return $this;
    }

    public function reload()
    {
        $id = $this->getId();
        if($id) {
            return $this->reset()->load($id);
        }
        return $this;
    }

    public function setLimit($rowNum, $from = 0)
    {
        $this->_limit = array(
            'from' => $from,
            'rowNum' => $rowNum
        );
        return $this;
    }

    public function setFilter($field, $value = null)
    {
        if($value === null) {
            $filter = array($field);
        } else {
            $filter = array(array($field => $value));
        }
        $this->_filters = $filter;
        return $this;
    }

    public function addFilter($field, $value = null)
    {
        if($value === null) {
            $filter = array($field);
        } else {
            $filter = array(array($field => $value));
        }
        $this->_filters = array_merge($this->_filters, $filter);
        return $this;
    }

    public function setOrder($orders)
    {
        $this->_orders = $this->convertOrder($orders);
        return $this;
    }

    public function addOrder($orders)
    {
        if(!$this->_orders) {
            $this->setOrder($orders);
            return $this;
        }
        $this->_orders = array_merge($this->_orders, $this->convertOrder($orders));
        return $this;
    }

    public function save()
    {
        $columns = $this->_db->getColumns();

        foreach($this->_storedData as $name => $value) {
            foreach($columns as $column) {
                if(preg_replace("/[^a-z0-9]+/", "", $column) == $name) {
                    $data[$column] = $this->_storedData[$name];
                    unset($this->_storedData[$name]);
                }
            }
        }

        $getId = 'get'.(preg_replace("/[^a-z0-9]+/", "", $this->_idColumn));
        if(!$this->$getId()) {
            try{
                $this->_db->insert($data);
                $setId = 'set'.ucfirst($this->_idColumn);
                $this->$setId($this->_db->insert_id());
                return true;
            } catch(Exception $e) {
                return false;
            }
        } else {
            try{
                $this->_db->update($data, "{$this->_idColumn} = '{$this->$getId()}'");
                return true;
            } catch(Exception $e) {
                return false;
            }
        }
    }

    public function load($value, $columnToSelect='')
    {
        if(empty($columnToSelect)) {
            $columnToSelect = $this->_idColumn;
        }
        $data = $this->_db->fetchOnce("SELECT * FROM `{$this->_tableName}` WHERE `$columnToSelect` = '$value'");

        if(!empty($data)) {
            $this->reset();
            foreach($data as $column => $value) {
                $property = preg_replace("/[^a-z0-9]+/", "", $column);
                $set = "set$property";
                $this->$set($value);
            }
        }
        return $this;

    }

    public function delete()
    {
        $getId = "get$this->_idColumn";
        $this->_db->delete("`{$this->_idColumn}` = '".$this->$getId()."'");
    }

    public function deleteAll()
    {
        $this->_db->delete();
    }

    public function getCollection()
    {
        $where = '';
        $order = '';
        $limit = '';
        if(!empty($this->_filters)) {
            $where = 'WHERE ';
            foreach($this->_filters as $filters) {
                $whereNode = '';
                foreach($filters as $field => $value) {
                    if($whereNode != '') {
                        $whereNode.= " AND `" . $field . "` LIKE '" . $value. "'";
                    } else {
                        $whereNode .= "`" . $field . "` LIKE '" . $value. "'";
                    }
                }
                if(count($filters) > 1) {
                    $whereNode = "($whereNode)";
                }

                if($where != 'WHERE ') {
                    $where.= " OR $whereNode";
                } else {
                    $where .= "$whereNode";
                }
            }
        }

        if(!empty($this->_orders)) {
            $order = 'ORDER BY ';
            foreach($this->_orders as $orderType => $fields) {
                if(is_array($fields)) {
                    $fields = implode(" $orderType,", $fields);
                }
                if($order != 'ORDER BY ') {
                    $order.= ", $fields $orderType";
                } else {
                    $order .= "$fields $orderType";
                }
            }
        }

        if(!empty($this->_limit)) {
            $limit = "LIMIT {$this->_limit['from']}, {$this->_limit['rowNum']}";
        }

        $query = "SELECT * FROM `{$this->_tableName}` $where $order $limit";
        $dataList = $this->_db->fetchAll($query);

        $collection = getModel('Ancestor')->clear();
        $i = 0;
        if($dataList) {
            foreach($dataList as $data) {
                $item = getModel(str_replace('Model', '', get_called_class()));
                foreach($data as $propertyName => $propertyValue) {
                    $set = "set$propertyName";
                    $item->$set($propertyValue);
                }
                $collection->$i = $item;
                $i++;
            }
        }

        return $collection;
    }

    public function getFirstItem()
    {
        $firstItem = $this->getCollection()->{0};
        if($firstItem) {
            return $firstItem;
        }
        return $this;
    }

    public function getData()
    {
        $dataList = array();
        if(get_called_class() == 'AncestorModel') {
            foreach($this as $item) {
                $data = array();
                foreach($item->_storedData as $propertyName => $propertyValue) {
                    if(!is_object($propertyValue)) {
                        if(in_array($propertyName, $item->_serializeValues) && isSerialized($propertyValue))  {
                            $data[$propertyName] = unserialize($propertyValue);
                        } else {
                            $data[$propertyName] = $propertyValue;
                        }
                    }
                }

                if(!empty($data)) {
                    $dataList[] = $data;
                }
            }
        } else {
            foreach($this->_storedData as $propertyName => $propertyValue) {
                if(!is_object($propertyValue)) {
                    if(in_array($propertyName, $this->_serializeValues) && isSerialized($propertyValue))  {
                        $dataList[$propertyName] = unserialize($propertyValue);
                    } else {
                        $dataList[$propertyName] = $propertyValue;
                    }
                }

            }
        }

        return $dataList;
    }

    public function count()
    {
        $numberOfProperties = 0;
        foreach ($this as $key => $value) {
            $numberOfProperties++;
        }
        return $numberOfProperties;
    }

    public function getTotalItem()
    {
        return $this->_db->getTotalItem();
    }

    public function insert_id()
    {
        return $this->_db->insert_id();
    }

    public function __call($method,$args)
    {
        if(strtolower(substr($method, 0, 3)) == 'get') {
            if(!empty($args)) {
                if(isset($this->{strtolower(substr($method, 3))})) {
                    $result = $this->{strtolower(substr($method, 3))};
                    if(in_array(strtolower(substr($method, 3)), $this->_serializeValues) && isSerialized($this->{strtolower(substr($method, 3))})) {
                        return unserialize($result[$args[0]]);
                    }
                    return $result[$args[0]];
                } elseif(in_array(strtolower(substr($method, 3)), array_keys($this->_specialProperties))) {
                    $set = 'set'.substr($method, 3);
                    $get = 'get'.substr($method, 3);
                    $this->$set(eval($this->_specialProperties[strtolower(substr($method, 3))]));
                    return $this->$get();
                }
            }

            if(isset($this->{strtolower(substr($method, 3))})) {
                if(in_array(strtolower(substr($method, 3)), $this->_serializeValues) && isSerialized($this->{strtolower(substr($method, 3))})) {
                    return unserialize($this->{strtolower(substr($method, 3))});
                }
                return $this->{strtolower(substr($method, 3))};
            } elseif(in_array(strtolower(substr($method, 3)), array_keys($this->_specialProperties))) {
                $set = 'set'.substr($method, 3);
                $get = 'get'.substr($method, 3);
                $this->$set(eval($this->_specialProperties[strtolower(substr($method, 3))]));
                return $this->$get();
            }
        }

        if(substr($method, 0, 3) == 'set') {
            if(isset($args[0])) {
                $this->_storedData[strtolower(substr($method, 3))] = $this->{strtolower(substr($method, 3))} = $args[0];
                if(!isSerialized($args[0]) && in_array(strtolower(substr($method, 3)), $this->_serializeValues)) {
                    $this->_storedData[strtolower(substr($method, 3))] = $this->{strtolower(substr($method, 3))} = serialize($args[0]);
                }
            }
            return $this;
        }

        if(substr($method, 0, 5) == 'unset') {
            $this->_storedData[strtolower(substr($method, 5))] = $this->{strtolower(substr($method, 5))} = $args[0];
            return $this;
        }
    }

    protected function convertOrder($orders)
    {
        if(!is_array($orders)) {
            return array('asc' => $orders);
        }

        array_change_key_case($orders, CASE_LOWER);
        $isRightOrderFormat = TRUE;
        switch(count($orders)) {
            case 1:
                if(array_key_exists('asc', $orders) && is_array(@$orders['asc'])) {
                    foreach($orders['asc'] as $column) {
                        if(is_array($column)) {
                            $isRightOrderFormat = FALSE;
                        }
                    }
                } elseif(array_key_exists('desc', $orders) && is_array(@$orders['desc'])) {
                    foreach($orders['desc'] as $column) {
                        if(is_array($column)) {
                            $isRightOrderFormat = FALSE;
                        }
                    }
                } else {
                    $isRightOrderFormat = FALSE;
                }
                break;
            case 2:
                if(array_key_exists('desc', $orders) && array_key_exists('asc', $orders)) {
                    foreach($orders['asc'] as $column) {
                        if(is_array($column)) {
                            $isRightOrderFormat = FALSE;
                        }
                    }
                    foreach($orders['asc'] as $column) {
                        if(is_array($column)) {
                            $isRightOrderFormat = FALSE;
                        }
                    }
                } else {
                    $isRightOrderFormat = FALSE;
                }
                break;
            default: $isRightOrderFormat = FALSE;
        }

        if(!$isRightOrderFormat) {
            $convertedOrders = array();
            foreach($orders as $key => $order) {
                if($key === 'desc') {
                    $convertedOrders['desc'][] = $order;
                } else {
                    $convertedOrders['asc'][] = $order;
                }
            }
            return $convertedOrders;
        } else {
            return $orders;
        }

    }
}