<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/26/14
 * Time: 8:29 PM
 */

function getConfig($configName) {
    require_once('Config.php');
    $configData = new configData();
    return $configData->get($configName);
}

function getHelper($helperName, $constructArgs = array()) {
    require_once("Helper/{$helperName}Helper.php");
    $helperName = $helperName.'Helper';
    $helper = new $helperName($constructArgs);
    return $helper;
}

function callHelper($helperName, $constructArgs = array()) {
    require_once("Helper/{$helperName}Helper.php");
    $helperName = $helperName.'Helper';
    $helper = $helperName::getInstance($constructArgs);
    return $helper;
}

function getModel($modelName, $constructArgs = array())
{
    require_once("Model/AncestorModel.php");
    require_once("Model/{$modelName}Model.php");
    $modelName = $modelName.'Model';
    $model = new $modelName($constructArgs);
    return $model;
}

function callModel($modelName, $constructArgs = '')
{
    require_once("Model/AncestorModel.php");
    require_once("Model/{$modelName}Model.php");
    $modelName = $modelName.'Model';
    $model = $modelName::getInstance($constructArgs);
    return $model;
}

function callController($controllerName)
{
    $controllerList = scandir('Controller');
    $controllerExisted = false;

    foreach($controllerList as $controllerFile) {
        $controllerItem = str_replace('Controller.php', '', $controllerFile);
        if(strtolower($controllerItem) == strtolower($controllerName)) {
            $controllerName = $controllerItem;
            $controllerExisted = true;
            break;
        }
    }
    if(!$controllerExisted) {
        die('Controller does not existed');
    }

    require_once("Controller/AncestorController.php");
    require_once("Controller/{$controllerName}Controller.php");
    $controllerName = $controllerName.'Controller';
    $controller = $controllerName::getInstance();
    return $controller;
}

function enhancedEcho(&$var, $default = '')
{
    echo isset($var) ? $var : $default;
}

function emptyEnhanced($var)
{
    if(empty($var)) {
        return true;
    }

    return false;
}

function setIfEmpty(&$var, $value)
{
    if($var == null || $var == '' || $var == array()) {
        $var = $value;
    }
}

function setIfValExist(&$var, $value)
{
    if($value !== null && $value !== '' && $value !== array()) {
        if(is_array($var) && is_array($value)){
            $var += $value;
        } elseif(is_array($var)) {
            $var[] = $value;
        } else {
            $var = $value;
        }
    }
}

function getRequest($name = null)
{
    if(isset($_REQUEST)) {
        if(is_null($name)) {
            return $_REQUEST;
        }

        if(isset($_REQUEST[$name])) {
            return $_REQUEST[$name];
        } else {
            return '';
        }
    }
}

function getPost($name = null)
{
    if(isset($_POST)) {
        if(is_null($name)) {
            return $_POST;
        }

        if(isset($_POST[$name])) {
            return $_POST[$name];
        } else {
            return '';
        }
    }
}

function getParam($name = null, $url = null)
{
    if($url && !is_null($name)) {
        $parts = parse_url($url);
        parse_str($parts['query'], $query);
        return $query[$name];
    }
    if(isset($_GET)) {
        if(is_null($name)) {
            return $_GET;
        }

        if(isset($_GET[$name])) {
            return $_GET[$name];
        } else {
            return '';
        }
    }
}

function addParam($url, $varName, $varValue)
{
    list($urlpart, $qspart) = array_pad(explode('?', $url), 2, '');
    parse_str($qspart, $qsvars);
    $qsvars[$varName] = $varValue;
    $newqs = http_build_query($qsvars);
    return $urlpart . '?' . $newqs;

}

function stripParam($url, $varname) {
    list($urlpart, $qspart) = array_pad(explode('?', $url), 2, '');
    parse_str($qspart, $qsvars);
    unset($qsvars[$varname]);
    $newqs = http_build_query($qsvars);
    return $urlpart . '?' . $newqs;
}

function keepParam($url, $vars) {
    list($urlpart, $qspart) = array_pad(explode('?', $url), 2, '');
    parse_str($qspart, $qsvars);
    foreach($qsvars as $key => $value) {
        if(!in_array($key, $vars)) {
            unset($qsvars[$key]);
        }
    }
    $newqs = http_build_query($qsvars);
    return $urlpart . '?' . $newqs;
}

function getCurrentUrl()
{
    return 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
}

function isPost() {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        return true;
    } else {
        return false;
    }
}

function shuffleObject($object) {
    $array = (array)$object;
    shuffle($array);
    return (object)$array;
}

function getClientIp() {
    $ipaddress = false;
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');

    return $ipaddress;
}


function romanicNumber($integer, $upcase = true)
{
    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
    $return = '';
    while($integer > 0)
    {
        foreach($table as $rom=>$arb)
        {
            if($integer >= $arb)
            {
                $integer -= $arb;
                $return .= $rom;
                break;
            }
        }
    }

    return $return;
}

function array_mesh() {
    // Combine multiple associative arrays and sum the values for any common keys
    // The function can accept any number of arrays as arguments
    // The values must be numeric or the summed value will be 0

    // Get the number of arguments being passed
    $numargs = func_num_args();

    // Save the arguments to an array
    $arg_list = func_get_args();

    // Create an array to hold the combined data
    $out = array();

    // Loop through each of the arguments
    for ($i = 0; $i < $numargs; $i++) {
        $in = $arg_list[$i]; // This will be equal to each array passed as an argument

        // Loop through each of the arrays passed as arguments
        foreach($in as $key => $value) {
            // If the same key exists in the $out array
            if(array_key_exists($key, $out)) {
                // Sum the values of the common key
                $sum = $in[$key] + $out[$key];
                // Add the key => value pair to array $out
                $out[$key] = $sum;
            }else{
                // Add to $out any key => value pairs in the $in array that did not have a match in $out
                $out[$key] = $in[$key];
            }
        }
    }

    return $out;
}

function isSerialized($value)
{
    if(@unserialize($value) === false && $value !== 'b:0;') {
        return false;
    } else {
        return true;
    }
}

function print_pre($data, $die = true)
{
    echo '<pre>';
    print_r($data);
    if($die) {
        die();
    } else {
        echo '</pre>';
    }
}

function setSession($sessionName, $var)
{
    if(session_id() == '') {
        session_start();
    }
    $_SESSION[$sessionName] = $var;
}

function getSession($sessionName)
{
    if(session_id() == '') {
        session_start();
    }
    return $_SESSION[$sessionName];
}

function unsetSession($sessionName)
{
    if(session_id() == '') {
        session_start();
    }
    unset($_SESSION[$sessionName]);
}

function fetch_full_seo_url($urlType, $urlInfo)
{
    $forum = callHelper('Forum');
    return $forum->getConfig('bburl').'/'.fetch_seo_url($urlType, $urlInfo);
}

function secondToTime($seconds, $unit = 'days') {
    $dtF = new DateTime("@0");
    $dtT = new DateTime("@$seconds");

    switch($unit) {
        case 'days':
            return $dtF->diff($dtT)->format('%a');
        case 'hours':
            return $dtF->diff($dtT)->format('%h');
        case 'minutes':
            return $dtF->diff($dtT)->format('%i');
        default:
            return $dtF->diff($dtT)->format('%a');
    }
}

function redirect($url, $permanent = false)
{
    $forum = callHelper('Forum');
    header('Location: ' . $forum->getConfig('bburl').'/'.$url, true, $permanent ? 301 : 302);

    exit();
}

function unicode_str_filter ($str){
    $unicode = array(
        'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
        'd'=>'đ',
        'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
        'i'=>'í|ì|ỉ|ĩ|ị',
        'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
        'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
        'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
        'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        'D'=>'Đ',
        'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
        'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
        'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
        'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
        'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
    );
    foreach($unicode as $nonUnicode=>$uni){
        $str = preg_replace("/($uni)/i", $nonUnicode, $str);
    }
    return $str;
}

function multiDimensCount($array) {
    return array_sum(array_map("count", $array));
}