<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/26/14
 * Time: 8:29 PM
 */

function construct_pagenavigation($pagenumber,$perpage,$results,$address)
{
    global $vbphrase;
    $pagenav='<li><a style="background:-moz-linear-gradient(center top , rgb(0, 0, 0) 5%, rgb(125, 125, 125) 100%) repeat scroll 0% 0% red;background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #000000), color-stop(1, #7D7D7D) );"><span>'.$pagenumber.'</span></a></li>';
    if(($pagenumber-1)>0)
        $pagenav ='<li><a href="'.$address.'&page='.($pagenumber-1).'" class="active"><span>'.($pagenumber-1).'</span></a></li>'.$pagenav;
    if(($pagenumber-2)>0)
        $pagenav ='<li><a href="'.$address.'&page='.($pagenumber-2).'" class="active"><span>'.($pagenumber-2).'</span></a></li>'.$pagenav;
    if(($pagenumber+1)<=ceil($results/$perpage))
        $pagenav =$pagenav.'<li><a href="'.$address.'&page='.($pagenumber+1).'" class="active"><span>'.($pagenumber+1).'</span></a></li>';
    if(($pagenumber+2)<=ceil($results/$perpage))
        $pagenav =$pagenav.'<li><a href="'.$address.'&page='.($pagenumber+2).'" class="active"><span>'.($pagenumber+2).'</span></a></li>';
    $pagenav='<ul>
              <li><a><span>'.construct_phrase($vbphrase['page_x_of_y'],$pagenumber,ceil($results/$perpage)).'</span></a></li>
              <li><a href="'.$address.'&page=1" class="active"><span>'.$vbphrase['first'].'</span></a></li>
              '.$pagenav.'
              <li><a href="'.$address.'&page='.ceil($results/$perpage).'" class="active"><span>'.$vbphrase['last'].'</span></a></li>
              </ul>';
    return $pagenav;
}

function mergeFansubmember($fansubmember1, $fansubmember2)
{
    foreach($fansubmember2 as $position => $members) {
        foreach($members as $member) {
            if($fansubmember1[$position]) {
                if(!in_array($member, $fansubmember1[$position])) {
                    $fansubmember1[$position][] = $member;
                }
            } else {
                $fansubmember1[$position] = $fansubmember2[$position];
            }

        }
    }
    return $fansubmember1;
}

function callback_mangaSort($a, $b) {
    $a = unicode_str_filter($a->getOriginalComposition().$a->getTitle());
    $b = unicode_str_filter($b->getOriginalComposition().$b->getTitle());
    return strcmp($a, $b);
}