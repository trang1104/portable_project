<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class VietsubmangaChapterController extends AncestorController
{
    public function listAction()
    {
        $forum = callHelper('Forum');
        $manga = getModel('VietsubmangaManga')->load(getParam('mangaid'));
        $dataView['pageTitle'] = $forum->getPhrase('yrms_chapterlist').' - '.$manga->getTitle();

        $data = getSession('forwardData');
        unsetSession('forwardData');
        if(!empty($data)) {
            $forum->rebuildForum($forum->getConfig('yrms_vietsubmanga_id_truyendich'));
            $dataView['msgType'] = $data['msgType'];
            $dataView['msg'] = $data['msg'];
        }

        $chapterModel = getModel('VietsubmangaChapter');
        if(!$manga->getId()){
            $dataView['msgType'] = 'error';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_error_thisnotexisted'), $forum->getPhrase('yrms_vietsubmanga'));
            $this->renderView('yrms_vietsubmanga_chapter_list', $dataView);
        }
        $dataView['manga'] = $manga->getData();

        $page = getParam('page');
        $perPage = 20;
        if(empty($page)) {
            $page = 1;
        }
        $chapters = $chapterModel->setFilter('mangaid', $manga->getId())
            ->setOrder(array('type', 'code'))
            ->setLimit($perPage, ($page-1)*$perPage)
            ->getCollection();
        $dataView['totalChapter'] = $totalItem = $chapters->count();

        if($totalItem > 0) {
            $dataView['chapters'] = array();
            foreach($chapters as $key => $chapter) {
                $dataView['chapters'][$key] = $chapter->fetchData();
                $dataView['chapters'][$key]['displayName'] = $dataView['chapters'][$key]['type'].' '.$dataView['chapters'][$key]['code'];
                $action = array();
                if(can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich')) || $manga->isOwner()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_edit'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=edit&id='.$chapter->getId(),
                        'confirm' => 0
                    );
                }
                if(count($chapter->getAllDownloadLink()) < 2 && count($chapter->getAllDownloadLink()) != 0 && $chapter->getTotalHost() > 0) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_mirrorlinkadd'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=additionallink&action=adddownload&resourceid='.$chapter->getId().'&type=VietsubmangaChapter',
                        'confirm' => 0
                    );
                }
                if(!$chapter->getStreamThreadId()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_readlinkadd'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=addstream&id='.$chapter->getId(),
                        'confirm' => 0
                    );
                } elseif(emptyEnhanced(array_filter($chapter->getAllStreamLink()))) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_readlinkadd'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=additionallink&action=addreadstream&resourceid='.$chapter->getId().'&type=VietsubmangaChapter',
                        'confirm' => 0
                    );
                }
                if(count($chapter->getAllDownloadLink()) == 0) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_downloadlinkrepair'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=additionallink&action=adddownload&resourceid='.$chapter->getId().'&type=VietsubmangaChapter',
                        'confirm' => 0
                    );
                }
                if(!emptyEnhanced($chapter->getAllDownloadLink())) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_deadlink_report'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=report&action=add&resourceid='.$chapter->getId().'&resourcetype=VietsubmangaChapter',
                        'confirm' => 0
                    );
                }
                if(can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich')) && $chapter->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_delete'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=delete&id='.$chapter->getId(),
                        'confirm' => 1
                    );
                }
                if(can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich')) && !$chapter->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_restore'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=restore&id='.$chapter->getId(),
                        'confirm' => 1
                    );
                }
                if(can_administer() && $chapter->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_deletehard'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=delete&hard=1&id='.$chapter->getId(),
                        'confirm' => 1
                    );
                }
                $dataView['chapters'][$key]['action'] = $action;
            }
            $dataView['pagenav'] = construct_pagenavigation($page, $perPage, $totalItem, stripParam(getCurrentUrl(), 'page'));
        } else {
            $dataView['msgType'] = 'info';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_info_emptylist'), $forum->getPhrase('yrms_chapter'), $forum->getPhrase('yrms_chapteradd'));
        }

        $this->renderView('yrms_vietsubmanga_chapter_list', $dataView);
    }

    public function deadlistAction()
    {
        $forum = callHelper('Forum');
        $dataView['pageTitle'] = construct_phrase($forum->getPhrase('yrms_dead'), $forum->getPhrase('yrms_vietsubmanga'));

        $manga = getModel('VietsubmangaManga')->load(107);
        if(!$manga->getId()){
            $dataView['msgType'] = 'error';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_error_thisnotexisted'), $forum->getPhrase('yrms_vietsubmanga'));
            $this->renderView('yrms_vietsubmanga_chapter_list', $dataView);
        }

        $page = getParam('page');
        $perPage = $forum->getConfig('yrms_main_perpage');
        if(empty($page)) {
            $page = 1;
        }

        $chapterModel = getModel('VietsubmangaChapter');
        if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
            $chapterModel->setFilter('active', 1);
        }
        $chapters = $chapterModel
            ->setOrder(array('type', 'code'))
            ->setLimit($perPage, ($page-1)*$perPage)
            ->getCollection();

        foreach($chapters as $key => $chapter) {
            if(!emptyEnhanced($chapter->getAllDownloadLink())) {
                unset($chapters->$key);
            }
        }
        $dataView['totalChapter'] = $totalItem = $chapters->count();

        if($totalItem > 0) {
            $dataView['chapters'] = array();
            foreach($chapters as $key => $chapter) {
                $dataView['chapters'][$key] = $chapter->fetchData();
                $dataView['chapters'][$key]['displayName'] = $chapter->getManga()->getTitle().': '.$dataView['chapters'][$key]['type'].' '.$dataView['chapters'][$key]['code'];
                $action = array();

                if(!$chapter->getStreamThreadId()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_readlinkadd'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=addstream&id='.$chapter->getId(),
                        'confirm' => 0
                    );
                } elseif(emptyEnhanced(array_filter($chapter->getAllStreamLink()))) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_readlinkadd'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=additionallink&action=addreadstream&resourceid='.$chapter->getId().'&type=VietsubmangaChapter',
                        'confirm' => 0
                    );
                }
                if(count($chapter->getAllDownloadLink()) == 0) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_downloadlinkrepair'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=additionallink&action=adddownload&resourceid='.$chapter->getId().'&type=VietsubmangaChapter',
                        'confirm' => 0
                    );
                }

                if(can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich')) && $chapter->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_delete'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=delete&id='.$chapter->getId(),
                        'confirm' => 1
                    );
                }
                if(can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich')) && !$chapter->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_restore'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=restore&id='.$chapter->getId(),
                        'confirm' => 1
                    );
                }
                if(can_administer() && $chapter->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_deletehard'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=delete&hard=1&id='.$chapter->getId(),
                        'confirm' => 1
                    );
                }
                $dataView['chapters'][$key]['action'] = $action;
            }
            $dataView['pagenav'] = construct_pagenavigation($page, $perPage, $totalItem, stripParam(getCurrentUrl(), 'page'));
        } else {
            $dataView['msgType'] = 'info';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_info_emptylist'), $forum->getPhrase('yrms_chapter'), $forum->getPhrase('yrms_chapteradd'));
        }

        $this->renderView('yrms_vietsubmanga_chapter_list', $dataView);
    }

    public function addAction()
    {
        $forum = callHelper('Forum');

        $chapter = callModel('VietsubmangaChapter');
        if(!$chapter->getManga()->getId()){
            $chapter->getManga()->load(getParam('mangaid'));
        }

        if(!$chapter->getManga()->getId()) {
            $dataView['msgType'] = 'error';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_error_thisnotexisted'), $forum->getPhrase('yrms_vietsubmanga'));
            $this->renderView('yrms_message', $dataView);
        }
        $dataView['manga'] = $chapter->getManga()->getData();

        $form = callHelper('Form');
        $dataView['pageTitle'] = $forum->getPhrase('yrms_chapteradd');
        $dataView['msgType'] = 'info';
        $dataView['msg'] = $forum->getPhrase('yrms_inputtip');
        if(isPost()) {
            $this->setBasicRule();
            if($form->validate()) {
                $inputData = $this->convertData(getPost());
                $this->saveChapter($inputData);
                $forum->rebuildForum($forum->getConfig('yrms_vietsubmanga_id_truyendich'));

                $awardInfo = array();
                foreach($chapter->getEarnedAward() as $userId => $amount) {
                    $userName = callModel('ForumUser')->load($userId)->getUserName();
                    if($userName) {
                        $awardInfo[] = "$userName: $amount ".$forum->getConfig('yrms_main_moneyname');
                    }
                }
                $awardInfo = implode('</br>', $awardInfo);

                $dataView['msgType'] = 'success';
                $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_newchapter'),
                    $forum->getUserInfo('username'),
                    $forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode(),
                    $chapter->getManga()->getTitle(),
                    $awardInfo);
                $this->renderView('yrms_message', $dataView);
            } else {
                $dataView['msgType'] = 'error';
                $dataView['msg'] = $form->getErrorMsg();
            }
        } else {
            $formPreData = array(
                'fansubname' => $dataView['manga']['fansubname'],
                'fansubsite' => $dataView['manga']['fansubsite']
            );
            if($chapter->getManga()->getType() == 2) {

            }
        }

        $form->setFormData($formPreData);
        $this->renderView('yrms_vietsubmanga_chapter_form', $dataView);
    }

    public function editAction()
    {
        $forum = callHelper('Forum');
        $chapter = callModel('VietsubmangaChapter')->load(getParam('id'));
        if(!$chapter->getManga()->isOwner()) {
            print_no_permission();
        }

        $dataView['pageTitle'] = $forum->getPhrase('yrms_edit').' - '.$chapter->getManga()->getTitle().': '.$forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode();
        $dataView['msgType'] = 'info';
        $dataView['msg'] = $forum->getPhrase('yrms_inputtip');

        $form = callHelper('Form');
        $formData = $chapter->getData();
        foreach($formData['fansubmember'] as $position => $members) {
            foreach($members as $key => $member) {
                $members[$key] = callModel('ForumUser')->load($member)->getUserName();
            }
            $formData['fansubmember'][$position] = implode(',', $members);
        }
        $formData['totalhost'] = multiDimensCount($formData['downloadlink']);
        $hostNumber = 1;
        foreach($formData['downloadlink'] as $hostname => $hostlinks) {
            foreach($hostlinks as $hostlink) {
                $formData['hostname'.$hostNumber] = $hostname;
                $formData['hostlink'.$hostNumber] = $hostlink;
                $hostNumber++;
            }

        }
        $form->setFormData($formData);

        if(isPost()) {
            $rule = array('adult' => array($forum->getPhrase('yrms_adult'), 'noblank'));
            if(getPost('type') == 1) {
                $rule += array('code' => array($forum->getPhrase('yrms_chaptercode'), 'noblank ud_codeValidate'));
            }
            $form->setRule($rule);

            if($form->validate()) {
                $inputData = $this->convertData(array_merge(getPost(), array('id' => getParam('id'))));
                $this->saveChapter($inputData);

                $dataView['msgType'] = 'success';
                $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_general'),
                    $forum->getPhrase('yrms_edit'),'',
                    $forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode());
                $this->renderView('yrms_message', $dataView);
            } else {
                $dataView['msgType'] = 'error';
                $dataView['msg'] = $form->getErrorMsg();
            }
        }

        $this->renderView('yrms_vietsubmanga_chapter_form', $dataView);
    }

    public function rewardAction()
    {
        $forum = callHelper('Forum');
        if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
            print_no_permission();
        }

        $form = callHelper('Form');
        $dataView['pageTitle'] = $forum->getPhrase('yrms_newchapter_award');
        $dataView['msgType'] = 'info';
        $dataView['msg'] = $forum->getPhrase('yrms_inputtip');
        if(isPost()) {
            $this->setBasicRule();
            $rule = array(
                'posturlmanga' => array($forum->getPhrase('yrms_posturl').' '.$forum->getPhrase('yrms_vietsubmanga'), 'noblank isurl ud_posturlmangaValidate'),
                'posturldownload' => array($forum->getPhrase('yrms_posturl').' '.$forum->getPhrase('yrms_downloadlink'), 'noblank isurl ud_posturldownloadValidate'),
                'posturlstream' => array($forum->getPhrase('yrms_posturl').' '.$forum->getPhrase('yrms_readlink'), 'noblank isurl ud_posturlstreamValidate')
            );
            $form->addRule($rule);
            if(getPost('posturldownload')) {
                if(!getPost('posturlstream')) {
                    $form->unsetRule('posturlstream');
                }

            }
            if(getPost('posturlstream')) {
                $form->addRule(array('streamlink' => array($forum->getPhrase('yrms_readlink'), 'noblank')));
                if(!getPost('posturldownload')) {
                    $form->unsetRule('posturldownload');
                    $form->unsetRule('totalhost');
                }

            }

            if($form->validate()) {
                $inputData = $this->convertData(getPost());
                $chapter = callModel('VietsubmangaChapter');
                $chapter->setManga($inputData['manga']);
                $this->saveChapter($inputData);
                $forum->rebuildForum($forum->getConfig('yrms_vietsubmanga_id_truyendich'));

                $awardInfo = array();
                foreach($chapter->getEarnedAward() as $userId => $amount) {
                    $userName = callModel('ForumUser')->load($userId)->getUserName();
                    if($userName) {
                        $awardInfo[] = "$userName: $amount ".$forum->getConfig('yrms_main_moneyname');
                    }
                }
                $awardInfo = implode('</br>', $awardInfo);

                $dataView['msgType'] = 'success';
                $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_reward'),
                    $forum->getUserInfo('username'),
                    $forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode(),
                    $forum->getPhrase('yrms_vietsubmanga').' '.$chapter->getManga()->getTitle(),
                    $awardInfo);
                $this->renderView('yrms_message', $dataView);
            } else {
                $dataView['msgType'] = 'error';
                $dataView['msg'] = $form->getErrorMsg();
            }
        }

        $this->renderView('yrms_vietsubmanga_chapter_form', $dataView);
    }

    public function addStreamAction()
    {
        $chapter = callModel('VietsubmangaChapter')->load(getParam('id'));
        $forum = callHelper('Forum');
        $form = callHelper('Form');
        $dataView['pageTitle'] = $forum->getPhrase('yrms_readlinkadd').' - '.$chapter->getManga()->getTitle().': '.$forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode();
        if($chapter->getStreamLink()) {
            $dataView['msgType'] = 'error';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_error_thisexisted'), $forum->getPhrase('yrms_readlink'));
            $this->renderView('yrms_message', $dataView);
        }

        if(isPost()) {
            $form->setRule(array('streamlink' => array($forum->getPhrase('yrms_readlink'), 'noblank')));
            if($form->validate()) {
                $chapter->setStreamLink(getPost('streamlink'))->save();

                $awardInfo = $chapter->getEarnedAward();
                $awardInfo = reset($awardInfo).' '.$forum->getConfig('yrms_main_moneyname');
                $dataView['msgType'] = 'success';
                $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_newreadstream'),
                    $forum->getUserInfo('username'),
                    $chapter->getManga()->getTitle().': '.$forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode(),
                    $awardInfo);
                $this->renderView('yrms_message', $dataView);
            } else {
                $dataView['msgType'] = 'error';
                $dataView['msg'] = $form->getErrorMsg();
            }
        }

        $this->renderView('yrms_additionallink_streamread_form', $dataView);
    }

    public function deleteAction()
    {
        if(getParam('controller', $_SERVER['HTTP_REFERER']) != 'vietsubmangachapter' || getParam('action', $_SERVER['HTTP_REFERER']) != 'list') {
            print_no_permission();
        }
        $forum = callHelper('Forum');
        $chapter = getModel('VietsubmangaChapter')->load(getParam('id'));
        if(!$chapter->getId() || !$chapter->getActive()) {
            $forwardData['msgType'] = 'error';
            $forwardData['msg'] = $forum->getPhrase('yrms_msg_error_general');
            setSession('forwardData', $forwardData);
            header("Location: {$_SERVER['HTTP_REFERER']}");
        }
        $detailedInfo = $forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode().' - '.$chapter->getManga()->getTitle();

        switch(getParam('hard')) {
            case 1:
                if(!can_administer()) {
                    print_no_permission();
                }
                $deleteSuccessful = $chapter->delete();
                break;
            default:
                if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
                    print_no_permission();
                }
                $deleteSuccessful = $chapter->softDelete();
                break;
        }

        if($deleteSuccessful) {
            $forwardData['msgType'] = 'success';
            $deletePhrase = (getParam('hard')) ? $forum->getPhrase('yrms_deletehard') : $forum->getPhrase('yrms_delete');
            $forwardData['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_general'),
                $deletePhrase,
                '',
                $detailedInfo);
        } else {
            $forwardData['msgType'] = 'error';
            $forwardData['msg'] = $forum->getPhrase('yrms_msg_error_general');
            setSession('forwardData', $forwardData);
            header("Location: {$_SERVER['HTTP_REFERER']}");
        }


        setSession('forwardData', $forwardData);
        header("Location: {$_SERVER['HTTP_REFERER']}");
    }

    public function restoreAction()
    {
        if(getParam('controller', $_SERVER['HTTP_REFERER']) != 'vietsubmangachapter' || getParam('action', $_SERVER['HTTP_REFERER']) != 'list') {
            print_no_permission();
        }
        $forum = callHelper('Forum');
        if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
            print_no_permission();
        }

        $chapter = getModel('VietsubmangaChapter')->load(getParam('id'));
        if(!$chapter->getId() || $chapter->getActive()) {
            $forwardData['msgType'] = 'error';
            $forwardData['msg'] = $forum->getPhrase('yrms_msg_error_general');
            setSession('forwardData', $forwardData);
            header("Location: {$_SERVER['HTTP_REFERER']}");
        }
        $detailedInfo = $forum->getPhrase('yrms_chaptertype'.$chapter->getType()).' '.$chapter->getCode().' - '.$chapter->getManga()->getTitle();
        $restoreSuccessful = $chapter->restore();

        if($restoreSuccessful) {
            $forwardData['msgType'] = 'success';
            $forwardData['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_general'),
                $forum->getPhrase('yrms_restore'),
                '',
                $detailedInfo);
        } else {
            $forwardData['msgType'] = 'error';
            $forwardData['msg'] = $forum->getPhrase('yrms_msg_error_general');
            setSession('forwardData', $forwardData);
            header("Location: {$_SERVER['HTTP_REFERER']}");
        }


        setSession('forwardData', $forwardData);
        header("Location: {$_SERVER['HTTP_REFERER']}");
    }

    public function generateHostFieldsAction()
    {
        $forum = callHelper('Forum');
        $totalHost = getPost('totalhost');
        $hostFields = '';
        if($totalHost == 0) {
            echo '';
            die();
        }

        for($i = 1; $i <= $totalHost; $i++) {
            ${'hostname'.$i} = getPost('hostname'.$i);
            ${'hostlink'.$i} = getPost('hostlink'.$i);

            $hostFields .= "<li>";
            $hostFields .= "<label><input required type='text' class='primary textbox' style='width:180px;' placeholder='".$forum->getPhrase('yrms_hostname')." $i' name='hostname$i' value='".${'hostname'.$i}."'/></label>";
            $hostFields .= "<input required type='text' class='primary textbox' placeholder='".$forum->getPhrase('yrms_hostlink')." $i' name='hostlink$i' value='".${'hostlink'.$i}."'/>";
            $hostFields .= "</li>";
        }

        echo $hostFields;
    }

    public function codeValidate($inputValue)
    {
        $forum = callHelper('Forum');
        $form = callHelper('Form');
        $form->addMsg(array('codeValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_thisexisted'), $forum->getPhrase('yrms_chapter').' '.$inputValue)));

        if(getParam('action') == 'edit') {
            $chapter = getModel('VietsubmangaChapter')->load(getParam('id'));
            $existedChapter = getModel('VietsubmangaChapter')->loadByMangaCode($chapter->getManga()->getId(), $inputValue);
            if($existedChapter->getCode() != $chapter->getCode() && $existedChapter->getCode() == $inputValue) {
                return false;
            }
            return true;
        }
        $mangaId = (getPost('posturlmanga')) ? callModel('VietsubmangaManga')->load(extract_threadid_from_url(getPost('posturlmanga')), 'threadid')->getId() : getParam('mangaid');

        $existedChapter = getModel('VietsubmangaChapter')->loadByMangaCode($mangaId, $inputValue);
        if($existedChapter->getId()) {
            if(getParam('action') != 'reward') {
                return false;
            }
            if($existedChapter->getPostId() && getPost('posturldownload')) {
                return false;
            }
            if($existedChapter->getStreamThreadId() && getPost('posturlstream')) {
                return false;
            }
        }
        return true;
    }

    public function posturlmangaValidate($inputValue)
    {
        $threadId = extract_threadid_from_url($inputValue);
        $mangaId = callModel('VietsubmangaManga')->load($threadId, 'threadid')->getId();
        if($mangaId) {
            return true;
        } else {
            $forum = callHelper('Forum');
            $form = callHelper('Form');
            if(!callModel('ForumThread')->load($threadId)->getThreadId()) {
                $form->addMsg(array('posturlmangaValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_thisnotexisted'), $forum->getPhrase('alp_posts'))));
            } else {
                $form->addMsg(array('posturlmangaValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_postnotappropriate'), $forum->getPhrase('yrms_vietsubmanga'))));
            }
            return false;
        }
    }

    public function posturldownloadValidate($inputValue)
    {
        $threadId = extract_threadid_from_url($inputValue);
        $postId = getParam('p', $inputValue);
        $mangaIdFromMangaLink = callModel('VietsubmangaManga')->load(extract_threadid_from_url(getPost('posturlmanga')), 'threadid')->getId();
        $mangaIdFromDownloadLink = callModel('VietsubmangaManga')->load($threadId, 'threadid')->getId();
        $isYrmsPost = callModel('ForumPost')->load($postId)->getYrmsPost();

        if($mangaIdFromMangaLink == $mangaIdFromDownloadLink && !$isYrmsPost) {
            return true;
        } else {
            $forum = callHelper('Forum');
            $form = callHelper('Form');
            if(!callModel('ForumPost')->load($postId)->getPostId()) {
                $form->addMsg(array('posturldownloadValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_thisnotexisted'), $forum->getPhrase('alp_posts'))));
            } elseif($mangaIdFromMangaLink != $mangaIdFromDownloadLink && $mangaIdFromDownloadLink){
                $form->addMsg(array('posturldownloadValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_postnotrelevant'), $forum->getPhrase('yrms_posturl').' '.$forum->getPhrase('yrms_vietsubmanga'))));
            } elseif($isYrmsPost) {
                $form->addMsg(array('posturldownloadValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_alreadyrewarded'), $forum->getPhrase('alp_posts'))));
            } else {
                $form->addMsg(array('posturldownloadValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_postnotappropriate'), $forum->getPhrase('yrms_vietsubmanga'))));
            }
            return false;
        }
    }

    public function posturlstreamValidate($inputValue)
    {
        $forum = callHelper('Forum');
        $thread = callModel('ForumThread')->load(extract_threadid_from_url($inputValue));
        $streamForums = array_merge(fetch_child_forums($forum->getConfig('yrms_vietsubmanga_id_doconline'), 'ARRAY'), array($forum->getConfig('yrms_vietsubmanga_id_doconline')));
        $isYrmsPost = callModel('ForumPost')->load($thread->getFirstPostId())->getYrmsPost();
        if(in_array($thread->getForumId(), $streamForums) && !$isYrmsPost) {
            return true;
        } else {
            $form = callHelper('Form');
            if(!$thread->getThreadId()) {
                $form->addMsg(array('posturlstreamValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_thisnotexisted'), $forum->getPhrase('alp_posts'))));
            } elseif($isYrmsPost) {
                $form->addMsg(array('posturlstreamValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_alreadyrewarded'), $forum->getPhrase('alp_posts'))));
            } elseif(!in_array($thread->getForumId(), $streamForums)) {
                $form->addMsg(array('posturlstreamValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_postnotappropriate'), $forum->getPhrase('yrms_readlink'))));
            }
            return false;
        }
    }

    protected function saveChapter($inputData)
    {
        $forum = callHelper('Forum');
        $chapter = callModel('VietsubmangaChapter');
        if(getParam('action') == 'edit' && !can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
            $chapter->load($inputData['id'])
                ->setCode($inputData['code'])
                ->setTitle($inputData['title'])
                ->setType($inputData['type'])
                ->setAdult($inputData['adult'])
                ->setFansubnote($inputData['fansubnote']);

            if(!empty($inputData['downloadlink'])) {
                $chapter->setDownloadLink($inputData['downloadlink']);
            }
            if(!empty($inputData['streamlink'])) {
                $chapter->setStreamLink($inputData['streamlink']);
            }

            $chapter->save();
        } else {
            if(getParam('action') == 'reward') {
                $mangaId = (getParam('mangaid')) ? (getParam('mangaid')) : $inputData['manga']->getId();
                $chapter->loadByMangaCode($mangaId, $inputData['code']);
            } else {
                $chapter->load(getParam('id'));
            }
            $chapter
                ->setCode($inputData['code'])
                ->setTitle($inputData['title'])
                ->setLength($inputData['length'])
                ->setType($inputData['type'])
                ->setAdult($inputData['adult'])
                ->setMangaId($chapter->getManga()->getId())
                ->setPostId($inputData['postid'])
                ->setStreamThreadId($inputData['streamthreadid'])
                ->setFansubName($inputData['fansubname'])
                ->setFansubSite($inputData['fansubsite'])
                ->setFansubmember($inputData['fansubmember'])
                ->setFansubnote($inputData['fansubnote'])
                ->setTotalHost($inputData['totalhost']);

            if(!empty($inputData['downloadlink'])) {
                $chapter->setDownloadLink($inputData['downloadlink']);
            }
            if(!empty($inputData['streamlink'])) {
                $chapter->setStreamLink($inputData['streamlink']);
            }

            $fansubName = $chapter->getManga()->getFansubName();
            if(!in_array($inputData['fansubname'], $fansubName)) {
                $fansubName[] = $inputData['fansubname'];
            }

            $fansubSite = $chapter->getManga()->getFansubSite();
            if(!in_array($inputData['fansubsite'], $fansubSite)) {
                $fansubSite[] = $inputData['fansubsite'];
            }

            $chapter->getManga()
                ->setStatus($inputData['status'])
                ->setFansubmember(mergeFansubmember($chapter->getManga()->getFansubmember(), $chapter->getFansubmember()))
                ->setFansubName($fansubName)
                ->setFansubSite($fansubSite);

            $chapter->save();
        }
    }

    protected function convertData($rawData)
    {
        unset($rawData['ajax']);

        if(is_array($rawData['fansubmember'])) {
            foreach($rawData['fansubmember'] as $position => $members) {
                $members = explode(',', $members);
                foreach($members as $key => $member) {
                    if(empty($member)) {
                        unset($rawData['fansubmember'][$position]);
                        break;
                    }
                    $memberId = callModel('ForumUser')->load(trim($member), 'username')->getUserId();
                    if($memberId) {
                        $members[$key] = $memberId;
                    }
                }
                $rawData['fansubmember'][$position] = $members;
            }
        }



        for($i = 1; $i <= $rawData['totalhost']; $i++) {
            $rawData['downloadlink'][$rawData["hostname$i"]][] = $rawData["hostlink$i"];
            unset($rawData["hostname$i"]);
            unset($rawData["hostlink$i"]);
        }

        if($rawData['posturlstream']) {
            $streamThreadId = extract_threadid_from_url($rawData['posturlstream']);
            if(callModel('ForumThread')->load($streamThreadId)->getForumId() == callHelper('Forum')->getConfig('yrms_vietsubmanga_id_doconline')) {
                $rawData['streamthreadid'] = extract_threadid_from_url($rawData['posturlstream']);
            }
        } elseif(getParam('action') == 'reward') {
            unset($rawData['streamlink']);
        }

        if($rawData['posturlmanga']) {
            $mangaThreadId = extract_threadid_from_url($rawData['posturlmanga']);
            $rawData['manga'] = callModel('VietsubmangaManga')->load($mangaThreadId, 'threadid');
            if($rawData['posturldownload']) {
                if(callModel('ForumThread')->load($mangaThreadId)->getForumId() == callHelper('Forum')->getConfig('yrms_vietsubmanga_id_truyendich')) {
                    $rawData['postid'] = getParam('p', $rawData['posturldownload']);
                }
            } else {
                $rawData['totalhost'] = 0;
                unset($rawData['downloadlink']);
            }
        }

        return $rawData;
    }

    protected function convertToForm($chapter)
    {
        $formData = array(
            'type' => $chapter->getType(),
            'code' => $chapter->getCode(),
            'title' => $chapter->getTitle(),
            'length' => $chapter->getLength(),
            'adult' => $chapter->getAdult(),
            'fansubname' => $chapter->getFansubName(),
            'fansubsite' => $chapter->getFansubSite(),
            'fansubnote' => $chapter->getFansubNote(),
            'totalhost' => $chapter->getTotalHost(),
            'streamlink' => $chapter->getStreamLink()
        );

        $order = 1;
        foreach($chapter->getDownloadLink() as $hostName => $hostLinks) {
            foreach($hostLinks as $hostLink) {
                $formData["hostname$order"] = $hostName;
                $formData["hostlink$order"] = $hostLink;
                $order++;
            }
        }

        foreach($chapter->getFansubMember() as $position => $members) {
            foreach($members as $key => $member){
                $members[$key] = callModel('ForumUser')->load($member)->getUserName();
            }
            $formData['fansubmember'][$position] = implode(',', $members);
        }

        return $formData;
    }

    protected function setBasicRule()
    {
        $forum = callHelper('Forum');
        $form = callHelper('Form');
        $rule = array(
            'status' => array($forum->getPhrase('yrms_projectstatus'), 'noblank'),
            'adult' => array($forum->getPhrase('yrms_adult'), 'noblank'),
            'fansubsite' => array($forum->getPhrase('yrms_fansubsite'), 'isurl'),
            'totalhost' => array($forum->getPhrase('yrms_hosttotal'), 'noblank isinteger ispositive')
        );

        $totalHost = getPost('totalhost');
        for($i = 1; $i <= $totalHost; $i++) {
            $rule["hostname$i"] = array($forum->getPhrase('yrms_hostname')." $i", 'noblank');
            $rule["hostlink$i"] = array($forum->getPhrase('yrms_hostlink')." $i", 'isurl');
        }

        if(getPost('type') == 1) {
            $rule += array('code' => array($forum->getPhrase('yrms_chaptercode'), 'noblank ud_codeValidate'));
        }

        $form->setRule($rule);
    }

    protected function renderView($viewName, $dataView='')
    {
        if(strpos($viewName, 'yrms_vietsubmanga_chapter_form') !== false) {
            $forum = callHelper('Forum');
            $dataView['yunRange'] = explode("\n", $forum->getConfig('yrms_vietsubmanga_yun_range'));
        }
        parent::renderView($viewName, $dataView);
    }

}