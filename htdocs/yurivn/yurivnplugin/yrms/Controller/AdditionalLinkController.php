<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class AdditionalLinkController extends AncestorController
{
    protected $_model;

    protected function getResourceInfo($type)
    {
        $forum = callHelper('Forum');
        switch($type) {
            case 'VietsubmangaChapter':
                return array(
                    'forumId' => $forum->getConfig('yrms_vietsubmanga_id_truyendich'),
                    'titleSuffix' => $this->_model->getResource()->getManga()->getTitle().
                        ': '.$forum->getPhrase('yrms_chaptertype'.$this->_model->getResource()->getType()).
                        ' '.$this->_model->getResource()->getCode()
                );
        }
    }

    public function addDownloadAction()
    {
        $forum = callHelper('Forum');
        $form = callHelper('Form');
        $this->_model = callModel('AdditionalLink');
        $this->_model->setResource(callModel(getParam('type'))->load(getParam('resourceid')));

        $resourceInfo = $this->getResourceInfo(getParam('type'));
        $totalDownloadLink = count($this->_model->getResource()->getAllDownloadLink());

        $dataView['titleRoot'] = ($totalDownloadLink) ? $forum->getPhrase('yrms_mirrorlinkadd') : $forum->getPhrase('yrms_downloadlinkrepair');
        $dataView['pageTitle'] = $dataView['titleRoot'].' - '.$resourceInfo['titleSuffix'];
        if($totalDownloadLink > 1) {
            $dataView['msgType'] = 'error';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_error_thisexisted'), $forum->getPhrase('yrms_mirror'));
            $this->renderView('yrms_message', $dataView);
        }

        if(isPost()) {
            $totalHost = getPost('totalhost');
            for($i = 1; $i <= $totalHost; $i++) {
                $rule["hostname$i"] = array($forum->getPhrase('yrms_hostname')." $i", 'noblank');
                $rule["hostlink$i"] = array($forum->getPhrase('yrms_hostlink')." $i", 'isurl');
            }
            $form->setRule($rule);
            if($form->validate()) {
                $inputData = getPost();
                for($i = 1; $i <= $inputData['totalhost']; $i++) {
                    $inputData['downloadlink'][$inputData["hostname$i"]][] = $inputData["hostlink$i"];
                    unset($inputData["hostname$i"]);
                    unset($inputData["hostlink$i"]);
                }

                $this->_model
                    ->setType(1)
                    ->setLink($this->_model->convertLink($inputData['downloadlink']))
                    ->setResourceId(getParam('resourceid'))
                    ->setResourceType(getParam('type'))
                    ->save();
                $this->_model->getResource()->save();

                $forum->rebuildForum($resourceInfo['forumId']);

                $awardInfo = $this->_model->getResource()->getEarnedAward();
                $awardInfo = (@$awardInfo[$forum->getUserInfo('userid')]) ? @$awardInfo[$forum->getUserInfo('userid')] : 0;
                $awardInfo .= ' '.$forum->getConfig('yrms_main_moneyname');

                $dataView['msgType'] = 'success';
                $successPhrase = ($totalDownloadLink) ? $forum->getPhrase('yrms_msg_success_newmirror') : $forum->getPhrase('yrms_msg_success_repairlink') ;
                $dataView['msg'] = construct_phrase($successPhrase,
                    $forum->getUserInfo('username'),
                    $resourceInfo['titleSuffix'],
                    $awardInfo);
                $this->renderView('yrms_message', $dataView);
            } else {
               $dataView['msgType'] = 'error';
               $dataView['msg'] = $form->getErrorMsg();
            }
        }

        $this->renderView('yrms_additionallink_download_form', $dataView);
    }

    public function addReadStreamAction()
    {
        $forum = callHelper('Forum');
        $form = callHelper('Form');
        $this->_model = callModel('AdditionalLink');
        $this->_model->setResource(callModel(getParam('type'))->load(getParam('resourceid')));

        $resourceInfo = $this->getResourceInfo(getParam('type'));
        $dataView['pageTitle'] = $forum->getPhrase('yrms_readlinkadd').' - '.$resourceInfo['titleSuffix'];
        if(!emptyEnhanced(array_filter($this->_model->getResource()->getAllStreamLink()))) {
            $dataView['msgType'] = 'error';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_error_thisexisted'), $forum->getPhrase('yrms_readlink'));
            $this->renderView('yrms_message', $dataView);
        }

        if(isPost()) {
            $rule['streamlink'] = array($forum->getPhrase('yrms_readlink'), 'noblank');
            $form->setRule($rule);
            if($form->validate()) {
                $inputData = getPost();

                $this->_model
                    ->setType(2)
                    ->setLink($inputData['streamlink'])
                    ->setResourceId(getParam('resourceid'))
                    ->setResourceType(getParam('type'))
                    ->save();
                $this->_model->getResource()->save();

                $forum->rebuildForum($resourceInfo['forumId']);

                $awardInfo = $this->_model->getResource()->getEarnedAward();
                $awardInfo = (@$awardInfo[$forum->getUserInfo('userid')]) ? @$awardInfo[$forum->getUserInfo('userid')] : 0;
                $awardInfo .= ' '.$forum->getConfig('yrms_main_moneyname');
                $dataView['msgType'] = 'success';
                $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_newreadstream'),
                    $forum->getUserInfo('username'),
                    $resourceInfo['titleSuffix'],
                    $awardInfo);
                $this->renderView('yrms_message', $dataView);
            } else {
                $dataView['msgType'] = 'error';
                $dataView['msg'] = $form->getErrorMsg();
            }
        }

        $this->renderView('yrms_additionallink_streamread_form', $dataView);
    }
}