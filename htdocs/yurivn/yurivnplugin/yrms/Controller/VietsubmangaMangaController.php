<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class VietsubmangaMangaController extends AncestorController
{
    public function listAction()
    {
        $forum = callHelper('Forum');
        $mangaModel = getModel('VietsubmangaManga');
        if(getParam('owner')) {
            $ownerName = callModel('ForumUser')->load(getParam('owner'))->getUserName();
            $dataView['owner'] = getParam('owner');
        }
        $dataView['pageTitle'] = ($ownerName) ? construct_phrase($forum->getPhrase('yrms_projectof'), $ownerName) : $forum->getPhrase('yrms_vietsubmangalist');

        $data = getSession('forwardData');
        unsetSession('forwardData');
        if(!empty($data)) {
            $forum->rebuildForum($forum->getConfig('yrms_vietsubmanga_id_truyendich'));
            $dataView['msgType'] = $data['msgType'];
            $dataView['msg'] = $data['msg'];
        }

        $page = getParam('page');
        $perPage = 20;
        if(empty($page)) {
            $page = 1;
        }

        $filter = getParam('filter');
        $keyWord = getParam('keyword');
        if(!empty($filter) && !empty($keyWord)) {
            $mangaModel->setFilter($filter, '%'.$keyWord.'%');
        }

        if(getParam('owner')) {
            $mangas = $mangaModel->getCollectionByOwner(getParam('owner'));
            $totalItem = $mangas->count();
        } else {
            $mangas = $mangaModel->setLimit($perPage, ($page-1)*$perPage)
                ->getCollection();
            $totalItem = $mangaModel->getTotalItem();
        }
        $dataView['totalManga'] = $totalItem;
        if($mangas->count() == 0 && getParam('page') > 1) {
            $redirectUrl = addParam(getCurrentUrl(), 'page', getParam('page') - 1);
            header("Location: $redirectUrl");
        }


        if($totalItem > 0) {
            $dataView['mangas'] = array();
            foreach($mangas as $key => $manga) {
                $dataView['mangas'][$key] = $manga->fetchData();
                $action = array();
                if(($manga->isOwner($forum->getUserInfo('userid')) || can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich')))
                    && $manga->getActive()
                ) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_edit'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangamanga&action=edit&id='.$manga->getId()
                    );
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_chaptermanage'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=list&mangaid='.$manga->getId()
                    );
                } elseif($manga->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_chaptersee'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangachapter&action=list&mangaid='.$manga->getId()
                    );
                }
                if(can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich')) && $manga->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_delete'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangamanga&action=delete&id='.$manga->getId(),
                        'confirm' => 1
                    );
                }
                if(can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich')) && !$manga->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_restore'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangamanga&action=restore&id='.$manga->getId(),
                        'confirm' => 1
                    );
                }
                if(can_administer() && $manga->getActive()) {
                    $action[] = array(
                        'name' => $forum->getPhrase('yrms_deletehard'),
                        'url' => 'yurivnplugin/yrms/index.php?controller=vietsubmangamanga&action=delete&hard=1&id='.$manga->getId(),
                        'confirm' => 1
                    );
                }
                $dataView['mangas'][$key]['action'] = $action;
            }
            $dataView['pagenav'] = construct_pagenavigation($page, $perPage, $totalItem, stripParam(getCurrentUrl(), 'page'));
        } else {
            $dataView['msgType'] = 'info';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_info_emptylist'), $forum->getPhrase('yrms_vietsubmanga'), $forum->getPhrase('yrms_mangaadd'));
        }

        $this->renderView('yrms_vietsubmanga_manga_list', $dataView);
    }

    public function addAction()
    {
        $forum = callHelper('Forum');
        if(!$forum->getUserInfo('userid')) {
            print_no_permission();
        }

        $form = callHelper('Form');
        $dataView['pageTitle'] = $forum->getPhrase('yrms_mangaadd');
        $dataView['msgType'] = 'info';
        $dataView['msg'] = $forum->getPhrase('yrms_inputtip');
        if(isPost()) {
            $this->setBasicRule();

            if($form->validate()) {
                $inputData = $this->convertData(getPost());
                $this->saveManga($inputData);
                $forum->rebuildForum($forum->getConfig('yrms_vietsubmanga_id_truyendich'));

                $manga = callModel('VietsubmangaManga');
                $dataView['msgType'] = 'success';
                $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_newmanga'),
                                                    $forum->getUserInfo('username'),
                                                    $manga->getTitle(),
                                                    $manga->getEarnedAward(),
                                                    $manga->getId());
                $this->renderView('yrms_message', $dataView);
            } else {
                $dataView['msgType'] = 'error';
                $dataView['msg'] = $form->getErrorMsg();
            }
        }

        $this->renderView('yrms_vietsubmanga_manga_form', $dataView);
    }

    public function editAction()
    {
        $forum = callHelper('Forum');
        $form = callHelper('Form');
        $manga = callModel('VietsubmangaManga')->load(getParam('id'));
        $dataView['pageTitle'] = $forum->getPhrase('yrms_edit');

        if(!$manga->getId()) {
            $dataView['msgType'] = 'error';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_error_thisnotexisted'), $forum->getPhrase('yrms_vietsubmanga'));
            $this->renderView('yrms_message', $dataView);
        }

        if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich')) && !$manga->isOwner($forum->getUserInfo('userid'))) {
            print_no_permission();
        }

        $dataView['msgType'] = 'info';
        $dataView['msg'] = $forum->getPhrase('yrms_inputtip');
        $formData = $manga->getData();
        $formData['fansubname'] = implode(',', $manga->getFansubName());
        $formData['fansubsite'] = implode(',', $manga->getFansubSite());
        $form->setFormData($formData);

        if(isPost()) {
            $inputData = $this->convertData(getPost() + array('id' => getParam('id')));
            $this->setBasicRule();

            if($form->validate()) {
                $this->saveManga($inputData);
                $dataView['msgType'] = 'success';
                $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_general'),
                    $forum->getPhrase('yrms_edit'),
                    $forum->getPhrase('yrms_vietsubmanga'),
                    $inputData['title']);
                $this->renderView('yrms_message', $dataView);
            } else {
                $dataView['msgType'] = 'error';
                $dataView['msg'] = $form->getErrorMsg();
            }
        }

        $this->renderView('yrms_vietsubmanga_manga_form', $dataView);
    }

    public function rewardAction()
    {
        $forum = callHelper('Forum');
        if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
            print_no_permission();
        }

        $form = callHelper('Form');

        $dataView['pageTitle'] = $forum->getPhrase('yrms_reward');
        $dataView['msgType'] = 'info';
        $dataView['msg'] = $forum->getPhrase('yrms_rewardtip');

        if(isPost()) {
            $this->setBasicRule();
            $rule = array(
                'posturl' => array($forum->getPhrase('yrms_posturl'), 'noblank isurl ud_posturlValidate')
            );
            $form->addRule($rule);

            if($form->validate()) {
                $inputData = $this->convertData(getPost());
                $this->saveManga($inputData);
                $dataView['msgType'] = 'success';
                $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_general'),
                                                    $forum->getPhrase('yrms_reward'),
                                                    $forum->getPhrase('yrms_vietsubmanga'),
                                                    $inputData['title']);
                $this->renderView('yrms_message', $dataView);
            } else {
                $dataView['msgType'] = 'error';
                $dataView['msg'] = $form->getErrorMsg();
            }
        }
        $this->renderView('yrms_vietsubmanga_manga_form', $dataView);
    }

    public function deleteAction()
    {
        if(getParam('controller', $_SERVER['HTTP_REFERER']) != 'vietsubmangamanga' || getParam('action', $_SERVER['HTTP_REFERER']) != 'list') {
            print_no_permission();
        }
        $forum = callHelper('Forum');
        $manga = getModel('VietsubmangaManga')->load(getParam('id'));
        if(!$manga->getId() || !$manga->getActive()) {
            $forwardData['msgType'] = 'error';
            $forwardData['msg'] = $forum->getPhrase('yrms_msg_error_general');
            setSession('forwardData', $forwardData);
            header("Location: {$_SERVER['HTTP_REFERER']}");
        }

        switch(getParam('hard')) {
            case 1:
                if(!can_administer()) {
                    print_no_permission();
                }
                $deleteSuccessful = $manga->delete();
                break;
            default:
                if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
                    print_no_permission();
                }
                $deleteSuccessful = $manga->softDelete();
                break;
        }

        if($deleteSuccessful) {
            getModel('VietsubmangaManga')->rebuildInforumList();
            $forwardData['msgType'] = 'success';
            $deletePhrase = (getParam('hard')) ? $forum->getPhrase('yrms_deletehard') : $forum->getPhrase('yrms_delete');
            $forwardData['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_general'),
                $deletePhrase,
                '',
                $manga->getTitle());
        } else {
            $forwardData['msgType'] = 'error';
            $forwardData['msg'] = $forum->getPhrase('yrms_msg_error_general');
            setSession('forwardData', $forwardData);
            header("Location: {$_SERVER['HTTP_REFERER']}");
        }


        setSession('forwardData', $forwardData);
        header("Location: {$_SERVER['HTTP_REFERER']}");
    }

    public function restoreAction()
    {
        if(getParam('controller', $_SERVER['HTTP_REFERER']) != 'vietsubmangamanga' || getParam('action', $_SERVER['HTTP_REFERER']) != 'list') {
            print_no_permission();
        }
        $forum = callHelper('Forum');
        if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
            print_no_permission();
        }

        $manga = getModel('VietsubmangaManga')->load(getParam('id'));
        if(!$manga->getId() || $manga->getActive()) {
            $forwardData['msgType'] = 'error';
            $forwardData['msg'] = $forum->getPhrase('yrms_msg_error_general');
            setSession('forwardData', $forwardData);
            header("Location: {$_SERVER['HTTP_REFERER']}");
        }

        $restoreSuccessful = $manga->restore();

        if($restoreSuccessful) {
            getModel('VietsubmangaManga')->rebuildInforumList();
            $forwardData['msgType'] = 'success';
            $forwardData['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_general'),
                $forum->getPhrase('yrms_restore'),
                '',
                $manga->getTitle());
        } else {
            $forwardData['msgType'] = 'error';
            $forwardData['msg'] = $forum->getPhrase('yrms_msg_error_general');
            setSession('forwardData', $forwardData);
            header("Location: {$_SERVER['HTTP_REFERER']}");
        }


        setSession('forwardData', $forwardData);
        header("Location: {$_SERVER['HTTP_REFERER']}");
    }

    public function dupplicateWarningAjaxAction()
    {
        if(getPost('mangaTitles')) {
            $mangaTitles = explode(',',getPost('mangaTitles'));
            $mangaModel = callModel('VietsubmangaManga');
            foreach($mangaTitles as $mangaTitle) {
                if($mangaTitle) {
                    $mangaModel->addFilter('title', $mangaTitle);
                    $mangaModel->addFilter('othertitle', $mangaTitle);
                }
            }

            $mangaCollection = $mangaModel->getCollection();

            if($mangaCollection->count()) {
                $forum = getHelper('Forum');
                $existedTitle = array();
                foreach($mangaCollection as $existedManga) {
                    $existedTitle[] = $existedManga->getTitle();
                }

                $messagebox = vB_Template::create('yrms_messagebox');
                $messagebox->register('messagetype', 'warn');
                $messagebox->register('message', nl2br(construct_phrase($forum->getPhrase('yrms_ajax_dupplicatewarning'), $forum->getPhrase('yrms_vietsubmanga'), implode("\n", $existedTitle))));

                echo $messagebox->render();
            }
        }
    }

    public function posturlValidate($inputValue)
    {
        $threadId = extract_threadid_from_url($inputValue);
        $forum = callHelper('Forum');
        $form = callHelper('Form');
        $thread = callModel('ForumThread')->load($threadId);

        $vietsubMangaForumIds = array($forum->getConfig('yrms_vietsubmanga_id_truyendich'), $forum->getConfig('yrms_vietsubmanga_id_doconline'));
        if($threadId) {
            if(callModel('ForumPost')->load($threadId, 'threadid')->getYrmsPost()) {
                $form->addMsg(array('posturlValidate' => $forum->getPhrase('yrms_msg_error_postrewarded')));
                return false;
            }

            $forumId = $thread->getForumId();
            if(in_array($forumId, $vietsubMangaForumIds)) {
                return true;
            }
        }
        $form->addMsg(array('posturlValidate' => construct_phrase($forum->getPhrase('yrms_msg_error_postnotexisted'), $forum->getPhrase('yrms_posturl'), $forum->getPhrase('yrms_vietsubmanga'))));
        return false;
    }

    protected function saveManga($inputData)
    {
        $manga = callModel('VietsubmangaManga')->load($inputData['id']);
        $manga  ->getThread()->setThreadId($inputData['threadid'])->setPostId($inputData['firstpostid']);
        $manga  ->setImage($inputData['image'])
                ->setTitle($inputData['title'])
                ->setOtherTitle($inputData['othertitle'])
                ->setAuthor($inputData['author'])
                ->setType($inputData['type'])
                ->setTotalChapter($inputData['totalchapter'])
                ->setOriginalComposition($inputData['originalcomposition'])
                ->setGenre($inputData['genre'])
                ->setSummary($inputData['summary'])
                ->setFansubName($inputData['fansubname'])
                ->setFansubSite($inputData['fansubsite'])
                ->setFansubNote($inputData['fansubnote'])
                ->setStatus($inputData['status'])
                ->save();
        getModel('VietsubmangaManga')->rebuildInforumList();
        return true;
    }

    protected function convertData($rawData)
    {
        unset($rawData['ajax']);
        setIfEmpty($rawData['status'], 2);

        if($rawData['posturl']) {
            if(callModel('ForumThread')->load(extract_threadid_from_url($rawData['posturl']))->getForumId() == callHelper('Forum')->getConfig('yrms_vietsubmanga_id_truyendich')) {
                $rawData['threadid'] = extract_threadid_from_url($rawData['posturl']);
                $rawData['firstpostid'] = callModel('ForumThread')->load($rawData['threadid'])->getFirstPostId();
            }
        }

        $rawData['fansubname'] = ($rawData['fansubname']) ? explode(',', $rawData['fansubname']) : array();
        $rawData['fansubsite'] = ($rawData['fansubsite']) ? explode(',', $rawData['fansubsite']) : array();

        return $rawData;
    }

    protected function setBasicRule()
    {
        $forum = callHelper('Forum');
        $form = callHelper('Form');
        $rule = array(
            'image' => array($forum->getPhrase('yrms_image'), 'trim noblank isurl'),
            'title' => array($forum->getPhrase('yrms_mangatitle'), 'noblank'),
            'othertitle' => array($forum->getPhrase('yrms_othertitle'), 'other[title|'.$forum->getConvertedPhrase('yrms_mangatitle').']'),
            'type' => array($forum->getPhrase('yrms_type'), 'noblank'),
            'genre' => array($forum->getPhrase('yrms_genre'), 'noblank'),
            'summary' => array($forum->getPhrase('yrms_summary'), 'noblank')
        );

        if(getPost('type') == 3) {
            $rule += array('originalcomposition' => array('', 'noblank'));
        }

        $form->setRule($rule);
    }
}