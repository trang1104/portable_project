<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class ReportController extends AncestorController
{
    protected $_model;

    public function __construct()
    {
        $this->_model = getModel('Report');
        parent::__construct();
    }

    protected function getResourceInfo($type, $model = '')
    {
        $forum = callHelper('Forum');
        $model = ($model) ? $model : $this->_model;
        switch($type) {
            case 'VietsubmangaChapter':
                return array(
                    'forumId' => $forum->getConfig('yrms_vietsubmanga_id_truyendich'),
                    'name' => $model->getResource()->getManga()->getTitle().
                        ': '.$forum->getPhrase('yrms_chaptertype'.$model->getResource()->getType()).
                        ' '.$model->getResource()->getCode()
                );
        }
    }

    public function listAction()
    {
        $forum = callHelper('Forum');
        if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))){
            print_no_permission();
        }

        $dataView['pageTitle'] = $forum->getPhrase('yrms_report_approving');

        $data = getSession('forwardData');
        unsetSession('forwardData');
        if(!empty($data)) {
            $forum->rebuildForum($forum->getConfig('yrms_vietsubmanga_id_truyendich'));
            $dataView['msgType'] = $data['msgType'];
            $dataView['msg'] = $data['msg'];
        }

        $page = getParam('page');
        $perPage = $forum->getConfig('yrms_main_perpage');
        if(empty($page)) {
            $page = 1;
        }
        $reports = $this->_model->setFilter('resourcetype', getParam('resourcetype'))->getCollection();
        $dataView['totalReport'] = $totalItem = $reports->count();

        if($totalItem > 0) {
            $dataView['reports'] = array();
            foreach($reports as $key => $report) {
                $resourceInfo = $report->getResourceInfo();
                $threadId = $resourceInfo['headResource']->getThreadId();
                $threadInfo = fetch_threadinfo($threadId);
                $action = array();

                $dataView['reports'][$key] = array(
                    'id' => $report->getId(),
                    'resourcename' => $resourceInfo['name'],
                    'resourcethread' => fetch_full_seo_url('thread', $threadInfo),
                    'deadhostname' => str_replace('stream', 'Online', implode(', ', $report->getDeadHostName())),
                    'reporter' => $userName = callModel('ForumUser')->load($report->getReporter())->getUserName()
                );

                $action[] = array(
                    'name' => $forum->getPhrase('yrms_approve'),
                    'url' => 'yurivnplugin/yrms/index.php?controller=report&action=approve&id='.$report->getId(),
                    'confirm' => 1
                );

                $action[] = array(
                    'name' => $forum->getPhrase('yrms_delete'),
                    'url' => 'yurivnplugin/yrms/index.php?controller=report&action=delete&id='.$report->getId(),
                    'confirm' => 1
                );

                $dataView['reports'][$key]['action'] = $action;
            }
            $dataView['pagenav'] = construct_pagenavigation($page, $perPage, $totalItem, stripParam(getCurrentUrl(), 'page'));
        } else {
            $dataView['msgType'] = 'success';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_error_thisnotexisted_current'), $forum->getPhrase('yrms_report'));
        }

        $this->renderView('yrms_report_list', $dataView);
    }

    public function addAction()
    {
        $forum = callHelper('Forum');

        $this->_model->setResource(callModel(getParam('resourcetype'))->load(getParam('resourceid')));
        $dataView['resourceLinks'] = array_filter($this->_model->getResource()->getAllDownloadLink() + $this->_model->getResource()->getAllStreamLink());
        foreach($dataView['resourceLinks'] as $key => $resourceLink) {
            if(is_int($key)) {
                $dataView['resourceLinks']['stream'] = $resourceLink;
                unset($dataView['resourceLinks'][$key]);
            }
        }
        $resourceInfo = $this->getResourceInfo(getParam('resourcetype'));
        $dataView['pageTitle'] = $forum->getPhrase('yrms_deadlink_report').' - '.$resourceInfo['name'];

        if(emptyEnhanced($dataView['resourceLinks'])) {
            $dataView['msgType'] = 'error';
            $dataView['msg'] = $forum->getPhrase('yrms_msg_error_general');
            $this->renderView('yrms_message', $dataView);
        }

        $filter = array(
            'resourceid' => getParam('resourceid'),
            'resourcetype' => getParam('resourcetype')
        );
        if(getModel('Report')->setFilter($filter)->getCollection()->count()) {
            $dataView['msgType'] = 'error';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_error_thisexisted'), $forum->getPhrase('yrms_report'));
            $this->renderView('yrms_message', $dataView);
        }


        $dataView['msgType'] = 'info';
        $dataView['msg'] = $forum->getPhrase('yrms_reporttip');



        if(isPost()) {
            $inputData = getPost() + getParam();
            $this->_model
                ->setDeadHostName($inputData['deadLink'])
                ->setResourceId($inputData['resourceid'])
                ->setResourceType($inputData['resourcetype'])
                ->setReporter($forum->getUserInfo('userid'))
                ->save();

            $moderators = $forum->getModerators($forum->getConfig('yrms_vietsubmanga_id_truyendich'));
            $pmTitle = construct_phrase($forum->getPhrase('yrms_pmtitle_report'), $resourceInfo['name']);
            if(array_search('stream', $inputData['deadLink'])) {
                $inputData['deadLink'][array_search('stream', $inputData['deadLink'])] = 'Online';
            }
            $pmContent = construct_phrase($forum->getPhrase('yrms_pm_reportadded'),
                $resourceInfo['name'], implode("\n", $inputData['deadLink']), getParam('resourcetype')
            );
            $forum->sendPM($forum->getConfig('SpecialUsers/superadministrators', 2), $moderators, $pmTitle, $pmContent);

            $dataView['msgType'] = 'success';
            $dataView['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_report'), $forum->getUserInfo('username'));
            $this->renderView('yrms_message', $dataView);
        }

        $this->renderView('yrms_report_form', $dataView);
    }

    public function testAction()
    {
        print_pre(callHelper('Forum')->getConfig('SpecialUsers/superadministrators', 2));
    }

    public function approveAction()
    {
        if(getParam('controller', $_SERVER['HTTP_REFERER']) != 'report' || getParam('action', $_SERVER['HTTP_REFERER']) != 'list') {
            print_no_permission();
        }
        $forum = callHelper('Forum');
        if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
            print_no_permission();
        }

        $this->_model->load(getParam('id'));
        $deadHostName = $this->_model->getDeadHostName();
        $resourceClass = get_class($this->_model->getResource());

        if(in_array('stream', $deadHostName)) {
            foreach($this->_model->getResource()->getAllStreamLink(false) as $streamLinkObject) {
                if($this->_model->getDuration($streamLinkObject, 'stream') > $forum->getConfig('yrms_main_linkduration')) {
                    $streamLinkObject->setKeep(1);
                }

                if($streamLinkObject->getStreamLink()) {
                    $streamLinkObject->setStreamLink('')->save();
                    break;
                } elseif($streamLinkObject->getLink()) {
                    $streamLinkObject->setLink('')->save();
                    break;
                }
            }
            unset($deadHostName[array_search('stream', $deadHostName)]);
        }

        if(!empty($deadHostName)) {
            foreach($this->_model->getResource()->getAllDownloadLink(false) as $downloadLinkObject) {
                if($this->_model->getDuration($downloadLinkObject) > $forum->getConfig('yrms_main_linkduration')) {
                    $downloadLinkObject->setKeep(1);
                }
                $downloadLinkClass = get_class($downloadLinkObject);
                $downloadLinks = ($downloadLinkClass == $resourceClass) ? $downloadLinkObject->getDownloadLink() : $downloadLinkObject->getLink();
                $setDownloadLink = ($downloadLinkClass == $resourceClass) ? 'setDownloadLink' : 'setLink';
                $changed = 0;
                foreach($downloadLinks as $hostName => $downloadLink) {
                    if(in_array($hostName, $deadHostName)) {
                        unset($downloadLinks[$hostName]);
                        $changed = 1;
                    }
                }

                if($changed) {
                    $downloadLinkObject->$setDownloadLink($downloadLinks);
                }

                $downloadLinkObject->save();

            }
        }

        $this->_model->getResource()->save();
        $resourceInfo = $this->_model->getResourceInfo();

        $reporter = callModel('ForumUser')->load($this->_model->getReporter())->getUserName();
        $pmTitle = construct_phrase($forum->getPhrase('yrms_pmtitle_report'), $resourceInfo['name']);
        if(array_search('stream', $deadHostName)) {
            $deadHostName[array_search('stream', $deadHostName)] = 'Online';
        }
        $pmContent = construct_phrase($forum->getPhrase('yrms_pm_reportapproved'),
            $resourceInfo['name'], implode("\n", $deadHostName), $reporter, $forum->getUserInfo('username'), $forum->getUserInfo('userid')
        );
        $forum->sendPM($forum->getConfig('SpecialUsers/superadministrators', 2), $resourceInfo['headResource']->getOwners(), $pmTitle, $pmContent);

        $resourceInfo['headResource']->rebuildInfo();
        $this->_model->delete();

        $forwardData['msgType'] = 'success';
        $forwardData['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_general'),
            $forum->getPhrase('yrms_approve'),
            '',
            '');

        setSession('forwardData', $forwardData);
        header("Location: {$_SERVER['HTTP_REFERER']}");
    }

    public function deleteAction()
    {
        if(getParam('controller', $_SERVER['HTTP_REFERER']) != 'report' || getParam('action', $_SERVER['HTTP_REFERER']) != 'list') {
            print_no_permission();
        }
        $forum = callHelper('Forum');
        $this->_model->load(getParam('id'));

        if(!can_moderate($forum->getConfig('yrms_vietsubmanga_id_truyendich'))) {
            print_no_permission();
        }
        $this->_model->delete();

        $forwardData['msgType'] = 'success';
        $forwardData['msg'] = construct_phrase($forum->getPhrase('yrms_msg_success_general'),
            $forum->getPhrase('yrms_delete'),
            '',
            '');


        setSession('forwardData', $forwardData);
        header("Location: {$_SERVER['HTTP_REFERER']}");
    }
}