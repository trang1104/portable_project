<?php
/**
 * Created by PhpStorm.
 * User: Ms Trang
 * Date: 12/29/14
 * Time: 4:52 PM
 */
class AncestorController extends Singleton
{
    protected $_aditionalDataView = array();

    protected function renderView($viewName, $dataView='')
    {
        $dataView['action'] = getParam('action');
        $page_templater = vB_Template::create($viewName);
        if(is_array($dataView)) {
            $dataView += $this->_aditionalDataView;
            foreach($dataView as $key => $value) {
                $page_templater->register($key,$value);
            }
        }

        if($dataView['msg']) {
            $messagebox = vB_Template::create('yrms_messagebox');
            $messagebox->register('messagetype', $dataView['msgType']);
            $messagebox->register('message', $dataView['msg']);
            $messagebox->register('pageTitle',$dataView['pageTitle']);
            $page_templater->register('messagebox',$messagebox->render());
        }

        $navbits = construct_navbits($navbits);
        $navbar = render_navbar_template($navbits);

        $includecss = array();
        $templater = vB_Template::create('yrms_navbar');
        $templater->register_page_templates();
        $templater->register('includecss', $includecss);
        $templater->register('cpnav', $cpnav);
        $templater->register('HTML', $page_templater->render());
        $templater->register('navbar', $navbar);
        $templater->register('onload', '');
        $templater->register('pagetitle', $dataView['pageTitle']);
        $templater->register('template_hook', $template_hook);
        $templater->register('clientscripts', $clientscripts);

        $reportModel = callModel('Report');
        $templater->register('vietsubmanga_reports', $reportModel->setFilter('resourcetype', 'VietsubmangaChapter')->getCollection()->count());

        print_output($templater->render());
    }

    protected function getAction()
    {
        return getParam('action');
    }

    public function setAdditionalDataView($additionalDataView)
    {
        $this->_aditionalDataView = $additionalDataView;
    }

    public function addAdditionalDataView($additionalDataView)
    {
        if(empty($this->_aditionalDataView)) {
            $this->_aditionalDataView = $additionalDataView;
        } else {
            $this->_aditionalDataView += $additionalDataView;
        }

    }

    protected function getView($viewName)
    {

    }
}