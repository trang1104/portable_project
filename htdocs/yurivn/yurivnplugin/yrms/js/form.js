function NumberOnly(evt, preventZero){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function showhideHostinput(){

    var numberofhost = document.getElementById("numberofhost").value;
    var i=1;
    while(i<=numberofhost){
        document.getElementById('host'+i).setAttribute('style','');
        i++;
    }
    while(i>numberofhost && i <=9){
        document.getElementById('host'+i).setAttribute('style','visibility:hidden;line-height:0;height:0px;');
        i++;
    }
}

function vietsubMangaWarning(){
    var mangaTitles = $('#title').val() + ',' + $('#othertitle').val();
    $.ajax({
        type: 'POST',
        url: 'yurivnplugin/yrms/index.php?controller=vietsubmangamanga&action=dupplicateWarningAjax',
        data: {
            mangaTitles: mangaTitles
        },
        success: function(result){
            if(result) {
                $('#warning').css({"padding":"10px 0px 15px 0px"});
            } else {
                $('#warning').css({"padding":""});
            }

            $('#warning').html(result);
        }
    });
}

function exclusiveRequiredField(field1, field2){
    if(jQuery('#'+field1+' input').val() != '' && jQuery('#'+field2+' input').val() == '') {
        jQuery('#'+field1+' label').addClass('required');
        jQuery('#'+field1+' input').attr('required', '');
        jQuery('#'+field2+' label').removeClass();
        jQuery('#'+field2+' input').removeAttr('required');
    }

    if(jQuery('#'+field2+' input').val() != '' && jQuery('#'+field1+' input').val() == '') {
        jQuery('#'+field1+' label').removeClass();
        jQuery('#'+field1+' input').removeAttr('required');
        jQuery('#'+field2+' label').addClass('required');
        jQuery('#'+field2+' input').attr('required', '');
    }

    if(jQuery('#'+field2+' input').val() == '' && jQuery('#'+field1+' input').val() == ''){
        jQuery('#'+field1+' label').addClass('required');
        jQuery('#'+field1+' input').attr('required', '');
        jQuery('#'+field2+' label').addClass('required');
        jQuery('#'+field2+' input').attr('required', '');
    }
}