<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tourism');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'e=O9y$%f{i|.::l.^%wLx+3cg#?dJdt@#avr|~/l} N*KtD`f1&M,_*ZSm~X{TeO');
define('SECURE_AUTH_KEY',  '%uFE#:*|5p6+~q7-K5Qv%k3lm~l.CnV:EeJ0Z!< 8R}kk*R>XcL`}7&{W4tq@@Yx');
define('LOGGED_IN_KEY',    '*s6=4h[uLB#i4!QYOj x6y8&Pj0#]s?,c|UvGf-(5`@P6t6p_5<Ev!7[ovEQl N ');
define('NONCE_KEY',        'I(~O5W|c61g2S?EwrVIq (x|m$gq9>Z9S|o(+).n2:f5A;uw%|T%(5Y_pL GaY{V');
define('AUTH_SALT',        '[7?-vh-5XcX1-[@v7>%m}$`!@%k@J--P_#-;wYz0GVC3v{~}WiX-6.r#kHldd&<#');
define('SECURE_AUTH_SALT', '=1ch1|[Jj|^pnKRZ~bz:qxyI-[H;+EUy9*U]-+2#w@$0B5y.,YqJC*@%XNjTOEC]');
define('LOGGED_IN_SALT',   '::;?g f~WiP]Gto13)/wRnD0S@:B%TeuRh+Ti:#bFHD|oVojyX[F]*#nFyV,fgu|');
define('NONCE_SALT',       'S|<ML=/:y-N> cen#Y5>V iv=8 `pXeV0D-x-VHY^FJazK8-I8HO7q8s{aR!Uz32');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
